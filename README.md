README for drugvedavendor
==========================

For building in dev environment outside eclipse, update database properties in
src\main\resources\config\application*.yml and run:

    mvn spring-boot:run

For production run (as test cases are not updated, they will fail the build):

    mvn -DskipTests=true -Pprod package

This will generate two files (if your application is called "jhipster"):

    target/jhipster-0.0.1-SNAPSHOT.war
    target/jhipster-0.0.1-SNAPSHOT.war.original

The first one is an executable WAR file (see next section to run it). It can also be deployed on an application server, but as it includes the Tomcat runtime libs, you will probably get some warnings, that's why we recommend you use the second, ".original" file if you want to deploy JHipster on an application server.

Once you have deployed you WAR file on your application server:

    It will use by default the "dev" profile
    It can run in "production mode" if you trigger the "prod" profile (there are several ways to trigger a Spring profile, for example you can add -Dspring.profiles.active=prod to your JAVA_OPTS when running your server)

Executing the WAR file without an application server

Instead of deploying on an application server, many people find it easier to just have an exectuable WAR file (which includes Tomcat 7, in fact).

The first WAR file generated in the previous step is such a WAR, so you can run it in "development mode" by typing:

    java -jar jhipster-0.0.1-SNAPSHOT.war

Or in "production" mode:

java -jar jhipster-0.0.1-SNAPSHOT.war --spring.profiles.active=prod