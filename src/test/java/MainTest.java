
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;

import com.drugveda.vendor.domain.ProductVariation;
import com.drugveda.vendor.domain.VariationOption;
import com.drugveda.vendor.domain.enumeration.MeasureType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class MainTest {

	@Test
	public void TestJson(){
		ProductVariation productVariation = new ProductVariation();
		
		Set<VariationOption> set = new HashSet<VariationOption>();
		for(int i=0;i<3;i++){
			VariationOption variationOption = new VariationOption();
			variationOption.setQuanity(i);
			variationOption.setActive(true);
			set.add(variationOption);
		}
		
		productVariation.setMeasureType(MeasureType.GM);
		productVariation.setVariationoptions(set);
		ObjectMapper objectMap= new ObjectMapper();
		ObjectWriter withDefaultPrettyPrinter = objectMap.writer().withDefaultPrettyPrinter();
		try {
			String writeValueAsString = withDefaultPrettyPrinter.writeValueAsString(productVariation);
			System.out.println(writeValueAsString);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
