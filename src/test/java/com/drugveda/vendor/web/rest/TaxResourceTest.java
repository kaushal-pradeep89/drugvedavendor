package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.Tax;
import com.drugveda.vendor.repository.TaxRepository;
import com.drugveda.vendor.repository.search.TaxSearchRepository;


/**
 * Test class for the TaxResource REST controller.
 *
 * @see TaxResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class TaxResourceTest {

    private static final String DEFAULT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_NAME = "UPDATED_TEXT";

    private static final Double DEFAULT_TAX = 1D;
    private static final Double UPDATED_TAX = 2D;

    private static final Double DEFAULT_COL_EXT1 = 1D;
    private static final Double UPDATED_COL_EXT1 = 2D;

    private static final Double DEFAULT_COL_EXT2 = 1D;
    private static final Double UPDATED_COL_EXT2 = 2D;

    @Inject
    private TaxRepository taxRepository;

    @Inject
    private TaxSearchRepository taxSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restTaxMockMvc;

    private Tax tax;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        TaxResource taxResource = new TaxResource();
        ReflectionTestUtils.setField(taxResource, "taxRepository", taxRepository);
        ReflectionTestUtils.setField(taxResource, "taxSearchRepository", taxSearchRepository);
        this.restTaxMockMvc = MockMvcBuilders.standaloneSetup(taxResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        tax = new Tax();
        tax.setName(DEFAULT_NAME);
        tax.setTax(DEFAULT_TAX);
        tax.setColExt1(DEFAULT_COL_EXT1);
        tax.setColExt2(DEFAULT_COL_EXT2);
    }

    @Test
    @Transactional
    public void createTax() throws Exception {
        int databaseSizeBeforeCreate = taxRepository.findAll().size();

        // Create the Tax

        restTaxMockMvc.perform(post("/api/taxs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tax)))
                .andExpect(status().isCreated());

        // Validate the Tax in the database
        List<Tax> taxs = taxRepository.findAll();
        assertThat(taxs).hasSize(databaseSizeBeforeCreate + 1);
        Tax testTax = taxs.get(taxs.size() - 1);
        assertThat(testTax.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testTax.getTax()).isEqualTo(DEFAULT_TAX);
        assertThat(testTax.getColExt1()).isEqualTo(DEFAULT_COL_EXT1);
        assertThat(testTax.getColExt2()).isEqualTo(DEFAULT_COL_EXT2);
    }

    @Test
    @Transactional
    public void getAllTaxs() throws Exception {
        // Initialize the database
        taxRepository.saveAndFlush(tax);

        // Get all the taxs
        restTaxMockMvc.perform(get("/api/taxs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(tax.getId().intValue())))
                .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
                .andExpect(jsonPath("$.[*].tax").value(hasItem(DEFAULT_TAX.doubleValue())))
                .andExpect(jsonPath("$.[*].colExt1").value(hasItem(DEFAULT_COL_EXT1.doubleValue())))
                .andExpect(jsonPath("$.[*].colExt2").value(hasItem(DEFAULT_COL_EXT2.doubleValue())));
    }

    @Test
    @Transactional
    public void getTax() throws Exception {
        // Initialize the database
        taxRepository.saveAndFlush(tax);

        // Get the tax
        restTaxMockMvc.perform(get("/api/taxs/{id}", tax.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(tax.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.tax").value(DEFAULT_TAX.doubleValue()))
            .andExpect(jsonPath("$.colExt1").value(DEFAULT_COL_EXT1.doubleValue()))
            .andExpect(jsonPath("$.colExt2").value(DEFAULT_COL_EXT2.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingTax() throws Exception {
        // Get the tax
        restTaxMockMvc.perform(get("/api/taxs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTax() throws Exception {
        // Initialize the database
        taxRepository.saveAndFlush(tax);

		int databaseSizeBeforeUpdate = taxRepository.findAll().size();

        // Update the tax
        tax.setName(UPDATED_NAME);
        tax.setTax(UPDATED_TAX);
        tax.setColExt1(UPDATED_COL_EXT1);
        tax.setColExt2(UPDATED_COL_EXT2);
        

        restTaxMockMvc.perform(put("/api/taxs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(tax)))
                .andExpect(status().isOk());

        // Validate the Tax in the database
        List<Tax> taxs = taxRepository.findAll();
        assertThat(taxs).hasSize(databaseSizeBeforeUpdate);
        Tax testTax = taxs.get(taxs.size() - 1);
        assertThat(testTax.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testTax.getTax()).isEqualTo(UPDATED_TAX);
        assertThat(testTax.getColExt1()).isEqualTo(UPDATED_COL_EXT1);
        assertThat(testTax.getColExt2()).isEqualTo(UPDATED_COL_EXT2);
    }

    @Test
    @Transactional
    public void deleteTax() throws Exception {
        // Initialize the database
        taxRepository.saveAndFlush(tax);

		int databaseSizeBeforeDelete = taxRepository.findAll().size();

        // Get the tax
        restTaxMockMvc.perform(delete("/api/taxs/{id}", tax.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Tax> taxs = taxRepository.findAll();
        assertThat(taxs).hasSize(databaseSizeBeforeDelete - 1);
    }
}
