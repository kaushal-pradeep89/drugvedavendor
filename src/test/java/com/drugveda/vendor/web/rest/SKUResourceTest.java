package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.SKU;
import com.drugveda.vendor.repository.SKURepository;
import com.drugveda.vendor.repository.search.SKUSearchRepository;


/**
 * Test class for the SKUResource REST controller.
 *
 * @see SKUResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class SKUResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final DateTime DEFAULT_CREATE_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATE_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATE_DATE_STR = dateTimeFormatter.print(DEFAULT_CREATE_DATE);

    private static final DateTime DEFAULT_UPDATE_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_UPDATE_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_UPDATE_DATE_STR = dateTimeFormatter.print(DEFAULT_UPDATE_DATE);

    @Inject
    private SKURepository sKURepository;

    @Inject
    private SKUSearchRepository sKUSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restSKUMockMvc;

    private SKU sKU;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        SKUResource sKUResource = new SKUResource();
        ReflectionTestUtils.setField(sKUResource, "sKURepository", sKURepository);
        ReflectionTestUtils.setField(sKUResource, "sKUSearchRepository", sKUSearchRepository);
        this.restSKUMockMvc = MockMvcBuilders.standaloneSetup(sKUResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        sKU = new SKU();
        sKU.setCreateDate(DEFAULT_CREATE_DATE);
        sKU.setUpdateDate(DEFAULT_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void createSKU() throws Exception {
        int databaseSizeBeforeCreate = sKURepository.findAll().size();

        // Create the SKU

        restSKUMockMvc.perform(post("/api/sKUs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(sKU)))
                .andExpect(status().isCreated());

        // Validate the SKU in the database
        List<SKU> sKUs = sKURepository.findAll();
        assertThat(sKUs).hasSize(databaseSizeBeforeCreate + 1);
        SKU testSKU = sKUs.get(sKUs.size() - 1);
        assertThat(testSKU.getCreateDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATE_DATE);
        assertThat(testSKU.getUpdateDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void getAllSKUs() throws Exception {
        // Initialize the database
        sKURepository.saveAndFlush(sKU);

        // Get all the sKUs
        restSKUMockMvc.perform(get("/api/sKUs"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(sKU.getId().intValue())))
                .andExpect(jsonPath("$.[*].createDate").value(hasItem(DEFAULT_CREATE_DATE_STR)))
                .andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE_STR)));
    }

    @Test
    @Transactional
    public void getSKU() throws Exception {
        // Initialize the database
        sKURepository.saveAndFlush(sKU);

        // Get the sKU
        restSKUMockMvc.perform(get("/api/sKUs/{id}", sKU.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(sKU.getId().intValue()))
            .andExpect(jsonPath("$.createDate").value(DEFAULT_CREATE_DATE_STR))
            .andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingSKU() throws Exception {
        // Get the sKU
        restSKUMockMvc.perform(get("/api/sKUs/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateSKU() throws Exception {
        // Initialize the database
        sKURepository.saveAndFlush(sKU);

		int databaseSizeBeforeUpdate = sKURepository.findAll().size();

        // Update the sKU
        sKU.setCreateDate(UPDATED_CREATE_DATE);
        sKU.setUpdateDate(UPDATED_UPDATE_DATE);
        

        restSKUMockMvc.perform(put("/api/sKUs")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(sKU)))
                .andExpect(status().isOk());

        // Validate the SKU in the database
        List<SKU> sKUs = sKURepository.findAll();
        assertThat(sKUs).hasSize(databaseSizeBeforeUpdate);
        SKU testSKU = sKUs.get(sKUs.size() - 1);
        assertThat(testSKU.getCreateDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATE_DATE);
        assertThat(testSKU.getUpdateDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_UPDATE_DATE);
    }

    @Test
    @Transactional
    public void deleteSKU() throws Exception {
        // Initialize the database
        sKURepository.saveAndFlush(sKU);

		int databaseSizeBeforeDelete = sKURepository.findAll().size();

        // Get the sKU
        restSKUMockMvc.perform(delete("/api/sKUs/{id}", sKU.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<SKU> sKUs = sKURepository.findAll();
        assertThat(sKUs).hasSize(databaseSizeBeforeDelete - 1);
    }
}
