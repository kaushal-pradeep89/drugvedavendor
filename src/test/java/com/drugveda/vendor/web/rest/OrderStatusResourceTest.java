package com.drugveda.vendor.web.rest;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.OrderStatus;
import com.drugveda.vendor.repository.OrderStatusRepository;
import com.drugveda.vendor.repository.search.OrderStatusSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.drugveda.vendor.domain.enumeration.STATUS;

/**
 * Test class for the OrderStatusResource REST controller.
 *
 * @see OrderStatusResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class OrderStatusResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final DateTime DEFAULT_ETA = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_ETA = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_ETA_STR = dateTimeFormatter.print(DEFAULT_ETA);

    private static final Integer DEFAULT_UNITS = 1;
    private static final Integer UPDATED_UNITS = 2;

    private static final STATUS DEFAULT_STATUS = STATUS.AWAITING_ACCEPTANCE;
    private static final STATUS UPDATED_STATUS = STATUS.ACCEPTED;

    @Inject
    private OrderStatusRepository orderStatusRepository;

    @Inject
    private OrderStatusSearchRepository orderStatusSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restOrderStatusMockMvc;

    private OrderStatus orderStatus;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OrderStatusResource orderStatusResource = new OrderStatusResource();
        ReflectionTestUtils.setField(orderStatusResource, "orderStatusRepository", orderStatusRepository);
        ReflectionTestUtils.setField(orderStatusResource, "orderStatusSearchRepository", orderStatusSearchRepository);
        this.restOrderStatusMockMvc = MockMvcBuilders.standaloneSetup(orderStatusResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        orderStatus = new OrderStatus();
        orderStatus.setEta(DEFAULT_ETA);
        orderStatus.setUnits(DEFAULT_UNITS);
        orderStatus.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createOrderStatus() throws Exception {
        int databaseSizeBeforeCreate = orderStatusRepository.findAll().size();

        // Create the OrderStatus

        restOrderStatusMockMvc.perform(post("/api/orderStatuss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderStatus)))
                .andExpect(status().isCreated());

        // Validate the OrderStatus in the database
        List<OrderStatus> orderStatuss = orderStatusRepository.findAll();
        assertThat(orderStatuss).hasSize(databaseSizeBeforeCreate + 1);
        OrderStatus testOrderStatus = orderStatuss.get(orderStatuss.size() - 1);
        assertThat(testOrderStatus.getEta().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_ETA);
        assertThat(testOrderStatus.getUnits()).isEqualTo(DEFAULT_UNITS);
        assertThat(testOrderStatus.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOrderStatuss() throws Exception {
        // Initialize the database
        orderStatusRepository.saveAndFlush(orderStatus);

        // Get all the orderStatuss
        restOrderStatusMockMvc.perform(get("/api/orderStatuss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(orderStatus.getId().intValue())))
                .andExpect(jsonPath("$.[*].eta").value(hasItem(DEFAULT_ETA_STR)))
                .andExpect(jsonPath("$.[*].units").value(hasItem(DEFAULT_UNITS)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getOrderStatus() throws Exception {
        // Initialize the database
        orderStatusRepository.saveAndFlush(orderStatus);

        // Get the orderStatus
        restOrderStatusMockMvc.perform(get("/api/orderStatuss/{id}", orderStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(orderStatus.getId().intValue()))
            .andExpect(jsonPath("$.eta").value(DEFAULT_ETA_STR))
            .andExpect(jsonPath("$.units").value(DEFAULT_UNITS))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOrderStatus() throws Exception {
        // Get the orderStatus
        restOrderStatusMockMvc.perform(get("/api/orderStatuss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderStatus() throws Exception {
        // Initialize the database
        orderStatusRepository.saveAndFlush(orderStatus);

		int databaseSizeBeforeUpdate = orderStatusRepository.findAll().size();

        // Update the orderStatus
        orderStatus.setEta(UPDATED_ETA);
        orderStatus.setUnits(UPDATED_UNITS);
        orderStatus.setStatus(UPDATED_STATUS);
        

        restOrderStatusMockMvc.perform(put("/api/orderStatuss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderStatus)))
                .andExpect(status().isOk());

        // Validate the OrderStatus in the database
        List<OrderStatus> orderStatuss = orderStatusRepository.findAll();
        assertThat(orderStatuss).hasSize(databaseSizeBeforeUpdate);
        OrderStatus testOrderStatus = orderStatuss.get(orderStatuss.size() - 1);
        assertThat(testOrderStatus.getEta().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_ETA);
        assertThat(testOrderStatus.getUnits()).isEqualTo(UPDATED_UNITS);
        assertThat(testOrderStatus.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteOrderStatus() throws Exception {
        // Initialize the database
        orderStatusRepository.saveAndFlush(orderStatus);

		int databaseSizeBeforeDelete = orderStatusRepository.findAll().size();

        // Get the orderStatus
        restOrderStatusMockMvc.perform(delete("/api/orderStatuss/{id}", orderStatus.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<OrderStatus> orderStatuss = orderStatusRepository.findAll();
        assertThat(orderStatuss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
