package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.GPSLocation;
import com.drugveda.vendor.repository.GPSLocationRepository;
import com.drugveda.vendor.repository.search.GPSLocationSearchRepository;


/**
 * Test class for the GPSLocationResource REST controller.
 *
 * @see GPSLocationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class GPSLocationResourceTest {

    private static final String DEFAULT_LOCATION = "SAMPLE_TEXT";
    private static final String UPDATED_LOCATION = "UPDATED_TEXT";

    @Inject
    private GPSLocationRepository gPSLocationRepository;

    @Inject
    private GPSLocationSearchRepository gPSLocationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restGPSLocationMockMvc;

    private GPSLocation gPSLocation;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        GPSLocationResource gPSLocationResource = new GPSLocationResource();
        ReflectionTestUtils.setField(gPSLocationResource, "gPSLocationRepository", gPSLocationRepository);
        ReflectionTestUtils.setField(gPSLocationResource, "gPSLocationSearchRepository", gPSLocationSearchRepository);
        this.restGPSLocationMockMvc = MockMvcBuilders.standaloneSetup(gPSLocationResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        gPSLocation = new GPSLocation();
        gPSLocation.setLocation(DEFAULT_LOCATION);
    }

    @Test
    @Transactional
    public void createGPSLocation() throws Exception {
        int databaseSizeBeforeCreate = gPSLocationRepository.findAll().size();

        // Create the GPSLocation

        restGPSLocationMockMvc.perform(post("/api/gPSLocations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gPSLocation)))
                .andExpect(status().isCreated());

        // Validate the GPSLocation in the database
        List<GPSLocation> gPSLocations = gPSLocationRepository.findAll();
        assertThat(gPSLocations).hasSize(databaseSizeBeforeCreate + 1);
        GPSLocation testGPSLocation = gPSLocations.get(gPSLocations.size() - 1);
        assertThat(testGPSLocation.getLocation()).isEqualTo(DEFAULT_LOCATION);
    }

    @Test
    @Transactional
    public void getAllGPSLocations() throws Exception {
        // Initialize the database
        gPSLocationRepository.saveAndFlush(gPSLocation);

        // Get all the gPSLocations
        restGPSLocationMockMvc.perform(get("/api/gPSLocations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(gPSLocation.getId().intValue())))
                .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())));
    }

    @Test
    @Transactional
    public void getGPSLocation() throws Exception {
        // Initialize the database
        gPSLocationRepository.saveAndFlush(gPSLocation);

        // Get the gPSLocation
        restGPSLocationMockMvc.perform(get("/api/gPSLocations/{id}", gPSLocation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(gPSLocation.getId().intValue()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingGPSLocation() throws Exception {
        // Get the gPSLocation
        restGPSLocationMockMvc.perform(get("/api/gPSLocations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGPSLocation() throws Exception {
        // Initialize the database
        gPSLocationRepository.saveAndFlush(gPSLocation);

		int databaseSizeBeforeUpdate = gPSLocationRepository.findAll().size();

        // Update the gPSLocation
        gPSLocation.setLocation(UPDATED_LOCATION);
        

        restGPSLocationMockMvc.perform(put("/api/gPSLocations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(gPSLocation)))
                .andExpect(status().isOk());

        // Validate the GPSLocation in the database
        List<GPSLocation> gPSLocations = gPSLocationRepository.findAll();
        assertThat(gPSLocations).hasSize(databaseSizeBeforeUpdate);
        GPSLocation testGPSLocation = gPSLocations.get(gPSLocations.size() - 1);
        assertThat(testGPSLocation.getLocation()).isEqualTo(UPDATED_LOCATION);
    }

    @Test
    @Transactional
    public void deleteGPSLocation() throws Exception {
        // Initialize the database
        gPSLocationRepository.saveAndFlush(gPSLocation);

		int databaseSizeBeforeDelete = gPSLocationRepository.findAll().size();

        // Get the gPSLocation
        restGPSLocationMockMvc.perform(delete("/api/gPSLocations/{id}", gPSLocation.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<GPSLocation> gPSLocations = gPSLocationRepository.findAll();
        assertThat(gPSLocations).hasSize(databaseSizeBeforeDelete - 1);
    }
}
