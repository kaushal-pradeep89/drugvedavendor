package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.ProductCatalogue;
import com.drugveda.vendor.repository.ProductCatalogueRepository;
import com.drugveda.vendor.repository.search.ProductCatalogueSearchRepository;


/**
 * Test class for the ProductCatalogueResource REST controller.
 *
 * @see ProductCatalogueResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ProductCatalogueResourceTest {


    private static final Integer DEFAULT_MIN_ORDER = 1;
    private static final Integer UPDATED_MIN_ORDER = 2;

    private static final Boolean DEFAULT_APPLY_TAX = false;
    private static final Boolean UPDATED_APPLY_TAX = true;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Inject
    private ProductCatalogueRepository productCatalogueRepository;

    @Inject
    private ProductCatalogueSearchRepository productCatalogueSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restProductCatalogueMockMvc;

    private ProductCatalogue productCatalogue;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProductCatalogueResource productCatalogueResource = new ProductCatalogueResource();
        ReflectionTestUtils.setField(productCatalogueResource, "productCatalogueRepository", productCatalogueRepository);
        ReflectionTestUtils.setField(productCatalogueResource, "productCatalogueSearchRepository", productCatalogueSearchRepository);
        this.restProductCatalogueMockMvc = MockMvcBuilders.standaloneSetup(productCatalogueResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        productCatalogue = new ProductCatalogue();
        productCatalogue.setMinOrder(DEFAULT_MIN_ORDER);
        productCatalogue.setApplyTax(DEFAULT_APPLY_TAX);
        productCatalogue.setActive(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createProductCatalogue() throws Exception {
        int databaseSizeBeforeCreate = productCatalogueRepository.findAll().size();

        // Create the ProductCatalogue

        restProductCatalogueMockMvc.perform(post("/api/productCatalogues")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productCatalogue)))
                .andExpect(status().isCreated());

        // Validate the ProductCatalogue in the database
        List<ProductCatalogue> productCatalogues = productCatalogueRepository.findAll();
        assertThat(productCatalogues).hasSize(databaseSizeBeforeCreate + 1);
        ProductCatalogue testProductCatalogue = productCatalogues.get(productCatalogues.size() - 1);
        assertThat(testProductCatalogue.getMinOrder()).isEqualTo(DEFAULT_MIN_ORDER);
        assertThat(testProductCatalogue.getApplyTax()).isEqualTo(DEFAULT_APPLY_TAX);
        assertThat(testProductCatalogue.getActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllProductCatalogues() throws Exception {
        // Initialize the database
        productCatalogueRepository.saveAndFlush(productCatalogue);

        // Get all the productCatalogues
        restProductCatalogueMockMvc.perform(get("/api/productCatalogues"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(productCatalogue.getId().intValue())))
                .andExpect(jsonPath("$.[*].minOrder").value(hasItem(DEFAULT_MIN_ORDER)))
                .andExpect(jsonPath("$.[*].applyTax").value(hasItem(DEFAULT_APPLY_TAX.booleanValue())))
                .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getProductCatalogue() throws Exception {
        // Initialize the database
        productCatalogueRepository.saveAndFlush(productCatalogue);

        // Get the productCatalogue
        restProductCatalogueMockMvc.perform(get("/api/productCatalogues/{id}", productCatalogue.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(productCatalogue.getId().intValue()))
            .andExpect(jsonPath("$.minOrder").value(DEFAULT_MIN_ORDER))
            .andExpect(jsonPath("$.applyTax").value(DEFAULT_APPLY_TAX.booleanValue()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingProductCatalogue() throws Exception {
        // Get the productCatalogue
        restProductCatalogueMockMvc.perform(get("/api/productCatalogues/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductCatalogue() throws Exception {
        // Initialize the database
        productCatalogueRepository.saveAndFlush(productCatalogue);

		int databaseSizeBeforeUpdate = productCatalogueRepository.findAll().size();

        // Update the productCatalogue
        productCatalogue.setMinOrder(UPDATED_MIN_ORDER);
        productCatalogue.setApplyTax(UPDATED_APPLY_TAX);
        productCatalogue.setActive(UPDATED_ACTIVE);
        

        restProductCatalogueMockMvc.perform(put("/api/productCatalogues")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productCatalogue)))
                .andExpect(status().isOk());

        // Validate the ProductCatalogue in the database
        List<ProductCatalogue> productCatalogues = productCatalogueRepository.findAll();
        assertThat(productCatalogues).hasSize(databaseSizeBeforeUpdate);
        ProductCatalogue testProductCatalogue = productCatalogues.get(productCatalogues.size() - 1);
        assertThat(testProductCatalogue.getMinOrder()).isEqualTo(UPDATED_MIN_ORDER);
        assertThat(testProductCatalogue.getApplyTax()).isEqualTo(UPDATED_APPLY_TAX);
        assertThat(testProductCatalogue.getActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void deleteProductCatalogue() throws Exception {
        // Initialize the database
        productCatalogueRepository.saveAndFlush(productCatalogue);

		int databaseSizeBeforeDelete = productCatalogueRepository.findAll().size();

        // Get the productCatalogue
        restProductCatalogueMockMvc.perform(delete("/api/productCatalogues/{id}", productCatalogue.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductCatalogue> productCatalogues = productCatalogueRepository.findAll();
        assertThat(productCatalogues).hasSize(databaseSizeBeforeDelete - 1);
    }
}
