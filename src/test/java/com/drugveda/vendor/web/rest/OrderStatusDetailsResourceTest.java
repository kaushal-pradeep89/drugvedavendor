package com.drugveda.vendor.web.rest;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.OrderStatusDetails;
import com.drugveda.vendor.repository.OrderStatusDetailsRepository;
import com.drugveda.vendor.repository.search.OrderStatusDetailsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the OrderStatusDetailsResource REST controller.
 *
 * @see OrderStatusDetailsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class OrderStatusDetailsResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final DateTime DEFAULT_CREATE_DATE_TIME = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATE_DATE_TIME = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATE_DATE_TIME_STR = dateTimeFormatter.print(DEFAULT_CREATE_DATE_TIME);
    private static final String DEFAULT_STATUS = "SAMPLE_TEXT";
    private static final String UPDATED_STATUS = "UPDATED_TEXT";

    @Inject
    private OrderStatusDetailsRepository orderStatusDetailsRepository;

    @Inject
    private OrderStatusDetailsSearchRepository orderStatusDetailsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restOrderStatusDetailsMockMvc;

    private OrderStatusDetails orderStatusDetails;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OrderStatusDetailsResource orderStatusDetailsResource = new OrderStatusDetailsResource();
        ReflectionTestUtils.setField(orderStatusDetailsResource, "orderStatusDetailsRepository", orderStatusDetailsRepository);
        ReflectionTestUtils.setField(orderStatusDetailsResource, "orderStatusDetailsSearchRepository", orderStatusDetailsSearchRepository);
        this.restOrderStatusDetailsMockMvc = MockMvcBuilders.standaloneSetup(orderStatusDetailsResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        orderStatusDetails = new OrderStatusDetails();
        orderStatusDetails.setCreateDateTime(DEFAULT_CREATE_DATE_TIME);
        orderStatusDetails.setStatus(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createOrderStatusDetails() throws Exception {
        int databaseSizeBeforeCreate = orderStatusDetailsRepository.findAll().size();

        // Create the OrderStatusDetails

        restOrderStatusDetailsMockMvc.perform(post("/api/orderStatusDetailss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderStatusDetails)))
                .andExpect(status().isCreated());

        // Validate the OrderStatusDetails in the database
        List<OrderStatusDetails> orderStatusDetailss = orderStatusDetailsRepository.findAll();
        assertThat(orderStatusDetailss).hasSize(databaseSizeBeforeCreate + 1);
        OrderStatusDetails testOrderStatusDetails = orderStatusDetailss.get(orderStatusDetailss.size() - 1);
        assertThat(testOrderStatusDetails.getCreateDateTime().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATE_DATE_TIME);
        assertThat(testOrderStatusDetails.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void getAllOrderStatusDetailss() throws Exception {
        // Initialize the database
        orderStatusDetailsRepository.saveAndFlush(orderStatusDetails);

        // Get all the orderStatusDetailss
        restOrderStatusDetailsMockMvc.perform(get("/api/orderStatusDetailss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(orderStatusDetails.getId().intValue())))
                .andExpect(jsonPath("$.[*].createDateTime").value(hasItem(DEFAULT_CREATE_DATE_TIME_STR)))
                .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getOrderStatusDetails() throws Exception {
        // Initialize the database
        orderStatusDetailsRepository.saveAndFlush(orderStatusDetails);

        // Get the orderStatusDetails
        restOrderStatusDetailsMockMvc.perform(get("/api/orderStatusDetailss/{id}", orderStatusDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(orderStatusDetails.getId().intValue()))
            .andExpect(jsonPath("$.createDateTime").value(DEFAULT_CREATE_DATE_TIME_STR))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingOrderStatusDetails() throws Exception {
        // Get the orderStatusDetails
        restOrderStatusDetailsMockMvc.perform(get("/api/orderStatusDetailss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderStatusDetails() throws Exception {
        // Initialize the database
        orderStatusDetailsRepository.saveAndFlush(orderStatusDetails);

		int databaseSizeBeforeUpdate = orderStatusDetailsRepository.findAll().size();

        // Update the orderStatusDetails
        orderStatusDetails.setCreateDateTime(UPDATED_CREATE_DATE_TIME);
        orderStatusDetails.setStatus(UPDATED_STATUS);
        

        restOrderStatusDetailsMockMvc.perform(put("/api/orderStatusDetailss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderStatusDetails)))
                .andExpect(status().isOk());

        // Validate the OrderStatusDetails in the database
        List<OrderStatusDetails> orderStatusDetailss = orderStatusDetailsRepository.findAll();
        assertThat(orderStatusDetailss).hasSize(databaseSizeBeforeUpdate);
        OrderStatusDetails testOrderStatusDetails = orderStatusDetailss.get(orderStatusDetailss.size() - 1);
        assertThat(testOrderStatusDetails.getCreateDateTime().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATE_DATE_TIME);
        assertThat(testOrderStatusDetails.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void deleteOrderStatusDetails() throws Exception {
        // Initialize the database
        orderStatusDetailsRepository.saveAndFlush(orderStatusDetails);

		int databaseSizeBeforeDelete = orderStatusDetailsRepository.findAll().size();

        // Get the orderStatusDetails
        restOrderStatusDetailsMockMvc.perform(delete("/api/orderStatusDetailss/{id}", orderStatusDetails.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<OrderStatusDetails> orderStatusDetailss = orderStatusDetailsRepository.findAll();
        assertThat(orderStatusDetailss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
