package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.ProductImage;
import com.drugveda.vendor.repository.ProductImageRepository;
import com.drugveda.vendor.repository.search.ProductImageSearchRepository;


/**
 * Test class for the ProductImageResource REST controller.
 *
 * @see ProductImageResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ProductImageResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_IMAGE_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_IMAGE_NAME = "UPDATED_TEXT";
    private static final String DEFAULT_IMAGE_URL = "SAMPLE_TEXT";
    private static final String UPDATED_IMAGE_URL = "UPDATED_TEXT";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final DateTime DEFAULT_CRATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CRATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CRATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CRATED_DATE);

    private static final DateTime DEFAULT_UPDATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_UPDATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_UPDATED_DATE_STR = dateTimeFormatter.print(DEFAULT_UPDATED_DATE);

    @Inject
    private ProductImageRepository productImageRepository;

    @Inject
    private ProductImageSearchRepository productImageSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restProductImageMockMvc;

    private ProductImage productImage;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProductImageResource productImageResource = new ProductImageResource();
        ReflectionTestUtils.setField(productImageResource, "productImageRepository", productImageRepository);
        ReflectionTestUtils.setField(productImageResource, "productImageSearchRepository", productImageSearchRepository);
        this.restProductImageMockMvc = MockMvcBuilders.standaloneSetup(productImageResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        productImage = new ProductImage();
        productImage.setImageName(DEFAULT_IMAGE_NAME);
        productImage.setImageUrl(DEFAULT_IMAGE_URL);
        productImage.setActive(DEFAULT_ACTIVE);
        productImage.setCratedDate(DEFAULT_CRATED_DATE);
        productImage.setUpdatedDate(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createProductImage() throws Exception {
        int databaseSizeBeforeCreate = productImageRepository.findAll().size();

        // Create the ProductImage

        restProductImageMockMvc.perform(post("/api/productImages")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productImage)))
                .andExpect(status().isCreated());

        // Validate the ProductImage in the database
        List<ProductImage> productImages = productImageRepository.findAll();
        assertThat(productImages).hasSize(databaseSizeBeforeCreate + 1);
        ProductImage testProductImage = productImages.get(productImages.size() - 1);
        assertThat(testProductImage.getImageName()).isEqualTo(DEFAULT_IMAGE_NAME);
        assertThat(testProductImage.getImageUrl()).isEqualTo(DEFAULT_IMAGE_URL);
        assertThat(testProductImage.getActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testProductImage.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CRATED_DATE);
        assertThat(testProductImage.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProductImages() throws Exception {
        // Initialize the database
        productImageRepository.saveAndFlush(productImage);

        // Get all the productImages
        restProductImageMockMvc.perform(get("/api/productImages"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(productImage.getId().intValue())))
                .andExpect(jsonPath("$.[*].imageName").value(hasItem(DEFAULT_IMAGE_NAME.toString())))
                .andExpect(jsonPath("$.[*].imageUrl").value(hasItem(DEFAULT_IMAGE_URL.toString())))
                .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].cratedDate").value(hasItem(DEFAULT_CRATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE_STR)));
    }

    @Test
    @Transactional
    public void getProductImage() throws Exception {
        // Initialize the database
        productImageRepository.saveAndFlush(productImage);

        // Get the productImage
        restProductImageMockMvc.perform(get("/api/productImages/{id}", productImage.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(productImage.getId().intValue()))
            .andExpect(jsonPath("$.imageName").value(DEFAULT_IMAGE_NAME.toString()))
            .andExpect(jsonPath("$.imageUrl").value(DEFAULT_IMAGE_URL.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.cratedDate").value(DEFAULT_CRATED_DATE_STR))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingProductImage() throws Exception {
        // Get the productImage
        restProductImageMockMvc.perform(get("/api/productImages/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductImage() throws Exception {
        // Initialize the database
        productImageRepository.saveAndFlush(productImage);

		int databaseSizeBeforeUpdate = productImageRepository.findAll().size();

        // Update the productImage
        productImage.setImageName(UPDATED_IMAGE_NAME);
        productImage.setImageUrl(UPDATED_IMAGE_URL);
        productImage.setActive(UPDATED_ACTIVE);
        productImage.setCratedDate(UPDATED_CRATED_DATE);
        productImage.setUpdatedDate(UPDATED_UPDATED_DATE);
        

        restProductImageMockMvc.perform(put("/api/productImages")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productImage)))
                .andExpect(status().isOk());

        // Validate the ProductImage in the database
        List<ProductImage> productImages = productImageRepository.findAll();
        assertThat(productImages).hasSize(databaseSizeBeforeUpdate);
        ProductImage testProductImage = productImages.get(productImages.size() - 1);
        assertThat(testProductImage.getImageName()).isEqualTo(UPDATED_IMAGE_NAME);
        assertThat(testProductImage.getImageUrl()).isEqualTo(UPDATED_IMAGE_URL);
        assertThat(testProductImage.getActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testProductImage.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CRATED_DATE);
        assertThat(testProductImage.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void deleteProductImage() throws Exception {
        // Initialize the database
        productImageRepository.saveAndFlush(productImage);

		int databaseSizeBeforeDelete = productImageRepository.findAll().size();

        // Get the productImage
        restProductImageMockMvc.perform(delete("/api/productImages/{id}", productImage.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductImage> productImages = productImageRepository.findAll();
        assertThat(productImages).hasSize(databaseSizeBeforeDelete - 1);
    }
}
