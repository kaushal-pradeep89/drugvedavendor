package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.ProductType;
import com.drugveda.vendor.repository.ProductTypeRepository;
import com.drugveda.vendor.repository.search.ProductTypeSearchRepository;


/**
 * Test class for the ProductTypeResource REST controller.
 *
 * @see ProductTypeResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ProductTypeResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_PRODUCT_TYPE = "SAMPLE_TEXT";
    private static final String UPDATED_PRODUCT_TYPE = "UPDATED_TEXT";

    private static final DateTime DEFAULT_CRATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CRATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CRATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CRATED_DATE);

    private static final DateTime DEFAULT_UPDATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_UPDATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_UPDATED_DATE_STR = dateTimeFormatter.print(DEFAULT_UPDATED_DATE);

    @Inject
    private ProductTypeRepository productTypeRepository;

    @Inject
    private ProductTypeSearchRepository productTypeSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restProductTypeMockMvc;

    private ProductType productType;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProductTypeResource productTypeResource = new ProductTypeResource();
        ReflectionTestUtils.setField(productTypeResource, "productTypeRepository", productTypeRepository);
        ReflectionTestUtils.setField(productTypeResource, "productTypeSearchRepository", productTypeSearchRepository);
        this.restProductTypeMockMvc = MockMvcBuilders.standaloneSetup(productTypeResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        productType = new ProductType();
        productType.setProductType(DEFAULT_PRODUCT_TYPE);
        productType.setCratedDate(DEFAULT_CRATED_DATE);
        productType.setUpdatedDate(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createProductType() throws Exception {
        int databaseSizeBeforeCreate = productTypeRepository.findAll().size();

        // Create the ProductType

        restProductTypeMockMvc.perform(post("/api/productTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productType)))
                .andExpect(status().isCreated());

        // Validate the ProductType in the database
        List<ProductType> productTypes = productTypeRepository.findAll();
        assertThat(productTypes).hasSize(databaseSizeBeforeCreate + 1);
        ProductType testProductType = productTypes.get(productTypes.size() - 1);
        assertThat(testProductType.getProductType()).isEqualTo(DEFAULT_PRODUCT_TYPE);
        assertThat(testProductType.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CRATED_DATE);
        assertThat(testProductType.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProductTypes() throws Exception {
        // Initialize the database
        productTypeRepository.saveAndFlush(productType);

        // Get all the productTypes
        restProductTypeMockMvc.perform(get("/api/productTypes"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(productType.getId().intValue())))
                .andExpect(jsonPath("$.[*].productType").value(hasItem(DEFAULT_PRODUCT_TYPE.toString())))
                .andExpect(jsonPath("$.[*].cratedDate").value(hasItem(DEFAULT_CRATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE_STR)));
    }

    @Test
    @Transactional
    public void getProductType() throws Exception {
        // Initialize the database
        productTypeRepository.saveAndFlush(productType);

        // Get the productType
        restProductTypeMockMvc.perform(get("/api/productTypes/{id}", productType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(productType.getId().intValue()))
            .andExpect(jsonPath("$.productType").value(DEFAULT_PRODUCT_TYPE.toString()))
            .andExpect(jsonPath("$.cratedDate").value(DEFAULT_CRATED_DATE_STR))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingProductType() throws Exception {
        // Get the productType
        restProductTypeMockMvc.perform(get("/api/productTypes/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductType() throws Exception {
        // Initialize the database
        productTypeRepository.saveAndFlush(productType);

		int databaseSizeBeforeUpdate = productTypeRepository.findAll().size();

        // Update the productType
        productType.setProductType(UPDATED_PRODUCT_TYPE);
        productType.setCratedDate(UPDATED_CRATED_DATE);
        productType.setUpdatedDate(UPDATED_UPDATED_DATE);
        

        restProductTypeMockMvc.perform(put("/api/productTypes")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productType)))
                .andExpect(status().isOk());

        // Validate the ProductType in the database
        List<ProductType> productTypes = productTypeRepository.findAll();
        assertThat(productTypes).hasSize(databaseSizeBeforeUpdate);
        ProductType testProductType = productTypes.get(productTypes.size() - 1);
        assertThat(testProductType.getProductType()).isEqualTo(UPDATED_PRODUCT_TYPE);
        assertThat(testProductType.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CRATED_DATE);
        assertThat(testProductType.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void deleteProductType() throws Exception {
        // Initialize the database
        productTypeRepository.saveAndFlush(productType);

		int databaseSizeBeforeDelete = productTypeRepository.findAll().size();

        // Get the productType
        restProductTypeMockMvc.perform(delete("/api/productTypes/{id}", productType.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductType> productTypes = productTypeRepository.findAll();
        assertThat(productTypes).hasSize(databaseSizeBeforeDelete - 1);
    }
}
