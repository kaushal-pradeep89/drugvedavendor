package com.drugveda.vendor.web.rest;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.OrderDetails;
import com.drugveda.vendor.repository.OrderDetailsRepository;
import com.drugveda.vendor.repository.search.OrderDetailsSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the OrderDetailsResource REST controller.
 *
 * @see OrderDetailsResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class OrderDetailsResourceTest {


    private static final Integer DEFAULT_PRODUCT_ID = 1;
    private static final Integer UPDATED_PRODUCT_ID = 2;
    private static final String DEFAULT_PRODUCT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_PRODUCT_NAME = "UPDATED_TEXT";

    private static final Integer DEFAULT_UNITS = 1;
    private static final Integer UPDATED_UNITS = 2;

    private static final Double DEFAULT_RETAIL_PRICE = 1D;
    private static final Double UPDATED_RETAIL_PRICE = 2D;

    private static final Double DEFAULT_WHOLE_SALE_PRICE = 1D;
    private static final Double UPDATED_WHOLE_SALE_PRICE = 2D;

    private static final Double DEFAULT_PURCHASED_PRICE = 1D;
    private static final Double UPDATED_PURCHASED_PRICE = 2D;

    @Inject
    private OrderDetailsRepository orderDetailsRepository;

    @Inject
    private OrderDetailsSearchRepository orderDetailsSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restOrderDetailsMockMvc;

    private OrderDetails orderDetails;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OrderDetailsResource orderDetailsResource = new OrderDetailsResource();
        ReflectionTestUtils.setField(orderDetailsResource, "orderDetailsRepository", orderDetailsRepository);
        ReflectionTestUtils.setField(orderDetailsResource, "orderDetailsSearchRepository", orderDetailsSearchRepository);
        this.restOrderDetailsMockMvc = MockMvcBuilders.standaloneSetup(orderDetailsResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        orderDetails = new OrderDetails();
        orderDetails.setProductId(DEFAULT_PRODUCT_ID);
        orderDetails.setProductName(DEFAULT_PRODUCT_NAME);
        orderDetails.setUnits(DEFAULT_UNITS);
        orderDetails.setRetailPrice(DEFAULT_RETAIL_PRICE);
        orderDetails.setWholeSalePrice(DEFAULT_WHOLE_SALE_PRICE);
        orderDetails.setPurchasedPrice(DEFAULT_PURCHASED_PRICE);
    }

    @Test
    @Transactional
    public void createOrderDetails() throws Exception {
        int databaseSizeBeforeCreate = orderDetailsRepository.findAll().size();

        // Create the OrderDetails

        restOrderDetailsMockMvc.perform(post("/api/orderDetailss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderDetails)))
                .andExpect(status().isCreated());

        // Validate the OrderDetails in the database
        List<OrderDetails> orderDetailss = orderDetailsRepository.findAll();
        assertThat(orderDetailss).hasSize(databaseSizeBeforeCreate + 1);
        OrderDetails testOrderDetails = orderDetailss.get(orderDetailss.size() - 1);
        assertThat(testOrderDetails.getProductId()).isEqualTo(DEFAULT_PRODUCT_ID);
        assertThat(testOrderDetails.getProductName()).isEqualTo(DEFAULT_PRODUCT_NAME);
        assertThat(testOrderDetails.getUnits()).isEqualTo(DEFAULT_UNITS);
        assertThat(testOrderDetails.getRetailPrice()).isEqualTo(DEFAULT_RETAIL_PRICE);
        assertThat(testOrderDetails.getWholeSalePrice()).isEqualTo(DEFAULT_WHOLE_SALE_PRICE);
        assertThat(testOrderDetails.getPurchasedPrice()).isEqualTo(DEFAULT_PURCHASED_PRICE);
    }

    @Test
    @Transactional
    public void getAllOrderDetailss() throws Exception {
        // Initialize the database
        orderDetailsRepository.saveAndFlush(orderDetails);

        // Get all the orderDetailss
        restOrderDetailsMockMvc.perform(get("/api/orderDetailss"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(orderDetails.getId().intValue())))
                .andExpect(jsonPath("$.[*].productId").value(hasItem(DEFAULT_PRODUCT_ID)))
                .andExpect(jsonPath("$.[*].productName").value(hasItem(DEFAULT_PRODUCT_NAME.toString())))
                .andExpect(jsonPath("$.[*].units").value(hasItem(DEFAULT_UNITS)))
                .andExpect(jsonPath("$.[*].retailPrice").value(hasItem(DEFAULT_RETAIL_PRICE.doubleValue())))
                .andExpect(jsonPath("$.[*].wholeSalePrice").value(hasItem(DEFAULT_WHOLE_SALE_PRICE.doubleValue())))
                .andExpect(jsonPath("$.[*].purchasedPrice").value(hasItem(DEFAULT_PURCHASED_PRICE.doubleValue())));
    }

    @Test
    @Transactional
    public void getOrderDetails() throws Exception {
        // Initialize the database
        orderDetailsRepository.saveAndFlush(orderDetails);

        // Get the orderDetails
        restOrderDetailsMockMvc.perform(get("/api/orderDetailss/{id}", orderDetails.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(orderDetails.getId().intValue()))
            .andExpect(jsonPath("$.productId").value(DEFAULT_PRODUCT_ID))
            .andExpect(jsonPath("$.productName").value(DEFAULT_PRODUCT_NAME.toString()))
            .andExpect(jsonPath("$.units").value(DEFAULT_UNITS))
            .andExpect(jsonPath("$.retailPrice").value(DEFAULT_RETAIL_PRICE.doubleValue()))
            .andExpect(jsonPath("$.wholeSalePrice").value(DEFAULT_WHOLE_SALE_PRICE.doubleValue()))
            .andExpect(jsonPath("$.purchasedPrice").value(DEFAULT_PURCHASED_PRICE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOrderDetails() throws Exception {
        // Get the orderDetails
        restOrderDetailsMockMvc.perform(get("/api/orderDetailss/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrderDetails() throws Exception {
        // Initialize the database
        orderDetailsRepository.saveAndFlush(orderDetails);

		int databaseSizeBeforeUpdate = orderDetailsRepository.findAll().size();

        // Update the orderDetails
        orderDetails.setProductId(UPDATED_PRODUCT_ID);
        orderDetails.setProductName(UPDATED_PRODUCT_NAME);
        orderDetails.setUnits(UPDATED_UNITS);
        orderDetails.setRetailPrice(UPDATED_RETAIL_PRICE);
        orderDetails.setWholeSalePrice(UPDATED_WHOLE_SALE_PRICE);
        orderDetails.setPurchasedPrice(UPDATED_PURCHASED_PRICE);
        

        restOrderDetailsMockMvc.perform(put("/api/orderDetailss")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(orderDetails)))
                .andExpect(status().isOk());

        // Validate the OrderDetails in the database
        List<OrderDetails> orderDetailss = orderDetailsRepository.findAll();
        assertThat(orderDetailss).hasSize(databaseSizeBeforeUpdate);
        OrderDetails testOrderDetails = orderDetailss.get(orderDetailss.size() - 1);
        assertThat(testOrderDetails.getProductId()).isEqualTo(UPDATED_PRODUCT_ID);
        assertThat(testOrderDetails.getProductName()).isEqualTo(UPDATED_PRODUCT_NAME);
        assertThat(testOrderDetails.getUnits()).isEqualTo(UPDATED_UNITS);
        assertThat(testOrderDetails.getRetailPrice()).isEqualTo(UPDATED_RETAIL_PRICE);
        assertThat(testOrderDetails.getWholeSalePrice()).isEqualTo(UPDATED_WHOLE_SALE_PRICE);
        assertThat(testOrderDetails.getPurchasedPrice()).isEqualTo(UPDATED_PURCHASED_PRICE);
    }

    @Test
    @Transactional
    public void deleteOrderDetails() throws Exception {
        // Initialize the database
        orderDetailsRepository.saveAndFlush(orderDetails);

		int databaseSizeBeforeDelete = orderDetailsRepository.findAll().size();

        // Get the orderDetails
        restOrderDetailsMockMvc.perform(delete("/api/orderDetailss/{id}", orderDetails.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<OrderDetails> orderDetailss = orderDetailsRepository.findAll();
        assertThat(orderDetailss).hasSize(databaseSizeBeforeDelete - 1);
    }
}
