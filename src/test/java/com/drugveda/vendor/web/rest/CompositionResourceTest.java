package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.Composition;
import com.drugveda.vendor.repository.CompositionRepository;
import com.drugveda.vendor.repository.search.CompositionSearchRepository;


/**
 * Test class for the CompositionResource REST controller.
 *
 * @see CompositionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CompositionResourceTest {

    private static final String DEFAULT_COMPOSITION = "SAMPLE_TEXT";
    private static final String UPDATED_COMPOSITION = "UPDATED_TEXT";
    private static final String DEFAULT_DESCRIPTION = "SAMPLE_TEXT";
    private static final String UPDATED_DESCRIPTION = "UPDATED_TEXT";

    @Inject
    private CompositionRepository compositionRepository;

    @Inject
    private CompositionSearchRepository compositionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restCompositionMockMvc;

    private Composition composition;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CompositionResource compositionResource = new CompositionResource();
        ReflectionTestUtils.setField(compositionResource, "compositionRepository", compositionRepository);
        ReflectionTestUtils.setField(compositionResource, "compositionSearchRepository", compositionSearchRepository);
        this.restCompositionMockMvc = MockMvcBuilders.standaloneSetup(compositionResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        composition = new Composition();
        composition.setComposition(DEFAULT_COMPOSITION);
        composition.setDescription(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createComposition() throws Exception {
        int databaseSizeBeforeCreate = compositionRepository.findAll().size();

        // Create the Composition

        restCompositionMockMvc.perform(post("/api/compositions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(composition)))
                .andExpect(status().isCreated());

        // Validate the Composition in the database
        List<Composition> compositions = compositionRepository.findAll();
        assertThat(compositions).hasSize(databaseSizeBeforeCreate + 1);
        Composition testComposition = compositions.get(compositions.size() - 1);
        assertThat(testComposition.getComposition()).isEqualTo(DEFAULT_COMPOSITION);
        assertThat(testComposition.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllCompositions() throws Exception {
        // Initialize the database
        compositionRepository.saveAndFlush(composition);

        // Get all the compositions
        restCompositionMockMvc.perform(get("/api/compositions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(composition.getId().intValue())))
                .andExpect(jsonPath("$.[*].composition").value(hasItem(DEFAULT_COMPOSITION.toString())))
                .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    @Transactional
    public void getComposition() throws Exception {
        // Initialize the database
        compositionRepository.saveAndFlush(composition);

        // Get the composition
        restCompositionMockMvc.perform(get("/api/compositions/{id}", composition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(composition.getId().intValue()))
            .andExpect(jsonPath("$.composition").value(DEFAULT_COMPOSITION.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingComposition() throws Exception {
        // Get the composition
        restCompositionMockMvc.perform(get("/api/compositions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateComposition() throws Exception {
        // Initialize the database
        compositionRepository.saveAndFlush(composition);

		int databaseSizeBeforeUpdate = compositionRepository.findAll().size();

        // Update the composition
        composition.setComposition(UPDATED_COMPOSITION);
        composition.setDescription(UPDATED_DESCRIPTION);
        

        restCompositionMockMvc.perform(put("/api/compositions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(composition)))
                .andExpect(status().isOk());

        // Validate the Composition in the database
        List<Composition> compositions = compositionRepository.findAll();
        assertThat(compositions).hasSize(databaseSizeBeforeUpdate);
        Composition testComposition = compositions.get(compositions.size() - 1);
        assertThat(testComposition.getComposition()).isEqualTo(UPDATED_COMPOSITION);
        assertThat(testComposition.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void deleteComposition() throws Exception {
        // Initialize the database
        compositionRepository.saveAndFlush(composition);

		int databaseSizeBeforeDelete = compositionRepository.findAll().size();

        // Get the composition
        restCompositionMockMvc.perform(delete("/api/compositions/{id}", composition.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Composition> compositions = compositionRepository.findAll();
        assertThat(compositions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
