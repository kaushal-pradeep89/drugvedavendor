package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.ProductVariation;
import com.drugveda.vendor.domain.enumeration.MeasureType;
import com.drugveda.vendor.repository.ProductVariationRepository;
import com.drugveda.vendor.repository.search.ProductVariationSearchRepository;

/**
 * Test class for the ProductVariationResource REST controller.
 *
 * @see ProductVariationResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ProductVariationResourceTest {


    private static final MeasureType DEFAULT_MEASURE_TYPE = MeasureType.MG;
    private static final MeasureType UPDATED_MEASURE_TYPE = MeasureType.ML;

    @Inject
    private ProductVariationRepository productVariationRepository;

    @Inject
    private ProductVariationSearchRepository productVariationSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restProductVariationMockMvc;

    private ProductVariation productVariation;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProductVariationResource productVariationResource = new ProductVariationResource();
        ReflectionTestUtils.setField(productVariationResource, "productVariationRepository", productVariationRepository);
        ReflectionTestUtils.setField(productVariationResource, "productVariationSearchRepository", productVariationSearchRepository);
        this.restProductVariationMockMvc = MockMvcBuilders.standaloneSetup(productVariationResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        productVariation = new ProductVariation();
        productVariation.setMeasureType(DEFAULT_MEASURE_TYPE);
    }

    @Test
    @Transactional
    public void createProductVariation() throws Exception {
        int databaseSizeBeforeCreate = productVariationRepository.findAll().size();

        // Create the ProductVariation

        restProductVariationMockMvc.perform(post("/api/productVariations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productVariation)))
                .andExpect(status().isCreated());

        // Validate the ProductVariation in the database
        List<ProductVariation> productVariations = productVariationRepository.findAll();
        assertThat(productVariations).hasSize(databaseSizeBeforeCreate + 1);
        ProductVariation testProductVariation = productVariations.get(productVariations.size() - 1);
        assertThat(testProductVariation.getMeasureType()).isEqualTo(DEFAULT_MEASURE_TYPE);
    }

    @Test
    @Transactional
    public void getAllProductVariations() throws Exception {
        // Initialize the database
        productVariationRepository.saveAndFlush(productVariation);

        // Get all the productVariations
        restProductVariationMockMvc.perform(get("/api/productVariations"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(productVariation.getId().intValue())))
                .andExpect(jsonPath("$.[*].measureType").value(hasItem(DEFAULT_MEASURE_TYPE.toString())));
    }

    @Test
    @Transactional
    public void getProductVariation() throws Exception {
        // Initialize the database
        productVariationRepository.saveAndFlush(productVariation);

        // Get the productVariation
        restProductVariationMockMvc.perform(get("/api/productVariations/{id}", productVariation.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(productVariation.getId().intValue()))
            .andExpect(jsonPath("$.measureType").value(DEFAULT_MEASURE_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingProductVariation() throws Exception {
        // Get the productVariation
        restProductVariationMockMvc.perform(get("/api/productVariations/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductVariation() throws Exception {
        // Initialize the database
        productVariationRepository.saveAndFlush(productVariation);

		int databaseSizeBeforeUpdate = productVariationRepository.findAll().size();

        // Update the productVariation
        productVariation.setMeasureType(UPDATED_MEASURE_TYPE);
        

        restProductVariationMockMvc.perform(put("/api/productVariations")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productVariation)))
                .andExpect(status().isOk());

        // Validate the ProductVariation in the database
        List<ProductVariation> productVariations = productVariationRepository.findAll();
        assertThat(productVariations).hasSize(databaseSizeBeforeUpdate);
        ProductVariation testProductVariation = productVariations.get(productVariations.size() - 1);
        assertThat(testProductVariation.getMeasureType()).isEqualTo(UPDATED_MEASURE_TYPE);
    }

    @Test
    @Transactional
    public void deleteProductVariation() throws Exception {
        // Initialize the database
        productVariationRepository.saveAndFlush(productVariation);

		int databaseSizeBeforeDelete = productVariationRepository.findAll().size();

        // Get the productVariation
        restProductVariationMockMvc.perform(delete("/api/productVariations/{id}", productVariation.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductVariation> productVariations = productVariationRepository.findAll();
        assertThat(productVariations).hasSize(databaseSizeBeforeDelete - 1);
    }
}
