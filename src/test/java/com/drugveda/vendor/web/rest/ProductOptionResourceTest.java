package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.ProductOption;
import com.drugveda.vendor.domain.enumeration.MeasureType;
import com.drugveda.vendor.repository.ProductOptionRepository;
import com.drugveda.vendor.repository.search.ProductOptionSearchRepository;

/**
 * Test class for the ProductOptionResource REST controller.
 *
 * @see ProductOptionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ProductOptionResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final MeasureType DEFAULT_MEASURE_TYPE = MeasureType.MG;
    private static final MeasureType UPDATED_MEASURE_TYPE = MeasureType.ML;

    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final DateTime DEFAULT_CRATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CRATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CRATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CRATED_DATE);

    private static final DateTime DEFAULT_UPDATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_UPDATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_UPDATED_DATE_STR = dateTimeFormatter.print(DEFAULT_UPDATED_DATE);

    @Inject
    private ProductOptionRepository productOptionRepository;

    @Inject
    private ProductOptionSearchRepository productOptionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restProductOptionMockMvc;

    private ProductOption productOption;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProductOptionResource productOptionResource = new ProductOptionResource();
        ReflectionTestUtils.setField(productOptionResource, "productOptionRepository", productOptionRepository);
        ReflectionTestUtils.setField(productOptionResource, "productOptionSearchRepository", productOptionSearchRepository);
        this.restProductOptionMockMvc = MockMvcBuilders.standaloneSetup(productOptionResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        productOption = new ProductOption();
        productOption.setMeasureType(DEFAULT_MEASURE_TYPE);
        productOption.setQuantity(DEFAULT_QUANTITY);
        productOption.setActive(DEFAULT_ACTIVE);
        productOption.setCratedDate(DEFAULT_CRATED_DATE);
        productOption.setUpdatedDate(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createProductOption() throws Exception {
        int databaseSizeBeforeCreate = productOptionRepository.findAll().size();

        // Create the ProductOption

        restProductOptionMockMvc.perform(post("/api/productOptions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productOption)))
                .andExpect(status().isCreated());

        // Validate the ProductOption in the database
        List<ProductOption> productOptions = productOptionRepository.findAll();
        assertThat(productOptions).hasSize(databaseSizeBeforeCreate + 1);
        ProductOption testProductOption = productOptions.get(productOptions.size() - 1);
        assertThat(testProductOption.getMeasureType()).isEqualTo(DEFAULT_MEASURE_TYPE);
        assertThat(testProductOption.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testProductOption.getActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testProductOption.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CRATED_DATE);
        assertThat(testProductOption.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProductOptions() throws Exception {
        // Initialize the database
        productOptionRepository.saveAndFlush(productOption);

        // Get all the productOptions
        restProductOptionMockMvc.perform(get("/api/productOptions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(productOption.getId().intValue())))
                .andExpect(jsonPath("$.[*].measureType").value(hasItem(DEFAULT_MEASURE_TYPE.toString())))
                .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
                .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].cratedDate").value(hasItem(DEFAULT_CRATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE_STR)));
    }

    @Test
    @Transactional
    public void getProductOption() throws Exception {
        // Initialize the database
        productOptionRepository.saveAndFlush(productOption);

        // Get the productOption
        restProductOptionMockMvc.perform(get("/api/productOptions/{id}", productOption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(productOption.getId().intValue()))
            .andExpect(jsonPath("$.measureType").value(DEFAULT_MEASURE_TYPE.toString()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.cratedDate").value(DEFAULT_CRATED_DATE_STR))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingProductOption() throws Exception {
        // Get the productOption
        restProductOptionMockMvc.perform(get("/api/productOptions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductOption() throws Exception {
        // Initialize the database
        productOptionRepository.saveAndFlush(productOption);

		int databaseSizeBeforeUpdate = productOptionRepository.findAll().size();

        // Update the productOption
        productOption.setMeasureType(UPDATED_MEASURE_TYPE);
        productOption.setQuantity(UPDATED_QUANTITY);
        productOption.setActive(UPDATED_ACTIVE);
        productOption.setCratedDate(UPDATED_CRATED_DATE);
        productOption.setUpdatedDate(UPDATED_UPDATED_DATE);
        

        restProductOptionMockMvc.perform(put("/api/productOptions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productOption)))
                .andExpect(status().isOk());

        // Validate the ProductOption in the database
        List<ProductOption> productOptions = productOptionRepository.findAll();
        assertThat(productOptions).hasSize(databaseSizeBeforeUpdate);
        ProductOption testProductOption = productOptions.get(productOptions.size() - 1);
        assertThat(testProductOption.getMeasureType()).isEqualTo(UPDATED_MEASURE_TYPE);
        assertThat(testProductOption.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testProductOption.getActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testProductOption.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CRATED_DATE);
        assertThat(testProductOption.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void deleteProductOption() throws Exception {
        // Initialize the database
        productOptionRepository.saveAndFlush(productOption);

		int databaseSizeBeforeDelete = productOptionRepository.findAll().size();

        // Get the productOption
        restProductOptionMockMvc.perform(delete("/api/productOptions/{id}", productOption.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductOption> productOptions = productOptionRepository.findAll();
        assertThat(productOptions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
