package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.ProductCategory;
import com.drugveda.vendor.repository.ProductCategoryRepository;
import com.drugveda.vendor.repository.search.ProductCategorySearchRepository;


/**
 * Test class for the ProductCategoryResource REST controller.
 *
 * @see ProductCategoryResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class ProductCategoryResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_CATEGORY_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_CATEGORY_NAME = "UPDATED_TEXT";
    private static final String DEFAULT_CATEGORY_DESCRIPTION = "SAMPLE_TEXT";
    private static final String UPDATED_CATEGORY_DESCRIPTION = "UPDATED_TEXT";

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final DateTime DEFAULT_CRATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CRATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CRATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CRATED_DATE);

    private static final DateTime DEFAULT_UPDATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_UPDATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_UPDATED_DATE_STR = dateTimeFormatter.print(DEFAULT_UPDATED_DATE);

    @Inject
    private ProductCategoryRepository productCategoryRepository;

    @Inject
    private ProductCategorySearchRepository productCategorySearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restProductCategoryMockMvc;

    private ProductCategory productCategory;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        ProductCategoryResource productCategoryResource = new ProductCategoryResource();
        ReflectionTestUtils.setField(productCategoryResource, "productCategoryRepository", productCategoryRepository);
        ReflectionTestUtils.setField(productCategoryResource, "productCategorySearchRepository", productCategorySearchRepository);
        this.restProductCategoryMockMvc = MockMvcBuilders.standaloneSetup(productCategoryResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        productCategory = new ProductCategory();
        productCategory.setCategoryName(DEFAULT_CATEGORY_NAME);
        productCategory.setCategoryDescription(DEFAULT_CATEGORY_DESCRIPTION);
        productCategory.setActive(DEFAULT_ACTIVE);
        productCategory.setCratedDate(DEFAULT_CRATED_DATE);
        productCategory.setUpdatedDate(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createProductCategory() throws Exception {
        int databaseSizeBeforeCreate = productCategoryRepository.findAll().size();

        // Create the ProductCategory

        restProductCategoryMockMvc.perform(post("/api/productCategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productCategory)))
                .andExpect(status().isCreated());

        // Validate the ProductCategory in the database
        List<ProductCategory> productCategorys = productCategoryRepository.findAll();
        assertThat(productCategorys).hasSize(databaseSizeBeforeCreate + 1);
        ProductCategory testProductCategory = productCategorys.get(productCategorys.size() - 1);
        assertThat(testProductCategory.getCategoryName()).isEqualTo(DEFAULT_CATEGORY_NAME);
        assertThat(testProductCategory.getCategoryDescription()).isEqualTo(DEFAULT_CATEGORY_DESCRIPTION);
        assertThat(testProductCategory.getActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testProductCategory.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CRATED_DATE);
        assertThat(testProductCategory.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllProductCategorys() throws Exception {
        // Initialize the database
        productCategoryRepository.saveAndFlush(productCategory);

        // Get all the productCategorys
        restProductCategoryMockMvc.perform(get("/api/productCategorys"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(productCategory.getId().intValue())))
                .andExpect(jsonPath("$.[*].categoryName").value(hasItem(DEFAULT_CATEGORY_NAME.toString())))
                .andExpect(jsonPath("$.[*].categoryDescription").value(hasItem(DEFAULT_CATEGORY_DESCRIPTION.toString())))
                .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
                .andExpect(jsonPath("$.[*].cratedDate").value(hasItem(DEFAULT_CRATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE_STR)));
    }

    @Test
    @Transactional
    public void getProductCategory() throws Exception {
        // Initialize the database
        productCategoryRepository.saveAndFlush(productCategory);

        // Get the productCategory
        restProductCategoryMockMvc.perform(get("/api/productCategorys/{id}", productCategory.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(productCategory.getId().intValue()))
            .andExpect(jsonPath("$.categoryName").value(DEFAULT_CATEGORY_NAME.toString()))
            .andExpect(jsonPath("$.categoryDescription").value(DEFAULT_CATEGORY_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.cratedDate").value(DEFAULT_CRATED_DATE_STR))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingProductCategory() throws Exception {
        // Get the productCategory
        restProductCategoryMockMvc.perform(get("/api/productCategorys/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateProductCategory() throws Exception {
        // Initialize the database
        productCategoryRepository.saveAndFlush(productCategory);

		int databaseSizeBeforeUpdate = productCategoryRepository.findAll().size();

        // Update the productCategory
        productCategory.setCategoryName(UPDATED_CATEGORY_NAME);
        productCategory.setCategoryDescription(UPDATED_CATEGORY_DESCRIPTION);
        productCategory.setActive(UPDATED_ACTIVE);
        productCategory.setCratedDate(UPDATED_CRATED_DATE);
        productCategory.setUpdatedDate(UPDATED_UPDATED_DATE);
        

        restProductCategoryMockMvc.perform(put("/api/productCategorys")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(productCategory)))
                .andExpect(status().isOk());

        // Validate the ProductCategory in the database
        List<ProductCategory> productCategorys = productCategoryRepository.findAll();
        assertThat(productCategorys).hasSize(databaseSizeBeforeUpdate);
        ProductCategory testProductCategory = productCategorys.get(productCategorys.size() - 1);
        assertThat(testProductCategory.getCategoryName()).isEqualTo(UPDATED_CATEGORY_NAME);
        assertThat(testProductCategory.getCategoryDescription()).isEqualTo(UPDATED_CATEGORY_DESCRIPTION);
        assertThat(testProductCategory.getActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testProductCategory.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CRATED_DATE);
        assertThat(testProductCategory.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void deleteProductCategory() throws Exception {
        // Initialize the database
        productCategoryRepository.saveAndFlush(productCategory);

		int databaseSizeBeforeDelete = productCategoryRepository.findAll().size();

        // Get the productCategory
        restProductCategoryMockMvc.perform(delete("/api/productCategorys/{id}", productCategory.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<ProductCategory> productCategorys = productCategoryRepository.findAll();
        assertThat(productCategorys).hasSize(databaseSizeBeforeDelete - 1);
    }
}
