package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.Vendor;
import com.drugveda.vendor.repository.VendorRepository;
import com.drugveda.vendor.repository.search.VendorSearchRepository;


/**
 * Test class for the VendorResource REST controller.
 *
 * @see VendorResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class VendorResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

    private static final String DEFAULT_VENDOR_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_VENDOR_NAME = "UPDATED_TEXT";
    private static final String DEFAULT_PHONE_NO = "SAMPLE_TEXT";
    private static final String UPDATED_PHONE_NO = "UPDATED_TEXT";
    private static final String DEFAULT_MOBIBLE_NO = "SAMPLE_TEXT";
    private static final String UPDATED_MOBIBLE_NO = "UPDATED_TEXT";

    private static final DateTime DEFAULT_CRATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CRATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CRATED_DATE_STR = dateTimeFormatter.print(DEFAULT_CRATED_DATE);

    private static final DateTime DEFAULT_UPDATED_DATE = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_UPDATED_DATE = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_UPDATED_DATE_STR = dateTimeFormatter.print(DEFAULT_UPDATED_DATE);

    @Inject
    private VendorRepository vendorRepository;

    @Inject
    private VendorSearchRepository vendorSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restVendorMockMvc;

    private Vendor vendor;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VendorResource vendorResource = new VendorResource();
        ReflectionTestUtils.setField(vendorResource, "vendorRepository", vendorRepository);
        ReflectionTestUtils.setField(vendorResource, "vendorSearchRepository", vendorSearchRepository);
        this.restVendorMockMvc = MockMvcBuilders.standaloneSetup(vendorResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        vendor = new Vendor();
        vendor.setVendorName(DEFAULT_VENDOR_NAME);
        vendor.setPhoneNo(DEFAULT_PHONE_NO);
        vendor.setMobibleNo(DEFAULT_MOBIBLE_NO);
        vendor.setCratedDate(DEFAULT_CRATED_DATE);
        vendor.setUpdatedDate(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void createVendor() throws Exception {
        int databaseSizeBeforeCreate = vendorRepository.findAll().size();

        // Create the Vendor

        restVendorMockMvc.perform(post("/api/vendors")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vendor)))
                .andExpect(status().isCreated());

        // Validate the Vendor in the database
        List<Vendor> vendors = vendorRepository.findAll();
        assertThat(vendors).hasSize(databaseSizeBeforeCreate + 1);
        Vendor testVendor = vendors.get(vendors.size() - 1);
        assertThat(testVendor.getVendorName()).isEqualTo(DEFAULT_VENDOR_NAME);
        assertThat(testVendor.getPhoneNo()).isEqualTo(DEFAULT_PHONE_NO);
        assertThat(testVendor.getMobibleNo()).isEqualTo(DEFAULT_MOBIBLE_NO);
        assertThat(testVendor.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CRATED_DATE);
        assertThat(testVendor.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void getAllVendors() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

        // Get all the vendors
        restVendorMockMvc.perform(get("/api/vendors"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(vendor.getId().intValue())))
                .andExpect(jsonPath("$.[*].vendorName").value(hasItem(DEFAULT_VENDOR_NAME.toString())))
                .andExpect(jsonPath("$.[*].phoneNo").value(hasItem(DEFAULT_PHONE_NO.toString())))
                .andExpect(jsonPath("$.[*].mobibleNo").value(hasItem(DEFAULT_MOBIBLE_NO.toString())))
                .andExpect(jsonPath("$.[*].cratedDate").value(hasItem(DEFAULT_CRATED_DATE_STR)))
                .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE_STR)));
    }

    @Test
    @Transactional
    public void getVendor() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

        // Get the vendor
        restVendorMockMvc.perform(get("/api/vendors/{id}", vendor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(vendor.getId().intValue()))
            .andExpect(jsonPath("$.vendorName").value(DEFAULT_VENDOR_NAME.toString()))
            .andExpect(jsonPath("$.phoneNo").value(DEFAULT_PHONE_NO.toString()))
            .andExpect(jsonPath("$.mobibleNo").value(DEFAULT_MOBIBLE_NO.toString()))
            .andExpect(jsonPath("$.cratedDate").value(DEFAULT_CRATED_DATE_STR))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE_STR));
    }

    @Test
    @Transactional
    public void getNonExistingVendor() throws Exception {
        // Get the vendor
        restVendorMockMvc.perform(get("/api/vendors/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVendor() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

		int databaseSizeBeforeUpdate = vendorRepository.findAll().size();

        // Update the vendor
        vendor.setVendorName(UPDATED_VENDOR_NAME);
        vendor.setPhoneNo(UPDATED_PHONE_NO);
        vendor.setMobibleNo(UPDATED_MOBIBLE_NO);
        vendor.setCratedDate(UPDATED_CRATED_DATE);
        vendor.setUpdatedDate(UPDATED_UPDATED_DATE);
        

        restVendorMockMvc.perform(put("/api/vendors")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(vendor)))
                .andExpect(status().isOk());

        // Validate the Vendor in the database
        List<Vendor> vendors = vendorRepository.findAll();
        assertThat(vendors).hasSize(databaseSizeBeforeUpdate);
        Vendor testVendor = vendors.get(vendors.size() - 1);
        assertThat(testVendor.getVendorName()).isEqualTo(UPDATED_VENDOR_NAME);
        assertThat(testVendor.getPhoneNo()).isEqualTo(UPDATED_PHONE_NO);
        assertThat(testVendor.getMobibleNo()).isEqualTo(UPDATED_MOBIBLE_NO);
        assertThat(testVendor.getCratedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CRATED_DATE);
        assertThat(testVendor.getUpdatedDate().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_UPDATED_DATE);
    }

    @Test
    @Transactional
    public void deleteVendor() throws Exception {
        // Initialize the database
        vendorRepository.saveAndFlush(vendor);

		int databaseSizeBeforeDelete = vendorRepository.findAll().size();

        // Get the vendor
        restVendorMockMvc.perform(delete("/api/vendors/{id}", vendor.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Vendor> vendors = vendorRepository.findAll();
        assertThat(vendors).hasSize(databaseSizeBeforeDelete - 1);
    }
}
