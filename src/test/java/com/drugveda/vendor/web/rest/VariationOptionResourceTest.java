package com.drugveda.vendor.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.StrictAssertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.VariationOption;
import com.drugveda.vendor.repository.VariationOptionRepository;
import com.drugveda.vendor.repository.search.VariationOptionSearchRepository;


/**
 * Test class for the VariationOptionResource REST controller.
 *
 * @see VariationOptionResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class VariationOptionResourceTest {


    private static final Integer DEFAULT_QUANITY = 1;
    private static final Integer UPDATED_QUANITY = 2;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    @Inject
    private VariationOptionRepository variationOptionRepository;

    @Inject
    private VariationOptionSearchRepository variationOptionSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restVariationOptionMockMvc;

    private VariationOption variationOption;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        VariationOptionResource variationOptionResource = new VariationOptionResource();
        ReflectionTestUtils.setField(variationOptionResource, "variationOptionRepository", variationOptionRepository);
        ReflectionTestUtils.setField(variationOptionResource, "variationOptionSearchRepository", variationOptionSearchRepository);
        this.restVariationOptionMockMvc = MockMvcBuilders.standaloneSetup(variationOptionResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        variationOption = new VariationOption();
        variationOption.setQuanity(DEFAULT_QUANITY);
        variationOption.setActive(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void createVariationOption() throws Exception {
        int databaseSizeBeforeCreate = variationOptionRepository.findAll().size();

        // Create the VariationOption

        restVariationOptionMockMvc.perform(post("/api/variationOptions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(variationOption)))
                .andExpect(status().isCreated());

        // Validate the VariationOption in the database
        List<VariationOption> variationOptions = variationOptionRepository.findAll();
        assertThat(variationOptions).hasSize(databaseSizeBeforeCreate + 1);
        VariationOption testVariationOption = variationOptions.get(variationOptions.size() - 1);
        assertThat(testVariationOption.getQuanity()).isEqualTo(DEFAULT_QUANITY);
        assertThat(testVariationOption.getActive()).isEqualTo(DEFAULT_ACTIVE);
    }

    @Test
    @Transactional
    public void getAllVariationOptions() throws Exception {
        // Initialize the database
        variationOptionRepository.saveAndFlush(variationOption);

        // Get all the variationOptions
        restVariationOptionMockMvc.perform(get("/api/variationOptions"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(variationOption.getId().intValue())))
                .andExpect(jsonPath("$.[*].quanity").value(hasItem(DEFAULT_QUANITY)))
                .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())));
    }

    @Test
    @Transactional
    public void getVariationOption() throws Exception {
        // Initialize the database
        variationOptionRepository.saveAndFlush(variationOption);

        // Get the variationOption
        restVariationOptionMockMvc.perform(get("/api/variationOptions/{id}", variationOption.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(variationOption.getId().intValue()))
            .andExpect(jsonPath("$.quanity").value(DEFAULT_QUANITY))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()));
    }

    @Test
    @Transactional
    public void getNonExistingVariationOption() throws Exception {
        // Get the variationOption
        restVariationOptionMockMvc.perform(get("/api/variationOptions/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateVariationOption() throws Exception {
        // Initialize the database
        variationOptionRepository.saveAndFlush(variationOption);

		int databaseSizeBeforeUpdate = variationOptionRepository.findAll().size();

        // Update the variationOption
        variationOption.setQuanity(UPDATED_QUANITY);
        variationOption.setActive(UPDATED_ACTIVE);
        

        restVariationOptionMockMvc.perform(put("/api/variationOptions")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(variationOption)))
                .andExpect(status().isOk());

        // Validate the VariationOption in the database
        List<VariationOption> variationOptions = variationOptionRepository.findAll();
        assertThat(variationOptions).hasSize(databaseSizeBeforeUpdate);
        VariationOption testVariationOption = variationOptions.get(variationOptions.size() - 1);
        assertThat(testVariationOption.getQuanity()).isEqualTo(UPDATED_QUANITY);
        assertThat(testVariationOption.getActive()).isEqualTo(UPDATED_ACTIVE);
    }

    @Test
    @Transactional
    public void deleteVariationOption() throws Exception {
        // Initialize the database
        variationOptionRepository.saveAndFlush(variationOption);

		int databaseSizeBeforeDelete = variationOptionRepository.findAll().size();

        // Get the variationOption
        restVariationOptionMockMvc.perform(delete("/api/variationOptions/{id}", variationOption.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<VariationOption> variationOptions = variationOptionRepository.findAll();
        assertThat(variationOptions).hasSize(databaseSizeBeforeDelete - 1);
    }
}
