package com.drugveda.vendor.web.rest;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.Order;
import com.drugveda.vendor.repository.OrderRepository;
import com.drugveda.vendor.repository.search.OrderSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the OrderResource REST controller.
 *
 * @see OrderResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class OrderResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final DateTime DEFAULT_CREATED = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_CREATED = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_CREATED_STR = dateTimeFormatter.print(DEFAULT_CREATED);

    private static final Double DEFAULT_TOTAL_AMOUNT = 1D;
    private static final Double UPDATED_TOTAL_AMOUNT = 2D;

    private static final Double DEFAULT_DISCOUNT = 1D;
    private static final Double UPDATED_DISCOUNT = 2D;
    private static final String DEFAULT_DISCOUNT_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_DISCOUNT_NAME = "UPDATED_TEXT";

    private static final Integer DEFAULT_PRODUCT_UNITS = 1;
    private static final Integer UPDATED_PRODUCT_UNITS = 2;
    private static final String DEFAULT_TAX_NAME = "SAMPLE_TEXT";
    private static final String UPDATED_TAX_NAME = "UPDATED_TEXT";

    private static final Double DEFAULT_TAX_RATE = 1D;
    private static final Double UPDATED_TAX_RATE = 2D;

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private OrderSearchRepository orderSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restOrderMockMvc;

    private Order order;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        OrderResource orderResource = new OrderResource();
        ReflectionTestUtils.setField(orderResource, "orderRepository", orderRepository);
        ReflectionTestUtils.setField(orderResource, "orderSearchRepository", orderSearchRepository);
        this.restOrderMockMvc = MockMvcBuilders.standaloneSetup(orderResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        order = new Order();
        order.setCreated(DEFAULT_CREATED);
        order.setTotalAmount(DEFAULT_TOTAL_AMOUNT);
        order.setDiscount(DEFAULT_DISCOUNT);
        order.setDiscountName(DEFAULT_DISCOUNT_NAME);
        order.setProductUnits(DEFAULT_PRODUCT_UNITS);
        order.setTaxName(DEFAULT_TAX_NAME);
        order.setTaxRate(DEFAULT_TAX_RATE);
    }

    @Test
    @Transactional
    public void createOrder() throws Exception {
        int databaseSizeBeforeCreate = orderRepository.findAll().size();

        // Create the Order

        restOrderMockMvc.perform(post("/api/orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(order)))
                .andExpect(status().isCreated());

        // Validate the Order in the database
        List<Order> orders = orderRepository.findAll();
        assertThat(orders).hasSize(databaseSizeBeforeCreate + 1);
        Order testOrder = orders.get(orders.size() - 1);
        assertThat(testOrder.getCreated().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_CREATED);
        assertThat(testOrder.getTotalAmount()).isEqualTo(DEFAULT_TOTAL_AMOUNT);
        assertThat(testOrder.getDiscount()).isEqualTo(DEFAULT_DISCOUNT);
        assertThat(testOrder.getDiscountName()).isEqualTo(DEFAULT_DISCOUNT_NAME);
        assertThat(testOrder.getProductUnits()).isEqualTo(DEFAULT_PRODUCT_UNITS);
        assertThat(testOrder.getTaxName()).isEqualTo(DEFAULT_TAX_NAME);
        assertThat(testOrder.getTaxRate()).isEqualTo(DEFAULT_TAX_RATE);
    }

    @Test
    @Transactional
    public void getAllOrders() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get all the orders
        restOrderMockMvc.perform(get("/api/orders"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(order.getId().intValue())))
                .andExpect(jsonPath("$.[*].created").value(hasItem(DEFAULT_CREATED_STR)))
                .andExpect(jsonPath("$.[*].totalAmount").value(hasItem(DEFAULT_TOTAL_AMOUNT.doubleValue())))
                .andExpect(jsonPath("$.[*].discount").value(hasItem(DEFAULT_DISCOUNT.doubleValue())))
                .andExpect(jsonPath("$.[*].discountName").value(hasItem(DEFAULT_DISCOUNT_NAME.toString())))
                .andExpect(jsonPath("$.[*].productUnits").value(hasItem(DEFAULT_PRODUCT_UNITS)))
                .andExpect(jsonPath("$.[*].taxName").value(hasItem(DEFAULT_TAX_NAME.toString())))
                .andExpect(jsonPath("$.[*].taxRate").value(hasItem(DEFAULT_TAX_RATE.doubleValue())));
    }

    @Test
    @Transactional
    public void getOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

        // Get the order
        restOrderMockMvc.perform(get("/api/orders/{id}", order.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(order.getId().intValue()))
            .andExpect(jsonPath("$.created").value(DEFAULT_CREATED_STR))
            .andExpect(jsonPath("$.totalAmount").value(DEFAULT_TOTAL_AMOUNT.doubleValue()))
            .andExpect(jsonPath("$.discount").value(DEFAULT_DISCOUNT.doubleValue()))
            .andExpect(jsonPath("$.discountName").value(DEFAULT_DISCOUNT_NAME.toString()))
            .andExpect(jsonPath("$.productUnits").value(DEFAULT_PRODUCT_UNITS))
            .andExpect(jsonPath("$.taxName").value(DEFAULT_TAX_NAME.toString()))
            .andExpect(jsonPath("$.taxRate").value(DEFAULT_TAX_RATE.doubleValue()));
    }

    @Test
    @Transactional
    public void getNonExistingOrder() throws Exception {
        // Get the order
        restOrderMockMvc.perform(get("/api/orders/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

		int databaseSizeBeforeUpdate = orderRepository.findAll().size();

        // Update the order
        order.setCreated(UPDATED_CREATED);
        order.setTotalAmount(UPDATED_TOTAL_AMOUNT);
        order.setDiscount(UPDATED_DISCOUNT);
        order.setDiscountName(UPDATED_DISCOUNT_NAME);
        order.setProductUnits(UPDATED_PRODUCT_UNITS);
        order.setTaxName(UPDATED_TAX_NAME);
        order.setTaxRate(UPDATED_TAX_RATE);
        

        restOrderMockMvc.perform(put("/api/orders")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(order)))
                .andExpect(status().isOk());

        // Validate the Order in the database
        List<Order> orders = orderRepository.findAll();
        assertThat(orders).hasSize(databaseSizeBeforeUpdate);
        Order testOrder = orders.get(orders.size() - 1);
        assertThat(testOrder.getCreated().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_CREATED);
        assertThat(testOrder.getTotalAmount()).isEqualTo(UPDATED_TOTAL_AMOUNT);
        assertThat(testOrder.getDiscount()).isEqualTo(UPDATED_DISCOUNT);
        assertThat(testOrder.getDiscountName()).isEqualTo(UPDATED_DISCOUNT_NAME);
        assertThat(testOrder.getProductUnits()).isEqualTo(UPDATED_PRODUCT_UNITS);
        assertThat(testOrder.getTaxName()).isEqualTo(UPDATED_TAX_NAME);
        assertThat(testOrder.getTaxRate()).isEqualTo(UPDATED_TAX_RATE);
    }

    @Test
    @Transactional
    public void deleteOrder() throws Exception {
        // Initialize the database
        orderRepository.saveAndFlush(order);

		int databaseSizeBeforeDelete = orderRepository.findAll().size();

        // Get the order
        restOrderMockMvc.perform(delete("/api/orders/{id}", order.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Order> orders = orderRepository.findAll();
        assertThat(orders).hasSize(databaseSizeBeforeDelete - 1);
    }
}
