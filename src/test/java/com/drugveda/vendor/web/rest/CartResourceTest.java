package com.drugveda.vendor.web.rest;

import com.drugveda.vendor.Application;
import com.drugveda.vendor.domain.Cart;
import com.drugveda.vendor.repository.CartRepository;
import com.drugveda.vendor.repository.search.CartSearchRepository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.hamcrest.Matchers.hasItem;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Test class for the CartResource REST controller.
 *
 * @see CartResource
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@IntegrationTest
public class CartResourceTest {

    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");


    private static final Integer DEFAULT_QUANTITY = 1;
    private static final Integer UPDATED_QUANTITY = 2;

    private static final Long DEFAULT_PRODUCT_CATALOUGE_ID = 1L;
    private static final Long UPDATED_PRODUCT_CATALOUGE_ID = 2L;

    private static final Long DEFAULT_USER_ID = 1L;
    private static final Long UPDATED_USER_ID = 2L;

    private static final DateTime DEFAULT_ADDED_TIME = new DateTime(0L, DateTimeZone.UTC);
    private static final DateTime UPDATED_ADDED_TIME = new DateTime(DateTimeZone.UTC).withMillisOfSecond(0);
    private static final String DEFAULT_ADDED_TIME_STR = dateTimeFormatter.print(DEFAULT_ADDED_TIME);
    private static final String DEFAULT_TOKEN = "SAMPLE_TEXT";
    private static final String UPDATED_TOKEN = "UPDATED_TEXT";

    @Inject
    private CartRepository cartRepository;

    @Inject
    private CartSearchRepository cartSearchRepository;

    @Inject
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    private MockMvc restCartMockMvc;

    private Cart cart;

    @PostConstruct
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CartResource cartResource = new CartResource();
        ReflectionTestUtils.setField(cartResource, "cartRepository", cartRepository);
        ReflectionTestUtils.setField(cartResource, "cartSearchRepository", cartSearchRepository);
        this.restCartMockMvc = MockMvcBuilders.standaloneSetup(cartResource).setMessageConverters(jacksonMessageConverter).build();
    }

    @Before
    public void initTest() {
        cart = new Cart();
        cart.setQuantity(DEFAULT_QUANTITY);
        cart.setProductCatalougeId(DEFAULT_PRODUCT_CATALOUGE_ID);
        cart.setUserId(DEFAULT_USER_ID);
        cart.setAddedTime(DEFAULT_ADDED_TIME);
        cart.setToken(DEFAULT_TOKEN);
    }

    @Test
    @Transactional
    public void createCart() throws Exception {
        int databaseSizeBeforeCreate = cartRepository.findAll().size();

        // Create the Cart

        restCartMockMvc.perform(post("/api/carts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cart)))
                .andExpect(status().isCreated());

        // Validate the Cart in the database
        List<Cart> carts = cartRepository.findAll();
        assertThat(carts).hasSize(databaseSizeBeforeCreate + 1);
        Cart testCart = carts.get(carts.size() - 1);
        assertThat(testCart.getQuantity()).isEqualTo(DEFAULT_QUANTITY);
        assertThat(testCart.getProductCatalougeId()).isEqualTo(DEFAULT_PRODUCT_CATALOUGE_ID);
        assertThat(testCart.getUserId()).isEqualTo(DEFAULT_USER_ID);
        assertThat(testCart.getAddedTime().toDateTime(DateTimeZone.UTC)).isEqualTo(DEFAULT_ADDED_TIME);
        assertThat(testCart.getToken()).isEqualTo(DEFAULT_TOKEN);
    }

    @Test
    @Transactional
    public void getAllCarts() throws Exception {
        // Initialize the database
        cartRepository.saveAndFlush(cart);

        // Get all the carts
        restCartMockMvc.perform(get("/api/carts"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[*].id").value(hasItem(cart.getId().intValue())))
                .andExpect(jsonPath("$.[*].quantity").value(hasItem(DEFAULT_QUANTITY)))
                .andExpect(jsonPath("$.[*].productCatalougeId").value(hasItem(DEFAULT_PRODUCT_CATALOUGE_ID)))
                .andExpect(jsonPath("$.[*].userId").value(hasItem(DEFAULT_USER_ID)))
                .andExpect(jsonPath("$.[*].addedTime").value(hasItem(DEFAULT_ADDED_TIME_STR)))
                .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN.toString())));
    }

    @Test
    @Transactional
    public void getCart() throws Exception {
        // Initialize the database
        cartRepository.saveAndFlush(cart);

        // Get the cart
        restCartMockMvc.perform(get("/api/carts/{id}", cart.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            .andExpect(jsonPath("$.id").value(cart.getId().intValue()))
            .andExpect(jsonPath("$.quantity").value(DEFAULT_QUANTITY))
            .andExpect(jsonPath("$.productCatalougeId").value(DEFAULT_PRODUCT_CATALOUGE_ID))
            .andExpect(jsonPath("$.userId").value(DEFAULT_USER_ID))
            .andExpect(jsonPath("$.addedTime").value(DEFAULT_ADDED_TIME_STR))
            .andExpect(jsonPath("$.token").value(DEFAULT_TOKEN.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCart() throws Exception {
        // Get the cart
        restCartMockMvc.perform(get("/api/carts/{id}", Long.MAX_VALUE))
                .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCart() throws Exception {
        // Initialize the database
        cartRepository.saveAndFlush(cart);

		int databaseSizeBeforeUpdate = cartRepository.findAll().size();

        // Update the cart
        cart.setQuantity(UPDATED_QUANTITY);
        cart.setProductCatalougeId(UPDATED_PRODUCT_CATALOUGE_ID);
        cart.setUserId(UPDATED_USER_ID);
        cart.setAddedTime(UPDATED_ADDED_TIME);
        cart.setToken(UPDATED_TOKEN);
        

        restCartMockMvc.perform(put("/api/carts")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(cart)))
                .andExpect(status().isOk());

        // Validate the Cart in the database
        List<Cart> carts = cartRepository.findAll();
        assertThat(carts).hasSize(databaseSizeBeforeUpdate);
        Cart testCart = carts.get(carts.size() - 1);
        assertThat(testCart.getQuantity()).isEqualTo(UPDATED_QUANTITY);
        assertThat(testCart.getProductCatalougeId()).isEqualTo(UPDATED_PRODUCT_CATALOUGE_ID);
        assertThat(testCart.getUserId()).isEqualTo(UPDATED_USER_ID);
        assertThat(testCart.getAddedTime().toDateTime(DateTimeZone.UTC)).isEqualTo(UPDATED_ADDED_TIME);
        assertThat(testCart.getToken()).isEqualTo(UPDATED_TOKEN);
    }

    @Test
    @Transactional
    public void deleteCart() throws Exception {
        // Initialize the database
        cartRepository.saveAndFlush(cart);

		int databaseSizeBeforeDelete = cartRepository.findAll().size();

        // Get the cart
        restCartMockMvc.perform(delete("/api/carts/{id}", cart.getId())
                .accept(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());

        // Validate the database is empty
        List<Cart> carts = cartRepository.findAll();
        assertThat(carts).hasSize(databaseSizeBeforeDelete - 1);
    }
}
