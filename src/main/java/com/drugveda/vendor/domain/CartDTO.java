package com.drugveda.vendor.domain;

/**
 * This entity will hold the information about the cart.
 * 
 * @author pradeep.kaushal
 *
 */
public class CartDTO {

	private Long id;

	private Long productCatalougeId;

	private Integer quantity;

	private ProductCatalogue productCatalogue;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public ProductCatalogue getProductCatalogue() {
		return productCatalogue;
	}

	public void setProductCatalogue(ProductCatalogue productCatalogue) {
		this.productCatalogue = productCatalogue;
	}

	public Long getProductCatalougeId() {
		return productCatalougeId;
	}

	public void setProductCatalougeId(Long productCatalougeId) {
		this.productCatalougeId = productCatalougeId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

}
