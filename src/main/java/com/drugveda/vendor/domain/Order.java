package com.drugveda.vendor.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.drugveda.vendor.domain.util.CustomDateTimeDeserializer;
import com.drugveda.vendor.domain.util.CustomDateTimeSerializer;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A Order.
 */
@Entity
@Table(name = "ORDER_TBL")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="order")
public class Order implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "created")
    private DateTime created;


    
    @Column(name = "total_amount")
    private Double totalAmount;


    
    @Column(name = "discount")
    private Double discount;


    
    @Column(name = "discount_name")
    private String discountName;


    
    @Column(name = "product_units")
    private Integer productUnits;


    
    @Column(name = "tax_name")
    private String taxName;


    
    @Column(name = "tax_rate")
    private Double taxRate;

    @ManyToOne
    private Vendor vendor;

    @OneToOne
    private Transaction transaction;

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderDetails> orderdetailss = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderStatus> orderstatuss = new HashSet<>();

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<OrderStatusDetails> orderstatusdetailss = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DateTime getCreated() {
        return created;
    }

    public void setCreated(DateTime created) {
        this.created = created;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public String getDiscountName() {
        return discountName;
    }

    public void setDiscountName(String discountName) {
        this.discountName = discountName;
    }

    public Integer getProductUnits() {
        return productUnits;
    }

    public void setProductUnits(Integer productUnits) {
        this.productUnits = productUnits;
    }

    public String getTaxName() {
        return taxName;
    }

    public void setTaxName(String taxName) {
        this.taxName = taxName;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public Set<OrderDetails> getOrderdetailss() {
        return orderdetailss;
    }

    public void setOrderdetailss(Set<OrderDetails> orderDetailss) {
        this.orderdetailss = orderDetailss;
    }

    public Set<OrderStatus> getOrderstatuss() {
        return orderstatuss;
    }

    public void setOrderstatuss(Set<OrderStatus> orderStatuss) {
        this.orderstatuss = orderStatuss;
    }

    public Set<OrderStatusDetails> getOrderstatusdetailss() {
        return orderstatusdetailss;
    }

    public void setOrderstatusdetailss(Set<OrderStatusDetails> orderStatusDetailss) {
        this.orderstatusdetailss = orderStatusDetailss;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Order order = (Order) o;

        if ( ! Objects.equals(id, order.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", created='" + created + "'" +
                ", totalAmount='" + totalAmount + "'" +
                ", discount='" + discount + "'" +
                ", discountName='" + discountName + "'" +
                ", productUnits='" + productUnits + "'" +
                ", taxName='" + taxName + "'" +
                ", taxRate='" + taxRate + "'" +
                '}';
    }
}
