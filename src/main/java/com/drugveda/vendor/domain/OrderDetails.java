package com.drugveda.vendor.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;


/**
 * A OrderDetails.
 */
@Entity
@Table(name = "ORDERDETAILS")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="orderdetails")
public class OrderDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Column(name = "product_id")
    private Integer productId;


    
    @Column(name = "product_name")
    private String productName;


    
    @Column(name = "units")
    private Integer units;


    
    @Column(name = "retail_price")
    private Double retailPrice;


    
    @Column(name = "whole_sale_price")
    private Double wholeSalePrice;


    
    @Column(name = "purchased_price")
    private Double purchasedPrice;

    @ManyToOne
    private Order order;

    @OneToOne(mappedBy = "orderDetails")
    @JsonIgnore
    private OrderStatusDetails orderstatusdetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getUnits() {
        return units;
    }

    public void setUnits(Integer units) {
        this.units = units;
    }

    public Double getRetailPrice() {
        return retailPrice;
    }

    public void setRetailPrice(Double retailPrice) {
        this.retailPrice = retailPrice;
    }

    public Double getWholeSalePrice() {
        return wholeSalePrice;
    }

    public void setWholeSalePrice(Double wholeSalePrice) {
        this.wholeSalePrice = wholeSalePrice;
    }

    public Double getPurchasedPrice() {
        return purchasedPrice;
    }

    public void setPurchasedPrice(Double purchasedPrice) {
        this.purchasedPrice = purchasedPrice;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public OrderStatusDetails getOrderstatusdetails() {
        return orderstatusdetails;
    }

    public void setOrderstatusdetails(OrderStatusDetails orderStatusDetails) {
        this.orderstatusdetails = orderStatusDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OrderDetails orderDetails = (OrderDetails) o;

        if ( ! Objects.equals(id, orderDetails.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "OrderDetails{" +
                "id=" + id +
                ", productId='" + productId + "'" +
                ", productName='" + productName + "'" +
                ", units='" + units + "'" +
                ", retailPrice='" + retailPrice + "'" +
                ", wholeSalePrice='" + wholeSalePrice + "'" +
                ", purchasedPrice='" + purchasedPrice + "'" +
                '}';
    }
}
