package com.drugveda.vendor.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;


/**
 * A ProductCatalogue.
 */
@Entity
@Table(name = "PRODUCTCATALOGUE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="productcatalogue")
public class ProductCatalogue implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5831650358821833361L;



	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Column(name = "min_order")
    private Integer minOrder;


    
    @Column(name = "apply_tax")
    private Boolean applyTax;


    
    @Column(name = "active")
    private Boolean active;

    @ManyToOne
    private Tax tax;

    @ManyToOne
    private ProductVariation productVariation;

    @ManyToOne
    private Vendor vendor;

    @ManyToOne
    private Product product;

    @OneToOne
    private Price price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getMinOrder() {
        return minOrder;
    }

    public void setMinOrder(Integer minOrder) {
        this.minOrder = minOrder;
    }

    public Boolean getApplyTax() {
        return applyTax;
    }

    public void setApplyTax(Boolean applyTax) {
        this.applyTax = applyTax;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Tax getTax() {
        return tax;
    }

    public void setTax(Tax tax) {
        this.tax = tax;
    }

    public ProductVariation getProductVariation() {
        return productVariation;
    }

    public void setProductVariation(ProductVariation productVariation) {
        this.productVariation = productVariation;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductCatalogue productCatalogue = (ProductCatalogue) o;

        if ( ! Objects.equals(id, productCatalogue.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProductCatalogue{" +
                "id=" + id +
                ", minOrder='" + minOrder + "'" +
                ", applyTax='" + applyTax + "'" +
                ", active='" + active + "'" +
                '}';
    }
}
