package com.drugveda.vendor.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.data.elasticsearch.annotations.Document;

import com.drugveda.vendor.domain.enumeration.MeasureType;
import com.drugveda.vendor.domain.util.CustomDateTimeDeserializer;
import com.drugveda.vendor.domain.util.CustomDateTimeSerializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * A ProductOption.
 */
@Entity
@Table(name = "PRODUCTOPTION")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="productoption")
public class ProductOption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7255291232575743012L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Enumerated(EnumType.STRING)
    @Column(name = "measure_type")
    private MeasureType measureType;


    
    @Column(name = "quantity")
    private Integer quantity;


    
    @Column(name = "active")
    private Boolean active;


    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "crated_date")
    private DateTime cratedDate;


    
    @Type(type = "org.jadira.usertype.dateandtime.joda.PersistentDateTime")
    @JsonSerialize(using = CustomDateTimeSerializer.class)
    @JsonDeserialize(using = CustomDateTimeDeserializer.class)
    @Column(name = "updated_date")
    private DateTime updatedDate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MeasureType getMeasureType() {
        return measureType;
    }

    public void setMeasureType(MeasureType measureType) {
        this.measureType = measureType;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public DateTime getCratedDate() {
        return cratedDate;
    }

    public void setCratedDate(DateTime cratedDate) {
        this.cratedDate = cratedDate;
    }

    public DateTime getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(DateTime updatedDate) {
        this.updatedDate = updatedDate;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductOption productOption = (ProductOption) o;

        if ( ! Objects.equals(id, productOption.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProductOption{" +
                "id=" + id +
                ", measureType='" + measureType + "'" +
                ", quantity='" + quantity + "'" +
                ", active='" + active + "'" +
                ", cratedDate='" + cratedDate + "'" +
                ", updatedDate='" + updatedDate + "'" +
                '}';
    }
}
