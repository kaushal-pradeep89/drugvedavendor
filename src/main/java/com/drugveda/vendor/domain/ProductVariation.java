package com.drugveda.vendor.domain;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import com.drugveda.vendor.domain.enumeration.MeasureType;

/**
 * A ProductVariation.
 */
@Entity
@Table(name = "PRODUCTVARIATION")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="productvariation")
public class ProductVariation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Enumerated(EnumType.STRING)
    @Column(name = "measure_type")
    private MeasureType measureType;

    @OneToMany(mappedBy = "productVariation",fetch=FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<VariationOption> variationoptions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MeasureType getMeasureType() {
        return measureType;
    }

    public void setMeasureType(MeasureType measureType) {
        this.measureType = measureType;
    }

    public Set<VariationOption> getVariationoptions() {
        return variationoptions;
    }

    public void setVariationoptions(Set<VariationOption> variationOptions) {
        this.variationoptions = variationOptions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProductVariation productVariation = (ProductVariation) o;

        if ( ! Objects.equals(id, productVariation.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public String toString() {
		return "ProductVariation [id=" + id + ", measureType=" + measureType + ", variationoptions=" + variationoptions
				+ "]";
	}

   
}
