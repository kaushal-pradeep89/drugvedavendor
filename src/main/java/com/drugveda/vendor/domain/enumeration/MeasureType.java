package com.drugveda.vendor.domain.enumeration;

/**
 * The MeasureType enumeration.
 */
public enum MeasureType {
    MG,ML,GM,KG,LTR,NO
}
