package com.drugveda.vendor.domain.enumeration;

/**
 * The STATUS enumeration.
 */
public enum STATUS {
    AWAITING_ACCEPTANCE,ACCEPTED,PROCESSING,PARTIALLY_PROCESSED,PACKED,DISPATCHED,CANCELED
}
