package com.drugveda.vendor.domain;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;


/**
 * A Tax.
 */
@Entity
@Table(name = "TAX")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName="tax")
public class Tax implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;


    
    @Column(name = "name")
    private String name;


    
    @Column(name = "tax")
    private Double tax;


    
    @Column(name = "col_ext1")
    private Double colExt1;


    
    @Column(name = "col_ext2")
    private Double colExt2;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getColExt1() {
        return colExt1;
    }

    public void setColExt1(Double colExt1) {
        this.colExt1 = colExt1;
    }

    public Double getColExt2() {
        return colExt2;
    }

    public void setColExt2(Double colExt2) {
        this.colExt2 = colExt2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Tax tax = (Tax) o;

        if ( ! Objects.equals(id, tax.id)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Tax{" +
                "id=" + id +
                ", name='" + name + "'" +
                ", tax='" + tax + "'" +
                ", colExt1='" + colExt1 + "'" +
                ", colExt2='" + colExt2 + "'" +
                '}';
    }
}
