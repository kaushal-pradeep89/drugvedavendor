package com.drugveda.vendor.domain;

/**
 * This is the dto for order status details. It will be used to carry some
 * comment information from order status.
 * 
 * @author pradeep.kaushal
 *
 */
public class StatusDetails {
	private String comment;

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

}
