package com.drugveda.vendor.service;

import java.util.List;

import com.drugveda.vendor.domain.Product;

/**
 * This interface will have the functionality for search of product, brand and
 * composition.
 * 
 * @author pradeep.kaushal
 *
 */
public interface ProductSearchService {
	/**
	 * This method will search the product on the basis of provided query.
	 * 
	 * @param query
	 * @return the
	 */
	List<Product> findProduct(String query);
}
