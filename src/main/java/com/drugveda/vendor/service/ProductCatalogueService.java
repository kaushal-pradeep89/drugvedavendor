package com.drugveda.vendor.service;

import com.drugveda.vendor.domain.ProductVariation;
/**
 * This service will deal with product catalogue service. 
 * @author pradeep.kaushal
 *
 */
public interface ProductCatalogueService {
	
	ProductVariation createProductVariation(Long id, ProductVariation productVariation);

}
