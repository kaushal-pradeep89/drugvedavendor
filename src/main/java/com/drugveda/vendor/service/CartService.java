package com.drugveda.vendor.service;

import java.util.List;

import com.drugveda.vendor.domain.CartDTO;

/**
 * This interface will handle the cart module.
 * 
 * @author pradeep.kaushal
 *
 */
public interface CartService {

	/**
	 * This method will save or update the cart. And will return the cart dto
	 * with the id.
	 * 
	 * @param cartDto
	 * @return the cart dto with the complete information.
	 */
	CartDTO save(CartDTO cartDto);

	/**
	 * This will delete the item in the cart.
	 * 
	 * @param cartDto
	 */
	void delete(CartDTO cartDto);

	/**
	 * This will delete the item in the cart.
	 * 
	 * @param cartDto
	 */
	void delete(Long id);
	/**
	 * This method will retrurn the cart items for the particular users.
	 */

	List<CartDTO> findCartItems(Long userId, String tokenId);
}
