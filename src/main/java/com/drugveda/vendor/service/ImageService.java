package com.drugveda.vendor.service;

import java.util.List;

import com.drugveda.vendor.domain.ProductImage;

public interface ImageService {
	List<ProductImage> getProductImageByProductId(Long id);
}
