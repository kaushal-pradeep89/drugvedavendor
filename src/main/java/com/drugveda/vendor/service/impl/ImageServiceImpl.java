package com.drugveda.vendor.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.domain.ProductImage;
import com.drugveda.vendor.repository.ImageRepository;
import com.drugveda.vendor.service.ImageService;

@Service
@Transactional
public class ImageServiceImpl implements ImageService {

	private final Logger log = LoggerFactory.getLogger(ImageServiceImpl.class);

	@Inject
	private ImageRepository imageRepository;

	@Override
	public List<ProductImage> getProductImageByProductId(Long id) {
		log.debug("Getting the product images by product id");
		return imageRepository.findImageByProductId(id);
	}

}
