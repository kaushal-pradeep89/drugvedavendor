package com.drugveda.vendor.service.impl;

import java.util.Set;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.domain.ProductCatalogue;
import com.drugveda.vendor.domain.ProductVariation;
import com.drugveda.vendor.domain.VariationOption;
import com.drugveda.vendor.repository.ProductCatalogueRepository;
import com.drugveda.vendor.repository.ProductVariationRepository;
import com.drugveda.vendor.repository.VariationOptionRepository;
import com.drugveda.vendor.service.ProductCatalogueService;

@Service
@Transactional
public class ProductCatalogueServiceImpl implements ProductCatalogueService {

	private final Logger log = LoggerFactory.getLogger(ProductCatalogueServiceImpl.class);

	@Inject
	private ProductVariationRepository productVariationRepository;

	@Inject
	private VariationOptionRepository variationOptionRepository;

	@Inject
	private ProductCatalogueRepository productCatalogueRepository;

	@Override
	public ProductVariation createProductVariation(Long id, ProductVariation productVariation) {
		log.debug("Service to save ProductVariation : {}", productVariation);

		ProductVariation temp = productVariationRepository.save(productVariation);

		Set<VariationOption> variationoptions = productVariation.getVariationoptions();
		if (variationoptions != null && !variationoptions.isEmpty()) {
			for (VariationOption variationOption : variationoptions) {
				variationOption.setProductVariation(temp);
				variationOptionRepository.save(variationOption);
			}
		}
		ProductCatalogue productCatalogue = productCatalogueRepository.findOne(id);
		productCatalogue.setProductVariation(temp);
		productCatalogueRepository.save(productCatalogue);
		return temp;
	}

}
