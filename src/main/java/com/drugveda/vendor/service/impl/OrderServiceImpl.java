package com.drugveda.vendor.service.impl;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.drugveda.vendor.domain.Order;
import com.drugveda.vendor.domain.OrderDetails;
import com.drugveda.vendor.domain.OrderStatus;
import com.drugveda.vendor.domain.OrderStatusDetails;
import com.drugveda.vendor.domain.User;
import com.drugveda.vendor.repository.OrderDAO;
import com.drugveda.vendor.repository.OrderDetailsDAO;
import com.drugveda.vendor.repository.OrderStatusDAO;
import com.drugveda.vendor.repository.OrderStatusDetailsDAO;
import com.drugveda.vendor.service.OrderService;
import com.drugveda.vendor.service.UserService;

@Service
@Transactional
public class OrderServiceImpl implements OrderService {

	private final Logger log = LoggerFactory.getLogger(OrderServiceImpl.class);

	@Inject
	private OrderDetailsDAO orderDetailsDAO;

	@Inject
	private UserService userService;

	@Inject
	private OrderDAO orderDAO;

	@Inject
	private OrderStatusDAO orderStatusDAO;

	@Inject
	private OrderStatusDetailsDAO orderStatusDetailsDAO;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.drugveda.vendor.service.OrderService#findOrderDetailsByOrderId(java.
	 * lang.Long)
	 */
	@Override
	public List<OrderDetails> findOrderDetailsByOrderId(Long orderId) {
		log.debug("Entering into to findOrderDetailsByOrderId" + orderId);

		List<OrderDetails> orderDetails = orderDetailsDAO.findOrderDetailsByOrderId(orderId);

		return orderDetails;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.drugveda.vendor.service.OrderService#findOrderForCurrentUser()
	 */
	@Override
	public List<Order> findOrderForCurrentUser() {

		Long userId = getUserId();

		List<Order> orders = orderDAO.getOrders(userId);

		return orders;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.drugveda.vendor.service.OrderService#saveOrder(com.drugveda.vendor.
	 * domain.Order)
	 */
	@Override
	public List<Order> saveOrder(Order order) {
		// TODO Auto-generated method stub
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.drugveda.vendor.service.OrderService#changeOrderStatus(com.drugveda.
	 * vendor.domain.OrderStatus)
	 */
	public OrderStatus changeOrderStatus(OrderStatus orderStatus) {
		OrderStatus status = orderStatusDAO.updateOrderStatus(orderStatus);
		return status;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.drugveda.vendor.service.OrderService#getOrderStatus(java.lang.Long)
	 */
	@Override
	public OrderStatus getOrderStatus(Long orderId) {
		Long userId = getUserId();
		OrderStatus orderStatus = orderStatusDAO.getOrderStatus(orderId, userId);
		return orderStatus;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.drugveda.vendor.service.OrderService#getOrderStatus(java.lang.Long,
	 * java.lang.Long)
	 */
	@Override
	public OrderStatus getOrderStatus(Long orderId, Long vendorId) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Get the user id for the logged in user.
	 * 
	 * @return
	 */
	private Long getUserId() {
		User user = userService.getUserWithAuthorities();

		Long userId = user.getId();
		return userId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.drugveda.vendor.service.OrderService#getOrderStatusDetails(java.lang.
	 * Long, java.lang.Long)
	 */
	@Override
	public List<OrderStatusDetails> getOrderStatusDetails(Long orderId, Long statusId) {
		List<OrderStatusDetails> orderStatusDetails = orderStatusDetailsDAO.getOrderStatusDetails(orderId, statusId);
		return orderStatusDetails;
	}

}
