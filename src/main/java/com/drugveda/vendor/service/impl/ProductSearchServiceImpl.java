package com.drugveda.vendor.service.impl;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.drugveda.vendor.domain.Product;
import com.drugveda.vendor.repository.ProductDAO;
import com.drugveda.vendor.service.ProductSearchService;

/**
 * This class is the implementation of
 * 
 * @author pradeep.kaushal
 *
 */
@Transactional
@Service
public class ProductSearchServiceImpl implements ProductSearchService {

	@Inject
	private ProductDAO productDAO;

	@Override
	public List<Product> findProduct(String query) {
		if (query == null || query.isEmpty()) {
			throw new IllegalArgumentException("The query is null or empty, query:" + query);
		}
		query = query.toUpperCase();
		Collection<Product> products = productDAO.findProduct(query);
		List<Product> prods = (List<Product>) products;
		return prods;
	}

}
