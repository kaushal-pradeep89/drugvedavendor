package com.drugveda.vendor.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import com.drugveda.vendor.domain.Cart;
import com.drugveda.vendor.domain.CartDTO;
import com.drugveda.vendor.domain.ProductCatalogue;
import com.drugveda.vendor.domain.User;
import com.drugveda.vendor.domain.Vendor;
import com.drugveda.vendor.repository.CartDAO;
import com.drugveda.vendor.repository.ProductCatalogueDAO;
import com.drugveda.vendor.service.CartService;
import com.drugveda.vendor.service.UserService;

/**
 * This is the implementation of cart service.
 * 
 * @author pradeep.kaushal
 *
 */
@Service
@Transactional
public class CartServiceImpl implements CartService {

	@Inject
	private CartDAO cartDAO;

	@Inject
	private ProductCatalogueDAO productCatalogueDAO;

	@Inject
	private UserService userService;

	@Override
	public CartDTO save(CartDTO cartDto) {
		Long userId = getUserId();

		Cart cart = prepareCartEntity(cartDto, userId);
		ProductCatalogue catalogue = null;
		if (cartDto.getProductCatalougeId() != null) {
			catalogue = productCatalogueDAO.getProductCatalougeById(cartDto.getProductCatalougeId());
		} else if (cartDto.getProductCatalogue() != null && cartDto.getProductCatalogue().getId() != null) {
			catalogue = productCatalogueDAO.getProductCatalougeById(cartDto.getProductCatalogue().getId());
		}
		// If the min order is less than catalouge min order
		if (catalogue.getMinOrder() < cartDto.getQuantity())
			return null;

		cart = cartDAO.save(cart);
		// Set the cart id.
		cartDto.setId(cart.getId());

		return cartDto;
	}

	@Override
	public void delete(CartDTO cartDto) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<CartDTO> findCartItems(Long userId, String tokenId) {
		List<Cart> items = cartDAO.findCartItem(userId);
		List<CartDTO> cartItems = new ArrayList<CartDTO>();

		if (items == null || items.isEmpty()) {
			return cartItems;
		}

		for (Cart cart : items) {
			ProductCatalogue productCatalogue = productCatalogueDAO
					.getProductCatalougeById(cart.getProductCatalougeId());

			if (productCatalogue == null)
				continue;

			CartDTO dto = prepareCartDTO(productCatalogue, cart);
			cartItems.add(dto);
		}
		return cartItems;
	}

	/**
	 * Remove some of the critical information fields from
	 * {@link ProductCatalogue} entity and translate them into cart dto to
	 * display on the page.
	 * 
	 * @param productCatalogue
	 *            the product catalogue entity.
	 * @param cart
	 *            the cart entity.
	 * @return the cartDto.
	 */
	private CartDTO prepareCartDTO(ProductCatalogue productCatalogue, Cart cart) {
		CartDTO cartDTO = new CartDTO();
		cartDTO.setId(cart.getId());
		cartDTO.setProductCatalougeId(productCatalogue.getId());
		Vendor vendor = getVendorForConsumerPage(productCatalogue.getVendor());
		productCatalogue.setVendor(vendor);
		cartDTO.setProductCatalogue(productCatalogue);
		return cartDTO;
	}

	private Vendor getVendorForConsumerPage(Vendor vendor) {
		vendor.setAddress(null);
		vendor.setCratedDate(null);
		vendor.setMobibleNo(null);
		vendor.setPhoneNo(null);
		vendor.setUpdatedDate(null);
		vendor.setUser(null);
		return vendor;
	}

	@Override
	public void delete(Long id) {
		Long userId = getUserId();
		cartDAO.delete(id, userId);
	}

	private Cart prepareCartEntity(CartDTO cartDto, Long userId) {
		Cart cart = new Cart();
		cart.setId(cartDto.getId());
		cart.setProductCatalougeId(cartDto.getProductCatalogue().getId());
		cart.setUserId(userId);
		cart.setQuantity(cartDto.getQuantity());
		cart.setAddedTime(new DateTime());
		return cart;
	}

	/**
	 * Get the user id for the logged in user.
	 * 
	 * @return
	 */
	private Long getUserId() {
		User user = userService.getUserWithAuthorities();

		Long userId = user.getId();
		return userId;
	}

}
