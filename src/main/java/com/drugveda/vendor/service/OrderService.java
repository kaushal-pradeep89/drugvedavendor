package com.drugveda.vendor.service;

import java.util.List;

import com.drugveda.vendor.domain.Order;
import com.drugveda.vendor.domain.OrderDetails;
import com.drugveda.vendor.domain.OrderStatus;
import com.drugveda.vendor.domain.OrderStatusDetails;

/**
 * This interface will deal with the order module
 * 
 * @author pradeep.kaushal
 *
 */
public interface OrderService {
	/**
	 * Get the order by order id.
	 * 
	 * @param orderId
	 * @return the order details.
	 */
	List<OrderDetails> findOrderDetailsByOrderId(Long orderId);

	/**
	 * Get the order for current user.
	 * 
	 * @return the orders.
	 */
	List<Order> findOrderForCurrentUser();

	/**
	 * Save the order. Order will be saved along with this order status. It will
	 * be cascading saved.
	 * 
	 * @param order
	 *            the order
	 * @return the saved order
	 */
	List<Order> saveOrder(Order order);

	/**
	 * Change the order status.
	 * 
	 * @param orderStatus
	 *            the order status
	 * @return the updated order status.
	 */
	OrderStatus changeOrderStatus(OrderStatus orderStatus);

	/**
	 * Get the order status.
	 * 
	 * @param orderStatus
	 *            the order status
	 * @return the updated order status.
	 */
	OrderStatus getOrderStatus(Long orderId);

	/**
	 * Get the order status.
	 * 
	 * @param orderStatus
	 *            the order status
	 * @param vendorId
	 *            the vendorId
	 * @return the updated order status.
	 */
	OrderStatus getOrderStatus(Long orderId, Long vendorId);

	/**
	 * Get the order details on by order id and status id
	 * 
	 * @param orderId
	 *            the order id
	 * @param statusId
	 *            the order status id
	 * @return the order status details.
	 */
	List<OrderStatusDetails> getOrderStatusDetails(Long orderId, Long statusId);
}
