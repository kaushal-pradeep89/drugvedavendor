package com.drugveda.vendor.web.rest;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.Product;
import com.drugveda.vendor.domain.ProductImage;
import com.drugveda.vendor.repository.ProductImageRepository;
import com.drugveda.vendor.repository.ProductRepository;
import com.drugveda.vendor.repository.search.ProductImageSearchRepository;
import com.drugveda.vendor.security.SecurityUtils;
import com.drugveda.vendor.service.ImageService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ProductImage.
 */
@RestController
@RequestMapping("/api")
public class ImageResource {

	private final Logger log = LoggerFactory.getLogger(ImageResource.class);

	@Inject
	private ProductImageRepository productImageRepository;

	@Inject
	private ProductRepository productRepository;

	@Inject
	private ProductImageSearchRepository productImageSearchRepository;

	@Inject
	private ImageService imageService;

	@Inject
	private Environment env;

	/**
	 * GET /images -> get all the productImages.
	 */
	@RequestMapping(value = "/images/{productId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<ProductImage> getAll(@PathVariable Long productId) {
		log.debug("REST request to get all ProductImages");
		return imageService.getProductImageByProductId(productId);
	}

	
	/**
	 * DELETE /productImages/:id -> delete the "id" productImage.
	 */
	@RequestMapping(value = "/images/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete ProductImage : {}", id);
		productImageRepository.delete(id);
		productImageSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productImage", id.toString())).build();
	}

	/**
	 * This resource is responsible for uploading the image.
	 *
	 *
	 * POST /uploadImage -> Create a new businessImage.
	 */
	@RequestMapping(value = "/uploadImage/{productId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductImage> uploadImage(@RequestParam("file") MultipartFile file,
			@PathVariable("productId") String productId) throws URISyntaxException {
		String fileName = SecurityUtils.getCurrentLogin() + "_" + System.currentTimeMillis();
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				Random rand = new Random();
				// nextInt is normally exclusive of the top value,
				// so add 1 to make it inclusive
				int folderNumber = rand.nextInt((999 - 100) + 1) + 100;
				File dir = new File(env.getProperty("businessImages.path") + folderNumber);
				if (!dir.exists()) {
					dir.mkdirs();
				}
				String originalFileName = file.getOriginalFilename();
				String imagePath = folderNumber + "/" + fileName + "."
						+ originalFileName.substring(originalFileName.lastIndexOf('.') + 1);
				;
				String folderPath = env.getProperty("businessImages.path");
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(new File(folderPath + imagePath)));
				stream.write(bytes);
				stream.close();
				ProductImage productImage = new ProductImage();
				productImage.setImageName(folderPath);
				productImage.setImageUrl(imagePath);

				Long prodId = Long.parseLong(productId);
				Product product = productRepository.findOne(prodId);

				productImage.setProduct(product);

				ProductImage save = productImageRepository.save(productImage);

				return ResponseEntity.ok().body(save);
			} catch (Exception e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
			}
		} else {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
		}
	}
}
