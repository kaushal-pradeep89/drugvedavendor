package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.websocket.server.PathParam;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.OrderDetails;
import com.drugveda.vendor.repository.OrderDetailsRepository;
import com.drugveda.vendor.repository.search.OrderDetailsSearchRepository;
import com.drugveda.vendor.service.OrderService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing OrderDetails.
 */
@RestController
@RequestMapping("/api")
public class OrderDetailsResource {

	private final Logger log = LoggerFactory.getLogger(OrderDetailsResource.class);

	@Inject
	private OrderDetailsRepository orderDetailsRepository;

	@Inject
	private OrderDetailsSearchRepository orderDetailsSearchRepository;

	@Inject
	private OrderService orderService;

	/**
	 * POST /orderDetailss -> Create a new orderDetails.
	 */
	@RequestMapping(value = "/orderDetailss", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderDetails> create(@RequestBody OrderDetails orderDetails) throws URISyntaxException {
		log.debug("REST request to save OrderDetails : {}", orderDetails);
		if (orderDetails.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new orderDetails cannot already have an ID")
					.body(null);
		}
		OrderDetails result = orderDetailsRepository.save(orderDetails);
		orderDetailsSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/orderDetailss/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("orderDetails", result.getId().toString())).body(result);
	}

	/**
	 * PUT /orderDetailss -> Updates an existing orderDetails.
	 */
	@RequestMapping(value = "/orderDetailss", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderDetails> update(@RequestBody OrderDetails orderDetails) throws URISyntaxException {
		log.debug("REST request to update OrderDetails : {}", orderDetails);
		if (orderDetails.getId() == null) {
			return create(orderDetails);
		}
		OrderDetails result = orderDetailsRepository.save(orderDetails);
		orderDetailsSearchRepository.save(orderDetails);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("orderDetails", orderDetails.getId().toString()))
				.body(result);
	}

	/**
	 * GET /orderDetailss -> get all the orderDetailss.
	 */
	@RequestMapping(value = "/orderDetailss", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderDetails> getAll(@RequestParam(required = false) String filter) {
		if ("orderstatusdetails-is-null".equals(filter)) {
			log.debug("REST request to get all OrderDetailss where orderstatusdetails is null");
			return StreamSupport.stream(orderDetailsRepository.findAll().spliterator(), false)
					.filter(orderDetails -> orderDetails.getOrderstatusdetails() == null).collect(Collectors.toList());
		}

		log.debug("REST request to get all OrderDetailss");
		return orderDetailsRepository.findAll();
	}

	/**
	 * GET /orderDetailss -> get all the orderDetailss.
	 */
	@RequestMapping(value = "/orderDetails/{orderId}/orderId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderDetails> getOrderDetailsByOrderId(@PathVariable("orderId") Long orderId) {
		log.debug("REST request to get order details by order id");
		List<OrderDetails> orderDetails = orderService.findOrderDetailsByOrderId(orderId);
		return orderDetails;
	}

	/**
	 * GET /orderDetailss/:id -> get the "id" orderDetails.
	 */
	@RequestMapping(value = "/orderDetailss/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderDetails> get(@PathVariable Long id) {
		log.debug("REST request to get OrderDetails : {}", id);
		return Optional.ofNullable(orderDetailsRepository.findOne(id))
				.map(orderDetails -> new ResponseEntity<>(orderDetails, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /orderDetailss/:id -> delete the "id" orderDetails.
	 */
	@RequestMapping(value = "/orderDetailss/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete OrderDetails : {}", id);
		orderDetailsRepository.delete(id);
		orderDetailsSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("orderDetails", id.toString())).build();
	}

	/**
	 * SEARCH /_search/orderDetailss/:query -> search for the orderDetails
	 * corresponding to the query.
	 */
	@RequestMapping(value = "/_search/orderDetailss/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderDetails> search(@PathVariable String query) {
		return StreamSupport.stream(orderDetailsSearchRepository.search(queryString(query)).spliterator(), false)
				.collect(Collectors.toList());
	}
}
