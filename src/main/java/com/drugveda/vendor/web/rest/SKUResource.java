package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.SKU;
import com.drugveda.vendor.repository.SKURepository;
import com.drugveda.vendor.repository.search.SKUSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing SKU.
 */
@RestController
@RequestMapping("/api")
public class SKUResource {

    private final Logger log = LoggerFactory.getLogger(SKUResource.class);

    @Inject
    private SKURepository sKURepository;

    @Inject
    private SKUSearchRepository sKUSearchRepository;

    /**
     * POST  /sKUs -> Create a new sKU.
     */
    @RequestMapping(value = "/sKUs",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SKU> create(@RequestBody SKU sKU) throws URISyntaxException {
        log.debug("REST request to save SKU : {}", sKU);
        if (sKU.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new sKU cannot already have an ID").body(null);
        }
        SKU result = sKURepository.save(sKU);
        sKUSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/sKUs/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("sKU", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /sKUs -> Updates an existing sKU.
     */
    @RequestMapping(value = "/sKUs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SKU> update(@RequestBody SKU sKU) throws URISyntaxException {
        log.debug("REST request to update SKU : {}", sKU);
        if (sKU.getId() == null) {
            return create(sKU);
        }
        SKU result = sKURepository.save(sKU);
        sKUSearchRepository.save(sKU);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("sKU", sKU.getId().toString()))
                .body(result);
    }

    /**
     * GET  /sKUs -> get all the sKUs.
     */
    @RequestMapping(value = "/sKUs",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SKU> getAll() {
        log.debug("REST request to get all SKUs");
        return sKURepository.findAll();
    }

    /**
     * GET  /sKUs/:id -> get the "id" sKU.
     */
    @RequestMapping(value = "/sKUs/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<SKU> get(@PathVariable Long id) {
        log.debug("REST request to get SKU : {}", id);
        return Optional.ofNullable(sKURepository.findOne(id))
            .map(sKU -> new ResponseEntity<>(
                sKU,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /sKUs/:id -> delete the "id" sKU.
     */
    @RequestMapping(value = "/sKUs/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete SKU : {}", id);
        sKURepository.delete(id);
        sKUSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("sKU", id.toString())).build();
    }

    /**
     * SEARCH  /_search/sKUs/:query -> search for the sKU corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/sKUs/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<SKU> search(@PathVariable String query) {
        return StreamSupport
            .stream(sKUSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
