package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.VariationOption;
import com.drugveda.vendor.repository.VariationOptionRepository;
import com.drugveda.vendor.repository.search.VariationOptionSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing VariationOption.
 */
@RestController
@RequestMapping("/api")
public class VariationOptionResource {

    private final Logger log = LoggerFactory.getLogger(VariationOptionResource.class);

    @Inject
    private VariationOptionRepository variationOptionRepository;

    @Inject
    private VariationOptionSearchRepository variationOptionSearchRepository;

    /**
     * POST  /variationOptions -> Create a new variationOption.
     */
    @RequestMapping(value = "/variationOptions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VariationOption> create(@RequestBody VariationOption variationOption) throws URISyntaxException {
        log.debug("REST request to save VariationOption : {}", variationOption);
        if (variationOption.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new variationOption cannot already have an ID").body(null);
        }
        VariationOption result = variationOptionRepository.save(variationOption);
        variationOptionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/variationOptions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("variationOption", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /variationOptions -> Updates an existing variationOption.
     */
    @RequestMapping(value = "/variationOptions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VariationOption> update(@RequestBody VariationOption variationOption) throws URISyntaxException {
        log.debug("REST request to update VariationOption : {}", variationOption);
        if (variationOption.getId() == null) {
            return create(variationOption);
        }
        VariationOption result = variationOptionRepository.save(variationOption);
        variationOptionSearchRepository.save(variationOption);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("variationOption", variationOption.getId().toString()))
                .body(result);
    }

    /**
     * GET  /variationOptions -> get all the variationOptions.
     */
    @RequestMapping(value = "/variationOptions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<VariationOption> getAll() {
        log.debug("REST request to get all VariationOptions");
        return variationOptionRepository.findAll();
    }

    /**
     * GET  /variationOptions/:id -> get the "id" variationOption.
     */
    @RequestMapping(value = "/variationOptions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<VariationOption> get(@PathVariable Long id) {
        log.debug("REST request to get VariationOption : {}", id);
        return Optional.ofNullable(variationOptionRepository.findOne(id))
            .map(variationOption -> new ResponseEntity<>(
                variationOption,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /variationOptions/:id -> delete the "id" variationOption.
     */
    @RequestMapping(value = "/variationOptions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete VariationOption : {}", id);
        variationOptionRepository.delete(id);
        variationOptionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("variationOption", id.toString())).build();
    }

    /**
     * SEARCH  /_search/variationOptions/:query -> search for the variationOption corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/variationOptions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<VariationOption> search(@PathVariable String query) {
        return StreamSupport
            .stream(variationOptionSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
