package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.ProductCategory;
import com.drugveda.vendor.repository.ProductCategoryRepository;
import com.drugveda.vendor.repository.search.ProductCategorySearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ProductCategory.
 */
@RestController
@RequestMapping("/api")
public class ProductCategoryResource {

    private final Logger log = LoggerFactory.getLogger(ProductCategoryResource.class);

    @Inject
    private ProductCategoryRepository productCategoryRepository;

    @Inject
    private ProductCategorySearchRepository productCategorySearchRepository;

    /**
     * POST  /productCategorys -> Create a new productCategory.
     */
    @RequestMapping(value = "/productCategorys",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductCategory> create(@RequestBody ProductCategory productCategory) throws URISyntaxException {
        log.debug("REST request to save ProductCategory : {}", productCategory);
        if (productCategory.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new productCategory cannot already have an ID").body(null);
        }
        ProductCategory result = productCategoryRepository.save(productCategory);
        productCategorySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/productCategorys/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("productCategory", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /productCategorys -> Updates an existing productCategory.
     */
    @RequestMapping(value = "/productCategorys",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductCategory> update(@RequestBody ProductCategory productCategory) throws URISyntaxException {
        log.debug("REST request to update ProductCategory : {}", productCategory);
        if (productCategory.getId() == null) {
            return create(productCategory);
        }
        ProductCategory result = productCategoryRepository.save(productCategory);
        productCategorySearchRepository.save(productCategory);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("productCategory", productCategory.getId().toString()))
                .body(result);
    }

    /**
     * GET  /productCategorys -> get all the productCategorys.
     */
    @RequestMapping(value = "/productCategorys",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductCategory> getAll() {
        log.debug("REST request to get all ProductCategorys");
        return productCategoryRepository.findAll();
    }

    /**
     * GET  /productCategorys/:id -> get the "id" productCategory.
     */
    @RequestMapping(value = "/productCategorys/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductCategory> get(@PathVariable Long id) {
        log.debug("REST request to get ProductCategory : {}", id);
        return Optional.ofNullable(productCategoryRepository.findOne(id))
            .map(productCategory -> new ResponseEntity<>(
                productCategory,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /productCategorys/:id -> delete the "id" productCategory.
     */
    @RequestMapping(value = "/productCategorys/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete ProductCategory : {}", id);
        productCategoryRepository.delete(id);
        productCategorySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productCategory", id.toString())).build();
    }

    /**
     * SEARCH  /_search/productCategorys/:query -> search for the productCategory corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/productCategorys/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductCategory> search(@PathVariable String query) {
        return StreamSupport
            .stream(productCategorySearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
