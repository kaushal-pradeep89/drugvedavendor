package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.GPSLocation;
import com.drugveda.vendor.repository.GPSLocationRepository;
import com.drugveda.vendor.repository.search.GPSLocationSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing GPSLocation.
 */
@RestController
@RequestMapping("/api")
public class GPSLocationResource {

    private final Logger log = LoggerFactory.getLogger(GPSLocationResource.class);

    @Inject
    private GPSLocationRepository gPSLocationRepository;

    @Inject
    private GPSLocationSearchRepository gPSLocationSearchRepository;

    /**
     * POST  /gPSLocations -> Create a new gPSLocation.
     */
    @RequestMapping(value = "/gPSLocations",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GPSLocation> create(@RequestBody GPSLocation gPSLocation) throws URISyntaxException {
        log.debug("REST request to save GPSLocation : {}", gPSLocation);
        if (gPSLocation.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new gPSLocation cannot already have an ID").body(null);
        }
        GPSLocation result = gPSLocationRepository.save(gPSLocation);
        gPSLocationSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/gPSLocations/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("gPSLocation", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /gPSLocations -> Updates an existing gPSLocation.
     */
    @RequestMapping(value = "/gPSLocations",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GPSLocation> update(@RequestBody GPSLocation gPSLocation) throws URISyntaxException {
        log.debug("REST request to update GPSLocation : {}", gPSLocation);
        if (gPSLocation.getId() == null) {
            return create(gPSLocation);
        }
        GPSLocation result = gPSLocationRepository.save(gPSLocation);
        gPSLocationSearchRepository.save(gPSLocation);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("gPSLocation", gPSLocation.getId().toString()))
                .body(result);
    }

    /**
     * GET  /gPSLocations -> get all the gPSLocations.
     */
    @RequestMapping(value = "/gPSLocations",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<GPSLocation> getAll() {
        log.debug("REST request to get all GPSLocations");
        return gPSLocationRepository.findAll();
    }

    /**
     * GET  /gPSLocations/:id -> get the "id" gPSLocation.
     */
    @RequestMapping(value = "/gPSLocations/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<GPSLocation> get(@PathVariable Long id) {
        log.debug("REST request to get GPSLocation : {}", id);
        return Optional.ofNullable(gPSLocationRepository.findOne(id))
            .map(gPSLocation -> new ResponseEntity<>(
                gPSLocation,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /gPSLocations/:id -> delete the "id" gPSLocation.
     */
    @RequestMapping(value = "/gPSLocations/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete GPSLocation : {}", id);
        gPSLocationRepository.delete(id);
        gPSLocationSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("gPSLocation", id.toString())).build();
    }

    /**
     * SEARCH  /_search/gPSLocations/:query -> search for the gPSLocation corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/gPSLocations/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<GPSLocation> search(@PathVariable String query) {
        return StreamSupport
            .stream(gPSLocationSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
