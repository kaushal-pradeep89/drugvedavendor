package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.Transaction;
import com.drugveda.vendor.repository.TransactionRepository;
import com.drugveda.vendor.repository.search.TransactionSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Transaction.
 */
@RestController
@RequestMapping("/api")
public class TransactionResource {

	private final Logger log = LoggerFactory.getLogger(TransactionResource.class);

	@Inject
	private TransactionRepository transactionRepository;

	@Inject
	private TransactionSearchRepository transactionSearchRepository;

	/**
	 * POST /transactions -> Create a new transaction.
	 */
	@RequestMapping(value = "/transactions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Transaction> create(@RequestBody Transaction transaction) throws URISyntaxException {
		log.debug("REST request to save Transaction : {}", transaction);
		if (transaction.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new transaction cannot already have an ID")
					.body(null);
		}
		Transaction result = transactionRepository.save(transaction);
		transactionSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/transactions/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("transaction", result.getId().toString())).body(result);
	}

	/**
	 * PUT /transactions -> Updates an existing transaction.
	 */
	@RequestMapping(value = "/transactions", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Transaction> update(@RequestBody Transaction transaction) throws URISyntaxException {
		log.debug("REST request to update Transaction : {}", transaction);
		if (transaction.getId() == null) {
			return create(transaction);
		}
		Transaction result = transactionRepository.save(transaction);
		transactionSearchRepository.save(transaction);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("transaction", transaction.getId().toString()))
				.body(result);
	}

	/**
	 * GET /transactions -> get all the transactions.
	 */
	@RequestMapping(value = "/transactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Transaction> getAll(@RequestParam(required = false) String filter) {
		if ("order-is-null".equals(filter)) {
			log.debug("REST request to get all Transactions where order is null");
			return StreamSupport.stream(transactionRepository.findAll().spliterator(), false)

			  .collect(Collectors.toList());
		}

		log.debug("REST request to get all Transactions");
		return transactionRepository.findAll();
	}

	/**
	 * GET /transactions/:id -> get the "id" transaction.
	 */
	@RequestMapping(value = "/transactions/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Transaction> get(@PathVariable Long id) {
		log.debug("REST request to get Transaction : {}", id);
		return Optional.ofNullable(transactionRepository.findOne(id))
				.map(transaction -> new ResponseEntity<>(transaction, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /transactions/:id -> delete the "id" transaction.
	 */
	@RequestMapping(value = "/transactions/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete Transaction : {}", id);
		transactionRepository.delete(id);
		transactionSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("transaction", id.toString())).build();
	}

	/**
	 * SEARCH /_search/transactions/:query -> search for the transaction
	 * corresponding to the query.
	 */
	@RequestMapping(value = "/_search/transactions/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Transaction> search(@PathVariable String query) {
		return StreamSupport.stream(transactionSearchRepository.search(queryString(query)).spliterator(), false)
				.collect(Collectors.toList());
	}
}
