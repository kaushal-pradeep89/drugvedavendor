package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.Price;
import com.drugveda.vendor.domain.ProductCatalogue;
import com.drugveda.vendor.domain.User;
import com.drugveda.vendor.domain.Vendor;
import com.drugveda.vendor.repository.PriceRepository;
import com.drugveda.vendor.repository.ProductCatalogueRepository;
import com.drugveda.vendor.repository.VendorDAO;
import com.drugveda.vendor.repository.search.ProductCatalogueSearchRepository;
import com.drugveda.vendor.service.UserService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ProductCatalogue.
 */
@RestController
@RequestMapping("/api")
public class ProductCatalogueResource {

	private final Logger log = LoggerFactory.getLogger(ProductCatalogueResource.class);

	@Inject
	private ProductCatalogueRepository productCatalogueRepository;

	@Inject
	private PriceRepository priceRepository;

	@Inject
	private ProductCatalogueSearchRepository productCatalogueSearchRepository;
	
	@Inject
	private UserService userService;
	
	@Inject
	private VendorDAO vendorDao;

	/**
	 * POST /productCatalogues -> Create a new productCatalogue.
	 */
	@RequestMapping(value = "/productCatalogues", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductCatalogue> create(@RequestBody ProductCatalogue productCatalogue)
			throws URISyntaxException {
		log.debug("REST request to save ProductCatalogue : {}", productCatalogue);
		if (productCatalogue.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new productCatalogue cannot already have an ID")
					.body(null);
		}
		Price price = productCatalogue.getPrice();
		price = priceRepository.save(price);
		
		Long userId = getUserId();
		Vendor vendor = vendorDao.getVendorByUserId(userId);
		
		productCatalogue.setVendor(vendor);
		productCatalogue.setPrice(price);
		ProductCatalogue result = productCatalogueRepository.save(productCatalogue);
		productCatalogueSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/productCatalogues/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("productCatalogue", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /productCatalogues -> Updates an existing productCatalogue.
	 */
	@RequestMapping(value = "/productCatalogues", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductCatalogue> update(@RequestBody ProductCatalogue productCatalogue)
			throws URISyntaxException {
		log.debug("REST request to update ProductCatalogue : {}", productCatalogue);
		if (productCatalogue.getId() == null) {
			return create(productCatalogue);
		}
		Price price = productCatalogue.getPrice();
		price = priceRepository.save(price);
		
		Long userId = getUserId();
		Vendor vendor = vendorDao.getVendorByUserId(userId);
		
		productCatalogue.setVendor(vendor);
		productCatalogue.setPrice(price);
		ProductCatalogue result = productCatalogueRepository.save(productCatalogue);
		productCatalogueSearchRepository.save(productCatalogue);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("productCatalogue", productCatalogue.getId().toString()))
				.body(result);
	}

	/**
	 * GET /productCatalogues -> get all the productCatalogues.
	 */
	@RequestMapping(value = "/productCatalogues", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<ProductCatalogue> getAll() {
		log.debug("REST request to get all ProductCatalogues");
		return productCatalogueRepository.findAll();
	}

	/**
	 * GET /productCatalogues/:id -> get the "id" productCatalogue.
	 */
	@RequestMapping(value = "/productCatalogues/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductCatalogue> get(@PathVariable Long id) {
		log.debug("REST request to get ProductCatalogue : {}", id);
		
		ResponseEntity<ProductCatalogue> obj=Optional.ofNullable(productCatalogueRepository.findOne(id))
		.map(productCatalogue -> new ResponseEntity<>(productCatalogue, HttpStatus.OK))
		.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
		return obj;
	}

	/**
	 * DELETE /productCatalogues/:id -> delete the "id" productCatalogue.
	 */
	@RequestMapping(value = "/productCatalogues/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete ProductCatalogue : {}", id);
		productCatalogueRepository.delete(id);
		productCatalogueSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productCatalogue", id.toString()))
				.build();
	}

	/**
	 * SEARCH /_search/productCatalogues/:query -> search for the
	 * productCatalogue corresponding to the query.
	 */
	@RequestMapping(value = "/_search/productCatalogues/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<ProductCatalogue> search(@PathVariable String query) {
		return StreamSupport.stream(productCatalogueSearchRepository.search(queryString(query)).spliterator(), false)
				.collect(Collectors.toList());
	}
	
	/**
	 * Get the user id for the logged in user.
	 * 
	 * @return
	 */
	private Long getUserId() {
		User user = userService.getUserWithAuthorities();

		Long userId = user.getId();
		return userId;
	}

}
