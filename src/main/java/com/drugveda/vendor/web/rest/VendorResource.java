package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.Vendor;
import com.drugveda.vendor.repository.VendorRepository;
import com.drugveda.vendor.repository.search.VendorSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Vendor.
 */
@RestController
@RequestMapping("/api")
public class VendorResource {

    private final Logger log = LoggerFactory.getLogger(VendorResource.class);

    @Inject
    private VendorRepository vendorRepository;

    @Inject
    private VendorSearchRepository vendorSearchRepository;

    /**
     * POST  /vendors -> Create a new vendor.
     */
    @RequestMapping(value = "/vendors",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vendor> create(@RequestBody Vendor vendor) throws URISyntaxException {
        log.debug("REST request to save Vendor : {}", vendor);
        if (vendor.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new vendor cannot already have an ID").body(null);
        }
        Vendor result = vendorRepository.save(vendor);
        vendorSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/vendors/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("vendor", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /vendors -> Updates an existing vendor.
     */
    @RequestMapping(value = "/vendors",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vendor> update(@RequestBody Vendor vendor) throws URISyntaxException {
        log.debug("REST request to update Vendor : {}", vendor);
        if (vendor.getId() == null) {
            return create(vendor);
        }
        Vendor result = vendorRepository.save(vendor);
        vendorSearchRepository.save(vendor);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("vendor", vendor.getId().toString()))
                .body(result);
    }

    /**
     * GET  /vendors -> get all the vendors.
     */
    @RequestMapping(value = "/vendors",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Vendor> getAll() {
        log.debug("REST request to get all Vendors");
        return vendorRepository.findAll();
    }

    /**
     * GET  /vendors/:id -> get the "id" vendor.
     */
    @RequestMapping(value = "/vendors/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Vendor> get(@PathVariable Long id) {
        log.debug("REST request to get Vendor : {}", id);
        return Optional.ofNullable(vendorRepository.findOne(id))
            .map(vendor -> new ResponseEntity<>(
                vendor,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /vendors/:id -> delete the "id" vendor.
     */
    @RequestMapping(value = "/vendors/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Vendor : {}", id);
        vendorRepository.delete(id);
        vendorSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("vendor", id.toString())).build();
    }

    /**
     * SEARCH  /_search/vendors/:query -> search for the vendor corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/vendors/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Vendor> search(@PathVariable String query) {
        return StreamSupport
            .stream(vendorSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
