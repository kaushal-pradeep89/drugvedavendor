package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.OrderStatus;
import com.drugveda.vendor.repository.OrderStatusRepository;
import com.drugveda.vendor.repository.search.OrderStatusSearchRepository;
import com.drugveda.vendor.service.OrderService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing OrderStatus.
 */
@RestController
@RequestMapping("/api")
public class OrderStatusResource {

	private final Logger log = LoggerFactory.getLogger(OrderStatusResource.class);

	@Inject
	private OrderStatusRepository orderStatusRepository;

	@Inject
	private OrderStatusSearchRepository orderStatusSearchRepository;

	@Inject
	private OrderService orderService;

	/**
	 * POST /orderStatuss -> Create a new orderStatus.
	 */
	@RequestMapping(value = "/orderStatuss", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderStatus> create(@RequestBody OrderStatus orderStatus) throws URISyntaxException {
		log.debug("REST request to save OrderStatus : {}", orderStatus);
		if (orderStatus.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new orderStatus cannot already have an ID")
					.body(null);
		}
		OrderStatus result = orderStatusRepository.save(orderStatus);
		orderStatusSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/orderStatuss/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("orderStatus", result.getId().toString())).body(result);
	}

	/**
	 * PUT /orderStatuss -> Updates an existing orderStatus.
	 */
	@RequestMapping(value = "/orderStatuss", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderStatus> update(@RequestBody OrderStatus orderStatus) throws URISyntaxException {
		log.debug("REST request to update OrderStatus : {}", orderStatus);
		if (orderStatus.getId() == null) {
			return create(orderStatus);
		}
		// OrderStatus result = orderStatusRepository.save(orderStatus);
		OrderStatus result = orderService.changeOrderStatus(orderStatus);
		orderStatusSearchRepository.save(orderStatus);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("orderStatus", orderStatus.getId().toString()))
				.body(result);
	}

	/**
	 * GET /orderStatuss -> get all the orderStatuss.
	 */
	@RequestMapping(value = "/orderStatuss", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderStatus> getAll() {
		log.debug("REST request to get all OrderStatuss");
		return orderStatusRepository.findAll();
	}

	/**
	 * GET /orderStatuss/:id -> get the "id" orderStatus.
	 */
	@RequestMapping(value = "/orderStatuss/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderStatus> get(@PathVariable Long id) {
		log.debug("REST request to get OrderStatus : {}", id);
		return Optional.ofNullable(orderStatusRepository.findOne(id))
				.map(orderStatus -> new ResponseEntity<>(orderStatus, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * GET /orderStatuss/:id/orderId
	 */
	@RequestMapping(value = "/orderStatuss/{orderId}/orderId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderStatus> getOrderStatusByOrderId(@PathVariable Long orderId) {
		log.debug("REST request to get OrderStatus : {}", orderId);
		OrderStatus orderStatus = orderService.getOrderStatus(orderId);
		ResponseEntity<OrderStatus> responseEntity = new ResponseEntity<OrderStatus>(orderStatus, HttpStatus.OK);
		return responseEntity;
	}

	/**
	 * DELETE /orderStatuss/:id -> delete the "id" orderStatus.
	 */
	@RequestMapping(value = "/orderStatuss/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete OrderStatus : {}", id);
		orderStatusRepository.delete(id);
		orderStatusSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("orderStatus", id.toString())).build();
	}

	/**
	 * SEARCH /_search/orderStatuss/:query -> search for the orderStatus
	 * corresponding to the query.
	 */
	@RequestMapping(value = "/_search/orderStatuss/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderStatus> search(@PathVariable String query) {
		return StreamSupport.stream(orderStatusSearchRepository.search(queryString(query)).spliterator(), false)
				.collect(Collectors.toList());
	}
}
