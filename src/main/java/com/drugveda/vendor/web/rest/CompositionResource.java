package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.Composition;
import com.drugveda.vendor.repository.CompositionRepository;
import com.drugveda.vendor.repository.search.CompositionSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Composition.
 */
@RestController
@RequestMapping("/api")
public class CompositionResource {

    private final Logger log = LoggerFactory.getLogger(CompositionResource.class);

    @Inject
    private CompositionRepository compositionRepository;

    @Inject
    private CompositionSearchRepository compositionSearchRepository;

    /**
     * POST  /compositions -> Create a new composition.
     */
    @RequestMapping(value = "/compositions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Composition> create(@RequestBody Composition composition) throws URISyntaxException {
        log.debug("REST request to save Composition : {}", composition);
        if (composition.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new composition cannot already have an ID").body(null);
        }
        Composition result = compositionRepository.save(composition);
        compositionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/compositions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("composition", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /compositions -> Updates an existing composition.
     */
    @RequestMapping(value = "/compositions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Composition> update(@RequestBody Composition composition) throws URISyntaxException {
        log.debug("REST request to update Composition : {}", composition);
        if (composition.getId() == null) {
            return create(composition);
        }
        Composition result = compositionRepository.save(composition);
        compositionSearchRepository.save(composition);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("composition", composition.getId().toString()))
                .body(result);
    }

    /**
     * GET  /compositions -> get all the compositions.
     */
    @RequestMapping(value = "/compositions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Composition> getAll() {
        log.debug("REST request to get all Compositions");
        return compositionRepository.findAll();
    }

    /**
     * GET  /compositions/:id -> get the "id" composition.
     */
    @RequestMapping(value = "/compositions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Composition> get(@PathVariable Long id) {
        log.debug("REST request to get Composition : {}", id);
        return Optional.ofNullable(compositionRepository.findOne(id))
            .map(composition -> new ResponseEntity<>(
                composition,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /compositions/:id -> delete the "id" composition.
     */
    @RequestMapping(value = "/compositions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Composition : {}", id);
        compositionRepository.delete(id);
        compositionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("composition", id.toString())).build();
    }

    /**
     * SEARCH  /_search/compositions/:query -> search for the composition corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/compositions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Composition> search(@PathVariable String query) {
        return StreamSupport
            .stream(compositionSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
