package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.Tax;
import com.drugveda.vendor.repository.TaxRepository;
import com.drugveda.vendor.repository.search.TaxSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Tax.
 */
@RestController
@RequestMapping("/api")
public class TaxResource {

    private final Logger log = LoggerFactory.getLogger(TaxResource.class);

    @Inject
    private TaxRepository taxRepository;

    @Inject
    private TaxSearchRepository taxSearchRepository;

    /**
     * POST  /taxs -> Create a new tax.
     */
    @RequestMapping(value = "/taxs",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tax> create(@RequestBody Tax tax) throws URISyntaxException {
        log.debug("REST request to save Tax : {}", tax);
        if (tax.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new tax cannot already have an ID").body(null);
        }
        Tax result = taxRepository.save(tax);
        taxSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/taxs/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("tax", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /taxs -> Updates an existing tax.
     */
    @RequestMapping(value = "/taxs",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tax> update(@RequestBody Tax tax) throws URISyntaxException {
        log.debug("REST request to update Tax : {}", tax);
        if (tax.getId() == null) {
            return create(tax);
        }
        Tax result = taxRepository.save(tax);
        taxSearchRepository.save(tax);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("tax", tax.getId().toString()))
                .body(result);
    }

    /**
     * GET  /taxs -> get all the taxs.
     */
    @RequestMapping(value = "/taxs",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Tax> getAll() {
        log.debug("REST request to get all Taxs");
        return taxRepository.findAll();
    }

    /**
     * GET  /taxs/:id -> get the "id" tax.
     */
    @RequestMapping(value = "/taxs/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Tax> get(@PathVariable Long id) {
        log.debug("REST request to get Tax : {}", id);
        return Optional.ofNullable(taxRepository.findOne(id))
            .map(tax -> new ResponseEntity<>(
                tax,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /taxs/:id -> delete the "id" tax.
     */
    @RequestMapping(value = "/taxs/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete Tax : {}", id);
        taxRepository.delete(id);
        taxSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("tax", id.toString())).build();
    }

    /**
     * SEARCH  /_search/taxs/:query -> search for the tax corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/taxs/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<Tax> search(@PathVariable String query) {
        return StreamSupport
            .stream(taxSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
