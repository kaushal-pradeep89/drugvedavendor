package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.ProductImage;
import com.drugveda.vendor.repository.ProductImageRepository;
import com.drugveda.vendor.repository.ProductRepository;
import com.drugveda.vendor.repository.search.ProductImageSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ProductImage.
 */
@RestController
@RequestMapping("/api")
public class ProductImageResource {

	private final Logger log = LoggerFactory.getLogger(ProductImageResource.class);

	@Inject
	private ProductImageRepository productImageRepository;
	
	@Inject
	private ProductRepository productRepository;

	@Inject
	private ProductImageSearchRepository productImageSearchRepository;

	@Inject
	private Environment env;

	/**
	 * POST /productImages -> Create a new productImage.
	 */
	@RequestMapping(value = "/productImages", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductImage> create(@RequestBody ProductImage productImage) throws URISyntaxException {
		log.debug("REST request to save ProductImage : {}", productImage);
		if (productImage.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new productImage cannot already have an ID")
					.body(null);
		}
		ProductImage result = productImageRepository.save(productImage);
		productImageSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/productImages/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("productImage", result.getId().toString())).body(result);
	}

	/**
	 * PUT /productImages -> Updates an existing productImage.
	 */
	@RequestMapping(value = "/productImages", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductImage> update(@RequestBody ProductImage productImage) throws URISyntaxException {
		log.debug("REST request to update ProductImage : {}", productImage);
		if (productImage.getId() == null) {
			return create(productImage);
		}
		ProductImage result = productImageRepository.save(productImage);
		productImageSearchRepository.save(productImage);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("productImage", productImage.getId().toString()))
				.body(result);
	}

	/**
	 * GET /productImages -> get all the productImages.
	 */
	@RequestMapping(value = "/productImages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<ProductImage> getAll() {
		log.debug("REST request to get all ProductImages");
		return productImageRepository.findAll();
	}

	/**
	 * GET /productImages/:id -> get the "id" productImage.
	 */
	@RequestMapping(value = "/productImages/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductImage> get(@PathVariable Long id) {
		log.debug("REST request to get ProductImage : {}", id);
		return Optional.ofNullable(productImageRepository.findOne(id))
				.map(productImage -> new ResponseEntity<>(productImage, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /productImages/:id -> delete the "id" productImage.
	 */
	@RequestMapping(value = "/productImages/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete ProductImage : {}", id);
		productImageRepository.delete(id);
		productImageSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productImage", id.toString())).build();
	}

	/**
	 * SEARCH /_search/productImages/:query -> search for the productImage
	 * corresponding to the query.
	 */
	@RequestMapping(value = "/_search/productImages/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<ProductImage> search(@PathVariable String query) {
		return StreamSupport.stream(productImageSearchRepository.search(queryString(query)).spliterator(), false)
				.collect(Collectors.toList());
	}


}
