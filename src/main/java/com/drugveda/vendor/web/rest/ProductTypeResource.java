package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.ProductType;
import com.drugveda.vendor.repository.ProductTypeRepository;
import com.drugveda.vendor.repository.search.ProductTypeSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ProductType.
 */
@RestController
@RequestMapping("/api")
public class ProductTypeResource {

    private final Logger log = LoggerFactory.getLogger(ProductTypeResource.class);

    @Inject
    private ProductTypeRepository productTypeRepository;

    @Inject
    private ProductTypeSearchRepository productTypeSearchRepository;

    /**
     * POST  /productTypes -> Create a new productType.
     */
    @RequestMapping(value = "/productTypes",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductType> create(@RequestBody ProductType productType) throws URISyntaxException {
        log.debug("REST request to save ProductType : {}", productType);
        if (productType.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new productType cannot already have an ID").body(null);
        }
        ProductType result = productTypeRepository.save(productType);
        productTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/productTypes/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("productType", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /productTypes -> Updates an existing productType.
     */
    @RequestMapping(value = "/productTypes",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductType> update(@RequestBody ProductType productType) throws URISyntaxException {
        log.debug("REST request to update ProductType : {}", productType);
        if (productType.getId() == null) {
            return create(productType);
        }
        ProductType result = productTypeRepository.save(productType);
        productTypeSearchRepository.save(productType);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("productType", productType.getId().toString()))
                .body(result);
    }

    /**
     * GET  /productTypes -> get all the productTypes.
     */
    @RequestMapping(value = "/productTypes",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductType> getAll(@RequestParam(required = false) String filter) {
        /*if ("product-is-null".equals(filter)) {
            log.debug("REST request to get all ProductTypes where product is null");
            return StreamSupport
                .stream(productTypeRepository.findAll().spliterator(), false)
                .filter(productType -> productType.getProduct() == null)
                .collect(Collectors.toList());
        }*/

        log.debug("REST request to get all ProductTypes");
        return productTypeRepository.findAll();
    }

    /**
     * GET  /productTypes/:id -> get the "id" productType.
     */
    @RequestMapping(value = "/productTypes/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductType> get(@PathVariable Long id) {
        log.debug("REST request to get ProductType : {}", id);
        return Optional.ofNullable(productTypeRepository.findOne(id))
            .map(productType -> new ResponseEntity<>(
                productType,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /productTypes/:id -> delete the "id" productType.
     */
    @RequestMapping(value = "/productTypes/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete ProductType : {}", id);
        productTypeRepository.delete(id);
        productTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productType", id.toString())).build();
    }

    /**
     * SEARCH  /_search/productTypes/:query -> search for the productType corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/productTypes/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductType> search(@PathVariable String query) {
        return StreamSupport
            .stream(productTypeSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
