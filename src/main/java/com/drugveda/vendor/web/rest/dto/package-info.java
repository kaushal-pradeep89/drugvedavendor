/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.drugveda.vendor.web.rest.dto;
