package com.drugveda.vendor.web.rest;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.Product;
import com.drugveda.vendor.repository.ProductRepository;
import com.drugveda.vendor.repository.search.ProductSearchRepository;
import com.drugveda.vendor.service.ProductSearchService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Product.
 */
@RestController
@RequestMapping("/api")
public class ProductResource {

	private final Logger log = LoggerFactory.getLogger(ProductResource.class);

	@Inject
	private ProductRepository productRepository;

	@Inject
	private ProductSearchRepository productSearchRepository;

	@Inject
	private ProductSearchService productSearchService;

	/**
	 * POST /products -> Create a new product.
	 */
	@RequestMapping(value = "/products", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Product> create(@RequestBody Product product) throws URISyntaxException {
		log.debug("REST request to save Product : {}", product);
		if (product.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new product cannot already have an ID").body(null);
		}
		Product result = productRepository.save(product);
		productSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/products/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("product", result.getId().toString())).body(result);
	}

	/**
	 * PUT /products -> Updates an existing product.
	 */
	@RequestMapping(value = "/products", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Product> update(@RequestBody Product product) throws URISyntaxException {
		log.debug("REST request to update Product : {}", product);
		if (product.getId() == null) {
			return create(product);
		}
		Product result = productRepository.save(product);
		productSearchRepository.save(product);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("product", product.getId().toString()))
				.body(result);
	}

	/**
	 * GET /products -> get all the products.
	 */
	@RequestMapping(value = "/products", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Product> getAll() {
		log.debug("REST request to get all Products");
		return productRepository.findAll();
	}

	/**
	 * GET /products/:id -> get the "id" product.
	 */
	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Product> get(@PathVariable Long id) {
		log.debug("REST request to get Product : {}", id);
		return Optional.ofNullable(productRepository.findOne(id))
				.map(product -> new ResponseEntity<>(product, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /products/:id -> delete the "id" product.
	 */
	@RequestMapping(value = "/products/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete Product : {}", id);
		productRepository.delete(id);
		productSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("product", id.toString())).build();
	}

	/**
	 * SEARCH /_search/products/:query -> search for the product corresponding
	 * to the query.
	 * 
	 * @RequestMapping(value = "/_search/products/{query}", method =
	 *                       RequestMethod.GET, produces =
	 *                       MediaType.APPLICATION_JSON_VALUE)
	 * @Timed public List<Product> search(@PathVariable String query) { return
	 *        StreamSupport
	 *        .stream(productSearchRepository.search(queryString(query)).
	 *        spliterator(), false) .collect(Collectors.toList()); }
	 */

	/**
	 * SEARCH /_search/products/:query -> search for the product corresponding
	 * to the query.
	 */
	@RequestMapping(value = "/_search/products/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<Product> search(@PathVariable String query) {
		log.debug("REST request to search Product : {}", query);
		if (query == null || query.isEmpty()) {
			throw new BadRequestException("Please check query");
		}
		List<Product> products = productSearchService.findProduct(query);

		return products;
	}
}
