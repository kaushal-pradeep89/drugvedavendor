package com.drugveda.vendor.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.CartDTO;
import com.drugveda.vendor.domain.User;
import com.drugveda.vendor.service.CartService;
import com.drugveda.vendor.service.UserService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing Cart.
 */
@RestController
@RequestMapping("/api")
public class CartResource {

	private final Logger log = LoggerFactory.getLogger(CartResource.class);

	@Inject
	private CartService cartService;

	@Inject
	private UserService userService;

	/**
	 * POST /carts -> Create a new cart.
	 */
	@RequestMapping(value = "/carts", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<CartDTO> create(@RequestBody CartDTO cartDto) throws URISyntaxException {
		log.debug("REST request to save Cart : {}", cartDto);
		if (cartDto.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new cart cannot already have an ID").body(null);
		}
		CartDTO result = cartService.save(cartDto);
		if (result == null) {
			return ResponseEntity.badRequest().header("Failure", "We are unable to decide the your order").body(null);
		}

		return ResponseEntity.ok().headers(HeaderUtil.createEntityCreationAlert("cart", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /carts -> Updates an existing cart.
	 */
	@RequestMapping(value = "/carts", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<CartDTO> update(@RequestBody CartDTO cartDto) throws URISyntaxException {
		log.debug("REST request to update Cart : {}", cartDto);
		if (cartDto.getId() == null) {
			return create(cartDto);
		}
		CartDTO result = cartService.save(cartDto);
		
		if (result == null) {
			return ResponseEntity.badRequest().header("Failure", "We are unable to decide the your order").body(null);
		}

		return ResponseEntity.ok().headers(HeaderUtil.createEntityUpdateAlert("cart", cartDto.getId().toString()))
				.body(result);
	}

	/**
	 * GET /carts/:id -> get the "id" cart.
	 */
	@RequestMapping(value = "/carts/findall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<CartDTO> get() {
		Long userId = getUserId();
		log.debug("REST request to get Cart");
		List<CartDTO> cartItems = cartService.findCartItems(userId, null);
		return cartItems;
	}

	/**
	 * Get the user id for the logged in user.
	 * 
	 * @return
	 */
	private Long getUserId() {
		User user = userService.getUserWithAuthorities();

		Long userId = user.getId();
		return userId;
	}

}
