package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.ProductVariation;
import com.drugveda.vendor.repository.ProductVariationRepository;
import com.drugveda.vendor.repository.search.ProductVariationSearchRepository;
import com.drugveda.vendor.service.ProductCatalogueService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ProductVariation.
 */
@RestController
@RequestMapping("/api")
public class ProductVariationResource {

	private final Logger log = LoggerFactory.getLogger(ProductVariationResource.class);

	@Inject
	private ProductVariationRepository productVariationRepository;

	@Inject
	private ProductVariationSearchRepository productVariationSearchRepository;

	@Inject
	private ProductCatalogueService productCatalogueService;

	/**
	 * POST /productVariations -> Create a new productVariation.
	 */
	@RequestMapping(value = "/productVariations", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductVariation> create(@RequestBody ProductVariation productVariation)
			throws URISyntaxException {
		log.debug("REST request to save ProductVariation : {}", productVariation);
		if (productVariation.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new productVariation cannot already have an ID")
					.body(null);
		}
		ProductVariation result = productVariationRepository.save(productVariation);
		productVariationSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/productVariations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("productVariation", result.getId().toString()))
				.body(result);
	}

	/**
	 * POST /productVariations -> Create a new productVariation.
	 */
	@RequestMapping(value = "/prodVar/{id}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductVariation> createProductVariation(@PathVariable Long id,
			@RequestBody ProductVariation productVariation) throws URISyntaxException {
		log.debug("REST request to save ProductVariation : {}", productVariation);
		if (id == null) {
			return ResponseEntity.badRequest().header("Failure", "Product Catalogue ID can't be nul.")
					.body(null);
		}
		ProductVariation result = productCatalogueService.createProductVariation(id, productVariation);
		return ResponseEntity.created(new URI("/api/productVariations/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("productVariation", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /productVariations -> Updates an existing productVariation.
	 */
	@RequestMapping(value = "/productVariations", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductVariation> update(@RequestBody ProductVariation productVariation)
			throws URISyntaxException {
		log.debug("REST request to update ProductVariation : {}", productVariation);
		if (productVariation.getId() == null) {
			return create(productVariation);
		}
		ProductVariation result = productVariationRepository.save(productVariation);
		productVariationSearchRepository.save(productVariation);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert("productVariation", productVariation.getId().toString()))
				.body(result);
	}

	/**
	 * GET /productVariations -> get all the productVariations.
	 */
	@RequestMapping(value = "/productVariations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<ProductVariation> getAll() {
		log.debug("REST request to get all ProductVariations");
		return productVariationRepository.findAll();
	}

	/**
	 * GET /productVariations/:id -> get the "id" productVariation.
	 */
	@RequestMapping(value = "/productVariations/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<ProductVariation> get(@PathVariable Long id) {
		log.debug("REST request to get ProductVariation : {}", id);
		return Optional.ofNullable(productVariationRepository.findOne(id))
				.map(productVariation -> new ResponseEntity<>(productVariation, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * DELETE /productVariations/:id -> delete the "id" productVariation.
	 */
	@RequestMapping(value = "/productVariations/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete ProductVariation : {}", id);
		productVariationRepository.delete(id);
		productVariationSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productVariation", id.toString()))
				.build();
	}

	/**
	 * SEARCH /_search/productVariations/:query -> search for the
	 * productVariation corresponding to the query.
	 */
	@RequestMapping(value = "/_search/productVariations/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<ProductVariation> search(@PathVariable String query) {
		return StreamSupport.stream(productVariationSearchRepository.search(queryString(query)).spliterator(), false)
				.collect(Collectors.toList());
	}
}
