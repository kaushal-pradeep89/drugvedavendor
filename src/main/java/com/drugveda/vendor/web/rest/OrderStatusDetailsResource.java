package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.OrderStatusDetails;
import com.drugveda.vendor.repository.OrderStatusDetailsRepository;
import com.drugveda.vendor.repository.search.OrderStatusDetailsSearchRepository;
import com.drugveda.vendor.service.OrderService;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing OrderStatusDetails.
 */
@RestController
@RequestMapping("/api")
public class OrderStatusDetailsResource {

	private final Logger log = LoggerFactory.getLogger(OrderStatusDetailsResource.class);

	@Inject
	private OrderStatusDetailsRepository orderStatusDetailsRepository;

	@Inject
	private OrderStatusDetailsSearchRepository orderStatusDetailsSearchRepository;

	@Inject
	private OrderService orderService;

	/**
	 * POST /orderStatusDetailss -> Create a new orderStatusDetails.
	 */
	@RequestMapping(value = "/orderStatusDetailss", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderStatusDetails> create(@RequestBody OrderStatusDetails orderStatusDetails)
			throws URISyntaxException {
		log.debug("REST request to save OrderStatusDetails : {}", orderStatusDetails);
		if (orderStatusDetails.getId() != null) {
			return ResponseEntity.badRequest().header("Failure", "A new orderStatusDetails cannot already have an ID")
					.body(null);
		}
		OrderStatusDetails result = orderStatusDetailsRepository.save(orderStatusDetails);
		orderStatusDetailsSearchRepository.save(result);
		return ResponseEntity.created(new URI("/api/orderStatusDetailss/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert("orderStatusDetails", result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /orderStatusDetailss -> Updates an existing orderStatusDetails.
	 */
	@RequestMapping(value = "/orderStatusDetailss", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderStatusDetails> update(@RequestBody OrderStatusDetails orderStatusDetails)
			throws URISyntaxException {
		log.debug("REST request to update OrderStatusDetails : {}", orderStatusDetails);
		if (orderStatusDetails.getId() == null) {
			return create(orderStatusDetails);
		}
		OrderStatusDetails result = orderStatusDetailsRepository.save(orderStatusDetails);
		orderStatusDetailsSearchRepository.save(orderStatusDetails);
		return ResponseEntity.ok()
				.headers(
						HeaderUtil.createEntityUpdateAlert("orderStatusDetails", orderStatusDetails.getId().toString()))
				.body(result);
	}

	/**
	 * GET /orderStatusDetailss -> get all the orderStatusDetailss.
	 */
	@RequestMapping(value = "/orderStatusDetailss", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderStatusDetails> getAll() {
		log.debug("REST request to get all OrderStatusDetailss");
		return orderStatusDetailsRepository.findAll();
	}

	/**
	 * GET /orderStatusDetailss/:id -> get the "id" orderStatusDetails.
	 */
	@RequestMapping(value = "/orderStatusDetailss/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<OrderStatusDetails> get(@PathVariable Long id) {
		log.debug("REST request to get OrderStatusDetails : {}", id);
		return Optional.ofNullable(orderStatusDetailsRepository.findOne(id))
				.map(orderStatusDetails -> new ResponseEntity<>(orderStatusDetails, HttpStatus.OK))
				.orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

	/**
	 * GET /orderStatusDetailss/:orderId/:statusId -> get the "id"
	 * orderStatusDetails.
	 */
	@RequestMapping(value = "/orderStatusDetailss/{orderId}/{statusId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderStatusDetails> get(@PathVariable Long orderId, @PathVariable Long statusId) {
		log.debug("REST request to get OrderStatusDetails : {}", orderId, statusId);
		List<OrderStatusDetails> orderStatusDetails = orderService.getOrderStatusDetails(orderId, statusId);
		return orderStatusDetails;
	}

	/**
	 * DELETE /orderStatusDetailss/:id -> delete the "id" orderStatusDetails.
	 */
	@RequestMapping(value = "/orderStatusDetailss/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		log.debug("REST request to delete OrderStatusDetails : {}", id);
		orderStatusDetailsRepository.delete(id);
		orderStatusDetailsSearchRepository.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("orderStatusDetails", id.toString()))
				.build();
	}

	/**
	 * SEARCH /_search/orderStatusDetailss/:query -> search for the
	 * orderStatusDetails corresponding to the query.
	 */
	@RequestMapping(value = "/_search/orderStatusDetailss/{query}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@Timed
	public List<OrderStatusDetails> search(@PathVariable String query) {
		return StreamSupport.stream(orderStatusDetailsSearchRepository.search(queryString(query)).spliterator(), false)
				.collect(Collectors.toList());
	}
}
