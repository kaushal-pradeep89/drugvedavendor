package com.drugveda.vendor.web.rest;

import static org.elasticsearch.index.query.QueryBuilders.queryString;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.drugveda.vendor.domain.ProductOption;
import com.drugveda.vendor.repository.ProductOptionRepository;
import com.drugveda.vendor.repository.search.ProductOptionSearchRepository;
import com.drugveda.vendor.web.rest.util.HeaderUtil;

/**
 * REST controller for managing ProductOption.
 */
@RestController
@RequestMapping("/api")
public class ProductOptionResource {

    private final Logger log = LoggerFactory.getLogger(ProductOptionResource.class);

    @Inject
    private ProductOptionRepository productOptionRepository;

    @Inject
    private ProductOptionSearchRepository productOptionSearchRepository;

    /**
     * POST  /productOptions -> Create a new productOption.
     */
    @RequestMapping(value = "/productOptions",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductOption> create(@RequestBody ProductOption productOption) throws URISyntaxException {
        log.debug("REST request to save ProductOption : {}", productOption);
        if (productOption.getId() != null) {
            return ResponseEntity.badRequest().header("Failure", "A new productOption cannot already have an ID").body(null);
        }
        ProductOption result = productOptionRepository.save(productOption);
        productOptionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/productOptions/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert("productOption", result.getId().toString()))
                .body(result);
    }

    /**
     * PUT  /productOptions -> Updates an existing productOption.
     */
    @RequestMapping(value = "/productOptions",
        method = RequestMethod.PUT,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductOption> update(@RequestBody ProductOption productOption) throws URISyntaxException {
        log.debug("REST request to update ProductOption : {}", productOption);
        if (productOption.getId() == null) {
            return create(productOption);
        }
        ProductOption result = productOptionRepository.save(productOption);
        productOptionSearchRepository.save(productOption);
        return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert("productOption", productOption.getId().toString()))
                .body(result);
    }

    /**
     * GET  /productOptions -> get all the productOptions.
     */
    @RequestMapping(value = "/productOptions",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductOption> getAll() {
        log.debug("REST request to get all ProductOptions");
        return productOptionRepository.findAll();
    }

    /**
     * GET  /productOptions/:id -> get the "id" productOption.
     */
    @RequestMapping(value = "/productOptions/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<ProductOption> get(@PathVariable Long id) {
        log.debug("REST request to get ProductOption : {}", id);
        return Optional.ofNullable(productOptionRepository.findOne(id))
            .map(productOption -> new ResponseEntity<>(
                productOption,
                HttpStatus.OK))
            .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    /**
     * DELETE  /productOptions/:id -> delete the "id" productOption.
     */
    @RequestMapping(value = "/productOptions/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        log.debug("REST request to delete ProductOption : {}", id);
        productOptionRepository.delete(id);
        productOptionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert("productOption", id.toString())).build();
    }

    /**
     * SEARCH  /_search/productOptions/:query -> search for the productOption corresponding
     * to the query.
     */
    @RequestMapping(value = "/_search/productOptions/{query}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    public List<ProductOption> search(@PathVariable String query) {
        return StreamSupport
            .stream(productOptionSearchRepository.search(queryString(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
}
