package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.Product;

/**
 * Spring Data JPA repository for the Product entity.
 */
public interface ProductRepository extends JpaRepository<Product,Long> {

}
