package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.GPSLocation;

/**
 * Spring Data ElasticSearch repository for the GPSLocation entity.
 */
public interface GPSLocationSearchRepository extends ElasticsearchRepository<GPSLocation, Long> {
}
