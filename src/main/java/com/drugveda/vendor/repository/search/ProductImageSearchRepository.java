package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.ProductImage;

/**
 * Spring Data ElasticSearch repository for the ProductImage entity.
 */
public interface ProductImageSearchRepository extends ElasticsearchRepository<ProductImage, Long> {
}
