package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.User;

/**
 * Spring Data ElasticSearch repository for the User entity.
 */
public interface UserSearchRepository extends ElasticsearchRepository<User, Long> {
}
