package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.ProductOption;

/**
 * Spring Data ElasticSearch repository for the ProductOption entity.
 */
public interface ProductOptionSearchRepository extends ElasticsearchRepository<ProductOption, Long> {
}
