package com.drugveda.vendor.repository.search;

import com.drugveda.vendor.domain.OrderStatus;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the OrderStatus entity.
 */
public interface OrderStatusSearchRepository extends ElasticsearchRepository<OrderStatus, Long> {
}
