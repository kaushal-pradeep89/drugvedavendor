package com.drugveda.vendor.repository.search;

import com.drugveda.vendor.domain.OrderStatusDetails;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the OrderStatusDetails entity.
 */
public interface OrderStatusDetailsSearchRepository extends ElasticsearchRepository<OrderStatusDetails, Long> {
}
