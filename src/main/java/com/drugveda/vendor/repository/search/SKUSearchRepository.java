package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.SKU;

/**
 * Spring Data ElasticSearch repository for the SKU entity.
 */
public interface SKUSearchRepository extends ElasticsearchRepository<SKU, Long> {
}
