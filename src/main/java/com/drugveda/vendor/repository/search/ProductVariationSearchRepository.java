package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.ProductVariation;

/**
 * Spring Data ElasticSearch repository for the ProductVariation entity.
 */
public interface ProductVariationSearchRepository extends ElasticsearchRepository<ProductVariation, Long> {
}
