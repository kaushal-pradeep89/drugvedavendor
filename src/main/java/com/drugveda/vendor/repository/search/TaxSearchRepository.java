package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.Tax;

/**
 * Spring Data ElasticSearch repository for the Tax entity.
 */
public interface TaxSearchRepository extends ElasticsearchRepository<Tax, Long> {
}
