package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.ProductCatalogue;

/**
 * Spring Data ElasticSearch repository for the ProductCatalogue entity.
 */
public interface ProductCatalogueSearchRepository extends ElasticsearchRepository<ProductCatalogue, Long> {
}
