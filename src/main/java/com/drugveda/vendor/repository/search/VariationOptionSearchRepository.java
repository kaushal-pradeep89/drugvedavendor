package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.VariationOption;

/**
 * Spring Data ElasticSearch repository for the VariationOption entity.
 */
public interface VariationOptionSearchRepository extends ElasticsearchRepository<VariationOption, Long> {
}
