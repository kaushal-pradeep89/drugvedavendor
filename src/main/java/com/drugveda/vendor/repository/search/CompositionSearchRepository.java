package com.drugveda.vendor.repository.search;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.drugveda.vendor.domain.Composition;

/**
 * Spring Data ElasticSearch repository for the Composition entity.
 */
public interface CompositionSearchRepository extends ElasticsearchRepository<Composition, Long> {
}
