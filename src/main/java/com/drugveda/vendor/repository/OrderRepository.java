package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.Order;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Order entity.
 */
public interface OrderRepository extends JpaRepository<Order,Long> {

}
