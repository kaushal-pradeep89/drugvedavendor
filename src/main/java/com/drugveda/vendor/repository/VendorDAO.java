package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.Vendor;

/**
 * This interface will deal with vendor functionality
 * 
 * @author pradeep.kaushal
 *
 */
public interface VendorDAO {
	/**
	 * Get the vendor on the userId.
	 * 
	 * @param userId
	 *            the logged in user.
	 * @return the vendor.
	 */
	Vendor getVendorByUserId(Long userId);
}
