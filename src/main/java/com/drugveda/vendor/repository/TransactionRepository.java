package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.Transaction;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Transaction entity.
 */
public interface TransactionRepository extends JpaRepository<Transaction,Long> {

}
