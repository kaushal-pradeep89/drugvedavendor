package com.drugveda.vendor.repository;

import java.util.List;

import com.drugveda.vendor.domain.Cart;

/**
 * This dao will handle the cart related issue.
 * 
 * @author pradeep.kaushal
 *
 */
public interface CartDAO {
	/**
	 * This method will persist the cart
	 * 
	 * @param cart
	 * @return the persisted cart with the id.
	 */
	Cart save(Cart cart);

	/**
	 * This method will update the exising cart.
	 * 
	 * @param cart
	 *            the cart object
	 * @return the cart object with persisted state.
	 */

	Cart update(Cart cart);

	/**
	 * get the cart item for the particular users.
	 * 
	 * @param userId
	 * @return
	 */
	List<Cart> findCartItem(Long userId);

	/**
	 * Delete the cart item.
	 * 
	 * @param cartId
	 *            the primary key of the cart item.
	 * @param userId
	 *            the user id. it's id of the logged in user.
	 */

	void delete(Long cartId, Long userId);
}
