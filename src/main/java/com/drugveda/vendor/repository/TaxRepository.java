package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.Tax;

/**
 * Spring Data JPA repository for the Tax entity.
 */
public interface TaxRepository extends JpaRepository<Tax,Long> {

}
