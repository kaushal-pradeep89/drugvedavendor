package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.Vendor;

/**
 * Spring Data JPA repository for the Vendor entity.
 */
public interface VendorRepository extends JpaRepository<Vendor,Long> {

}
