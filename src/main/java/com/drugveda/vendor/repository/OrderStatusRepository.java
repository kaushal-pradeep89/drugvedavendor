package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.OrderStatus;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the OrderStatus entity.
 */
public interface OrderStatusRepository extends JpaRepository<OrderStatus,Long> {

}
