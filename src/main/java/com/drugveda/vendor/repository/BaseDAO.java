package com.drugveda.vendor.repository;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public abstract class BaseDAO {
	@Inject
	private EntityManager em;

	protected <T> void save(T t) {
		em.persist(t);
	}

	protected <T> T update(T t) {
		t = em.merge(t);
		return t;
	}

	protected <T> T findById(Class<T> cls, Long id) {
		T entity = em.find(cls, id);
		return entity;
	}

	protected <T> void delete(T t) {
		em.remove(t);
	}

	protected EntityManager getEntitytManager() {
		return em;
	}
}
