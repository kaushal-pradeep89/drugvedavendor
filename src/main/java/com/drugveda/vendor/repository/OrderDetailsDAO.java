package com.drugveda.vendor.repository;

import java.util.List;

import com.drugveda.vendor.domain.OrderDetails;

/**
 * This dao will handle the order details related dao.
 * 
 * @author pradeep.kaushal
 *
 */
public interface OrderDetailsDAO {
	List<OrderDetails> findOrderDetailsByOrderId(Long orderId);
}
