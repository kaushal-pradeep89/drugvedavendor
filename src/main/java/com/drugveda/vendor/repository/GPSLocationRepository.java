package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.GPSLocation;

/**
 * Spring Data JPA repository for the GPSLocation entity.
 */
public interface GPSLocationRepository extends JpaRepository<GPSLocation,Long> {

}
