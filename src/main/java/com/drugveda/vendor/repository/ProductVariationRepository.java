package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.ProductVariation;

/**
 * Spring Data JPA repository for the ProductVariation entity.
 */
public interface ProductVariationRepository extends JpaRepository<ProductVariation,Long> {

}
