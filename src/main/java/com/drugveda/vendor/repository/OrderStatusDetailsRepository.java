package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.OrderStatusDetails;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the OrderStatusDetails entity.
 */
public interface OrderStatusDetailsRepository extends JpaRepository<OrderStatusDetails,Long> {

}
