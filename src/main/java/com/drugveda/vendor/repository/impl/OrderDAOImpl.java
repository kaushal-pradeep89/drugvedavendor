package com.drugveda.vendor.repository.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.Order;
import com.drugveda.vendor.repository.OrderDAO;

@Repository
public class OrderDAOImpl implements OrderDAO {
	private final Logger log = LoggerFactory.getLogger(OrderDAOImpl.class);

	@Inject
	private EntityManager entityManager;

	@Override
	public List<Order> getOrders(Long userId) {
		log.info("Entering into getOrder, userId " + userId);
	
		TypedQuery<Order> query = entityManager
				.createQuery("SELECT ordrs FROM Order ordrs WHERE ordrs.vendor.user.id = :userId", Order.class);
		query.setParameter("userId", userId);
		
		
		List<Order> orders = query.getResultList();
		
		return orders;
	}

}
