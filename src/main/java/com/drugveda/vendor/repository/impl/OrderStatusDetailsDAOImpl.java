package com.drugveda.vendor.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.OrderStatusDetails;
import com.drugveda.vendor.repository.OrderStatusDetailsDAO;

@Repository
public class OrderStatusDetailsDAOImpl implements OrderStatusDetailsDAO {

	private final Logger log = LoggerFactory.getLogger(OrderStatusDetailsDAOImpl.class);

	@Inject
	private EntityManager entityManager;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.drugveda.vendor.repository.OrderStatusDetailsDAO#
	 * getOrderStatusDetails(java.lang.Long, java.lang.Long)
	 */
	@Override
	public List<OrderStatusDetails> getOrderStatusDetails(Long orderId, Long statusId) {
		log.debug("Entering into getOrderStatusDetails", orderId, statusId);
		TypedQuery<OrderStatusDetails> query = entityManager.createQuery(
				"SELECT ordrStatusDetails FROM OrderStatusDetails ordrStatusDetails WHERE ordrStatusDetails.orderStatus.id = :statusId AND ordrStatusDetails.order.id = :orderId ORDER BY ordrStatusDetails.createDateTime DESC",
				OrderStatusDetails.class);
		query.setParameter("statusId", statusId);
		query.setParameter("orderId", orderId);
		List<OrderStatusDetails> orderStatusDtails = query.getResultList();
		return orderStatusDtails != null && !orderStatusDtails.isEmpty() ? orderStatusDtails
				: new ArrayList<OrderStatusDetails>();

	}

}
