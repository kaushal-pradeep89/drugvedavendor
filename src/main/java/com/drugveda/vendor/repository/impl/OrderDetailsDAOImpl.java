package com.drugveda.vendor.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.OrderDetails;
import com.drugveda.vendor.repository.OrderDetailsDAO;

/**
 * This class is the implementation of {@link OrderDetailsDAO}
 * 
 * @author pradeep.kaushal
 *
 */
@Repository
public class OrderDetailsDAOImpl implements OrderDetailsDAO {
	private final Logger log = LoggerFactory.getLogger(OrderDetailsDAOImpl.class);
	@Inject
	private EntityManager entityManager;

	@Override
	public List<OrderDetails> findOrderDetailsByOrderId(Long orderId) {
		log.info("Entering into findOrderDetailsByOrderId, orderId " + orderId);
		TypedQuery<OrderDetails> query = entityManager.createQuery(
				"SELECT ordtls FROM OrderDetails ordtls WHERE ordtls.order.id = :orderId", OrderDetails.class);
		query.setParameter("orderId", orderId);
		List<OrderDetails> images = query.getResultList();
		return images != null && !images.isEmpty() ? images : new ArrayList<OrderDetails>();
	}

}
