package com.drugveda.vendor.repository.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.Cart;
import com.drugveda.vendor.repository.CartDAO;

/**
 * This is the implementation for cart dao
 * 
 * @author pradeep.kaushal
 *
 */
@Repository
public class CartDAOImpl implements CartDAO {

	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(CartDAOImpl.class);

	@Inject
	private EntityManager entityManager;

	@Override
	public Cart save(Cart cart) {
		Cart entity = entityManager.merge(cart);
		return entity;
	}

	@Override
	public Cart update(Cart cart) {
		return save(cart);
	}

	@Override
	public List<Cart> findCartItem(Long userId) {
		String sql = "SELECT ct FROM Cart ct WHERE ct.userId=? ";
		TypedQuery<Cart> createQuery = entityManager.createQuery(sql, Cart.class);
		createQuery.setParameter(1, userId);
		List<Cart> carts = createQuery.getResultList();
		return carts;
	}

	@Override
	public void delete(Long cartId, Long userId) {
		// TODO Auto-generated method stub

	}

}
