package com.drugveda.vendor.repository.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.Product;
import com.drugveda.vendor.repository.BaseDAO;
import com.drugveda.vendor.repository.ProductDAO;

/**
 * This is the implementation of {@link ProductDAO}
 * 
 * @author pradeep.kaushal
 *
 */
@Repository
public class ProductDAOImpl extends BaseDAO implements ProductDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.drugveda.vendor.repository.ProductDAO#findProduct(java.lang.String)
	 */
	@Override
	public Collection<Product> findProduct(String searhQuery) {
		List<Product> products = new ArrayList<Product>();

		EntityManager em = getEntitytManager();
		if (searhQuery != null && !searhQuery.isEmpty()) {
			String sql = "SELECT prod FROM Product prod WHERE prod.productName LIKE '" + searhQuery + "%'";
			TypedQuery<Product> query = em.createQuery(sql, Product.class);

			products = query.getResultList();

			String ids = getIds(products);
			sql = "SELECT prod FROM Product prod WHERE prod.productName LIKE '%" + searhQuery
					+ "%' AND prod.id NOT IN (" + ids + ")";

			query = em.createQuery(sql, Product.class);

			List<Product> prods = query.getResultList();
			products.addAll(prods);
		}
		return products;
	}

	@Override
	public Collection<Product> findAll() {
		EntityManager em = getEntitytManager();
		String sql = "SELECT prod Product prod";
		TypedQuery<Product> query = em.createQuery(sql, Product.class);
		List<Product> products = query.getResultList();
		return products;
	}

	private String getIds(List<Product> products) {
		if (products == null)
			return "";
		StringBuffer buffer = new StringBuffer();
		Product product = null;
		
		if (products.size() >= 0) {
			product = products.get(0);
			buffer.append(product.getId());
		}
		
		for (int i = 1; i < products.size(); i++) {
			buffer.append(", ");
			product = products.get(i);
			buffer.append(product.getId());
		}
		return buffer.toString();

	}

}
