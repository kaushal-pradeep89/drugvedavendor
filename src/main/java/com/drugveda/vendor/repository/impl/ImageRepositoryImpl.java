package com.drugveda.vendor.repository.impl;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.ProductImage;
import com.drugveda.vendor.repository.ImageRepository;

@Repository
public class ImageRepositoryImpl implements ImageRepository {

	@Inject
	private EntityManager entityManager;

	@Override
	public List<ProductImage> findImageByProductId(Long productId) {
		TypedQuery<ProductImage> query = entityManager
				.createQuery("SELECT img FROM ProductImage img WHERE img.product.id = :productId", ProductImage.class);
		query.setParameter("productId", productId);
		List<ProductImage> images = query.getResultList();
		return images != null && !images.isEmpty() ? images : new ArrayList<ProductImage>();
	}
}
