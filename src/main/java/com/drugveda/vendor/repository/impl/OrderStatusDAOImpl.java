package com.drugveda.vendor.repository.impl;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.OrderStatus;
import com.drugveda.vendor.domain.OrderStatusDetails;
import com.drugveda.vendor.repository.OrderStatusDAO;

/**
 * This class will handle the order status tables.
 * 
 * @author pradeep.kaushal
 *
 */
@Repository
public class OrderStatusDAOImpl implements OrderStatusDAO {
	private final Logger log = LoggerFactory.getLogger(OrderStatusDAOImpl.class);

	@Inject
	private EntityManager entityManager;

	@Override
	public OrderStatus getOrderStatus(Long orderId, Long userId) {
		log.info("Entering into getOrderStatus, orderId " + orderId);
		TypedQuery<OrderStatus> query = entityManager.createQuery(
				"SELECT ordrStatus FROM OrderStatus ordrStatus WHERE ordrStatus.order.id = :orderId AND ordrStatus.vendor.user.id = :userId",
				OrderStatus.class);
		query.setParameter("orderId", orderId);
		query.setParameter("userId", userId);
		query.setFirstResult(0);
		query.setMaxResults(1);

		List<OrderStatus> orderStatuses = query.getResultList();

		OrderStatus orderStatus = orderStatuses.get(0);

		return orderStatus;
	}

	@Override
	public OrderStatus updateOrderStatus(OrderStatus orderStatus) {
		log.info("Entering into updateOrderStatus, OrderStatus " + orderStatus);
		OrderStatusDetails orderStatusDetails = new OrderStatusDetails();
		
		orderStatusDetails.setOrderStatus(orderStatus);
		orderStatusDetails.setStatus(orderStatus.getStatus().toString());
		orderStatusDetails.setOrder(orderStatus.getOrder());
		orderStatusDetails.setComment(orderStatus.getComment());
		orderStatusDetails.setCreateDateTime(DateTime.now());
		
		entityManager.merge(orderStatus);
		entityManager.persist(orderStatusDetails);

		return orderStatus;
	}

}
