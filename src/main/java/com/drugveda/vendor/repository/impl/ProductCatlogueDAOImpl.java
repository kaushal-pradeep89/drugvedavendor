package com.drugveda.vendor.repository.impl;

import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.ProductCatalogue;
import com.drugveda.vendor.repository.BaseDAO;
import com.drugveda.vendor.repository.ProductCatalogueDAO;

/**
 * This is the implementation of {@link ProductCatalogueDAO}
 * 
 * @author pradeep.kaushal
 *
 */
@Repository
public class ProductCatlogueDAOImpl extends BaseDAO implements ProductCatalogueDAO {
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.drugveda.vendor.repository.ProductCatalogueDAO#
	 * getProductCatalougeById(java.lang.Long)
	 */
	@Override
	public ProductCatalogue getProductCatalougeById(Long id) {

		ProductCatalogue entity = findById(ProductCatalogue.class, id);

		return entity;
	}
}
