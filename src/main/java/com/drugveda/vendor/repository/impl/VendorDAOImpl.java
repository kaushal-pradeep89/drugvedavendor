package com.drugveda.vendor.repository.impl;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.drugveda.vendor.domain.Vendor;
import com.drugveda.vendor.repository.BaseDAO;
import com.drugveda.vendor.repository.VendorDAO;

@Repository
public class VendorDAOImpl extends BaseDAO implements VendorDAO {

	@Override
	public Vendor getVendorByUserId(Long userId) {
		EntityManager em = getEntitytManager();

		String sql = "SELECT ven FROM Vendor ven WHERE ven.user.id = ?";
		TypedQuery<Vendor> query = em.createQuery(sql, Vendor.class);
		query.setParameter(1, userId);
		Vendor vendor = query.getSingleResult();
		return vendor;
	}

}
