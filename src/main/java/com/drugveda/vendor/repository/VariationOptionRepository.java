package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.VariationOption;

/**
 * Spring Data JPA repository for the VariationOption entity.
 */
public interface VariationOptionRepository extends JpaRepository<VariationOption,Long> {

}
