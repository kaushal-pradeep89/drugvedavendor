package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.Cart;
import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Cart entity.
 */
public interface CartRepository extends JpaRepository<Cart,Long> {

}
