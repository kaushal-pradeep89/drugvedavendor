package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.Composition;

/**
 * Spring Data JPA repository for the Composition entity.
 */
public interface CompositionRepository extends JpaRepository<Composition,Long> {

}
