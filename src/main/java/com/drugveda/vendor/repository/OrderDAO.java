package com.drugveda.vendor.repository;

import java.util.List;

import com.drugveda.vendor.domain.Order;

/**
 * This dao will interact with order_tbl
 * 
 * @author pradeep.kaushal
 *
 */
public interface OrderDAO {
	/**
	 * The user id is being to extract the order records on the base of provided
	 * user.
	 * 
	 * @param userId
	 * @return the orders.
	 */
	List<Order> getOrders(Long userId);
}
