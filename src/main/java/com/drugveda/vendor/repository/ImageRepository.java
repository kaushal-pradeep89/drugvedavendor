package com.drugveda.vendor.repository;

import java.util.List;

import com.drugveda.vendor.domain.ProductImage;

/**
 * Image repository to handle the images related issue.
 * @author pradeep.kaushal
 *
 */
public interface ImageRepository {
List<ProductImage> findImageByProductId(Long productId);
}
