package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.ProductOption;

/**
 * Spring Data JPA repository for the ProductOption entity.
 */
public interface ProductOptionRepository extends JpaRepository<ProductOption,Long> {

}
