package com.drugveda.vendor.repository;

import java.util.Collection;

import com.drugveda.vendor.domain.Product;

/**
 * This interface will handle the product.
 * 
 * @author pradeep.kaushal
 *
 */
public interface ProductDAO {
	/**
	 * Find the product on the base of search query.
	 * 
	 * @param searhQuery
	 * @return
	 */
	Collection<Product> findProduct(String searhQuery);

	/**
	 * Find all the product when there is no query parameter.
	 * 
	 * @return
	 */
	Collection<Product> findAll();
}
