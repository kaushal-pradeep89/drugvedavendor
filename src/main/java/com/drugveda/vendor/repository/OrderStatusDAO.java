package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.OrderStatus;

/**
 * This will handle order status changes.
 * 
 * @author pradeep.kaushal
 *
 */
public interface OrderStatusDAO {
	/**
	 * Get the order status .
	 * 
	 * @param orderId
	 *            the order id.
	 * @param vendorId
	 *            the vendor id
	 * @return the order status.
	 */
	OrderStatus getOrderStatus(Long orderId, Long vendorId);

	/**
	 * update the order status along with order status details.
	 * 
	 * @param orderStatus
	 * @return the order status.
	 */
	OrderStatus updateOrderStatus(OrderStatus orderStatus);
}
