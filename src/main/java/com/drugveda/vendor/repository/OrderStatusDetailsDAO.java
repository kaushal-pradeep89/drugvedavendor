package com.drugveda.vendor.repository;

import java.util.List;

import com.drugveda.vendor.domain.OrderStatusDetails;

/**
 * This is the interface which handles the interaction with order details.
 * 
 * @author pradeep.kaushal
 *
 */
public interface OrderStatusDetailsDAO {
	/**
	 * Get the order status details related to particular order. An order can be
	 * splitted in many sub order. It will return the order status details of
	 * sub order. It will chronical entry for an order. It will hold all the
	 * information about the order status changes.
	 * 
	 * @param orderId
	 *            the order id.
	 * @param statusId
	 *            the status id.
	 * @return the order status details.
	 */
	List<OrderStatusDetails> getOrderStatusDetails(Long orderId, Long statusId);
}
