package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.ProductImage;

/**
 * Spring Data JPA repository for the ProductImage entity.
 */
public interface ProductImageRepository extends JpaRepository<ProductImage,Long> {

}
