package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.ProductCatalogue;

/**
 * Spring Data JPA repository for the ProductCatalogue entity.
 */
public interface ProductCatalogueRepository extends JpaRepository<ProductCatalogue,Long> {

}
