package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.SKU;

/**
 * Spring Data JPA repository for the SKU entity.
 */
public interface SKURepository extends JpaRepository<SKU,Long> {

}
