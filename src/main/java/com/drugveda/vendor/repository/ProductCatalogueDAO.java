package com.drugveda.vendor.repository;

import com.drugveda.vendor.domain.ProductCatalogue;

/**
 * This interface will handle with product catalogue.
 * 
 * @author pradeep.kaushal
 *
 */
public interface ProductCatalogueDAO {
	/**
	 * This method will return the product catalogue by its id.
	 * 
	 * @param id
	 *            the product catalogue id.
	 * @return the product catalogue.
	 */
	ProductCatalogue getProductCatalougeById(Long id);
}
