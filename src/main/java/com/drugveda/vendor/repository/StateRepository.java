package com.drugveda.vendor.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.drugveda.vendor.domain.State;

/**
 * Spring Data JPA repository for the State entity.
 */
public interface StateRepository extends JpaRepository<State,Long> {

}
