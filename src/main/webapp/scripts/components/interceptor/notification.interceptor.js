 'use strict';

angular.module('drugvedavendorApp')
    .factory('notificationInterceptor', function ($q, AlertService) {
        return {
            response: function(response) {
                var alertKey = response.headers('X-drugvedavendorApp-alert');
                if (angular.isString(alertKey)) {
                    AlertService.success(alertKey, { param : response.headers('X-drugvedavendorApp-params')});
                }
                return response;
            },
        };
    });