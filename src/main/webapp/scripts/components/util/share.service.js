'use strict';

angular.module('drugvedavendorApp')
    .service('ShareService', function () {
        var data ={};

        return {
            getProperty: function () {
                return data;
            },
            setProperty: function(value) {
            	data = value;
            }
        };
    });