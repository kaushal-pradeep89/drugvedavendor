/* globals $ */
'use strict';

angular.module('drugvedavendorApp')
    .directive('drugvedavendorAppPager', function() {
        return {
            templateUrl: 'scripts/components/form/pager.html'
        };
    });
