/* globals $ */
'use strict';

angular.module('drugvedavendorApp')
    .directive('drugvedavendorAppPagination', function() {
        return {
            templateUrl: 'scripts/components/form/pagination.html'
        };
    });
