'use strict';

angular.module('drugvedavendorApp')
    .factory('SKUSearch', function ($resource) {
        return $resource('api/_search/sKUs/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
