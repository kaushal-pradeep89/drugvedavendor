'use strict';

angular.module('drugvedavendorApp')
    .factory('Images', function($http) {
       this.saveProductImages = function( pathParam, productImage){
    	  $http.post("api/productImage/"+pathParam,productImage).then(success,failure);
       };
       this.getProductImages = function(productId){
    	   var data=$http.get("api/images/"+productId).then(success,failure);
    	   return data;
       }
       function success(response){
    	   return response.data;
       };
       function failure(xhr){
    	   return xhr;
       }
       return this;
    });