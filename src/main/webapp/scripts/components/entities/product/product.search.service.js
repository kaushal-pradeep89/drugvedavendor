'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductSearch', function($http) {
       this.getProducts = function( query){
    	  var data=$http.get("api/_search/products/"+query).then(success,failure);
    	  console.log("product")
    	  console.log(data);
    	  return data;
       };
     
       function success(response){
    	   console.log("response");
    	   console.log(response)
    	   return response.data;
       };
       function failure(xhr){
    	   return xhr;
       }
       return this;
});
