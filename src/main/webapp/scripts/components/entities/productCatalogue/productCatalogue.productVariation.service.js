'use strict';

angular.module('drugvedavendorApp')
    .factory('CatalogueProductVariation', function($http) {
       this.save = function( pathParam, productVariation){
    	  $http.post("api/prodVar/"+pathParam,productVariation).then(success,success);
       };
       function success(response){
    	   console.log(response);
    	   return response.data;
       };
       function failure(xhr){
    	   console.log("error");
    	   return xhr;
       }
       return this;
    });