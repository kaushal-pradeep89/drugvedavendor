'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductCatalogue', function ($resource, DateUtils) {
        return $resource('api/productCatalogues/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
