'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductCatalogueSearch', function ($resource) {
        return $resource('api/_search/productCatalogues/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
