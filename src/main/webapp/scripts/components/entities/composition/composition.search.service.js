'use strict';

angular.module('drugvedavendorApp')
    .factory('CompositionSearch', function ($resource) {
        return $resource('api/_search/compositions/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
