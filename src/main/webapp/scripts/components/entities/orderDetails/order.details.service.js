'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderDetailsService', function($http) {
       this.getOrderDetailsByOrderId = function( orderId){
    	  var data=$http.get("api/orderDetails/"+orderId+"/orderId").then(success,failure);
    	  return data;
       };
     
       function success(response){
    	   return response.data;
       };
       function failure(xhr){
    	   return xhr;
       }
       return this;
    });