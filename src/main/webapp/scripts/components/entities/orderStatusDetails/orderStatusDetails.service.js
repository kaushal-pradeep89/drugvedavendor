'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderStatusDetails', function ($resource, DateUtils) {
        return $resource('api/orderStatusDetailss/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createDateTime = DateUtils.convertDateTimeFromServer(data.createDateTime);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
