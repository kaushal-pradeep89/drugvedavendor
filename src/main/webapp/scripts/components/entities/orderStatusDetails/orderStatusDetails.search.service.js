'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderStatusDetailsSearch', function ($resource) {
        return $resource('api/_search/orderStatusDetailss/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
