'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderStatusDetailsService', function($http) {
       this.getOrderStatusDetails = function( orderId,statusId){
    	  var data=$http.get("api/orderStatusDetailss/"+orderId+"/"+statusId).then(success,failure);
    	  return data;
       };
     
       function success(response){
    	   return response.data;
       };
       function failure(xhr){
    	   return xhr;
       }
       return this;
    });