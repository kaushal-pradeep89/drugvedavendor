'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductVariationSearch', function ($resource) {
        return $resource('api/_search/productVariations/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
