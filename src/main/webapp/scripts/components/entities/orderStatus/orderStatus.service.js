'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderStatus', function ($resource, DateUtils) {
        return $resource('api/orderStatuss/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.eta = DateUtils.convertDateTimeFromServer(data.eta);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
