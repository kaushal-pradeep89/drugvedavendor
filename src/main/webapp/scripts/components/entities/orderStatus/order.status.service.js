'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderStatusService', function($http) {
       this.getOrderStatusByOrderId = function( orderId){
    	  var data=$http.get("api/orderStatuss/"+orderId+"/orderId").then(success,failure);
    	  return data;
       };
     
       function success(response){
    	   return response.data;
       };
       function failure(xhr){
    	   return xhr;
       }
       return this;
    });