'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderStatusSearch', function ($resource) {
        return $resource('api/_search/orderStatuss/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
