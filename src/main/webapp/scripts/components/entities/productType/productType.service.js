'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductType', function ($resource, DateUtils) {
        return $resource('api/productTypes/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.cratedDate = DateUtils.convertDateTimeFromServer(data.cratedDate);
                    data.updatedDate = DateUtils.convertDateTimeFromServer(data.updatedDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
