'use strict';

angular.module('drugvedavendorApp')
    .factory('StateSearch', function ($resource) {
        return $resource('api/_search/states/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
