'use strict';

angular.module('drugvedavendorApp')
    .factory('OrderSearch', function ($resource) {
        return $resource('api/_search/orders/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
