'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductCategory', function ($resource, DateUtils) {
        return $resource('api/productCategorys/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.cratedDate = DateUtils.convertDateTimeFromServer(data.cratedDate);
                    data.updatedDate = DateUtils.convertDateTimeFromServer(data.updatedDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
