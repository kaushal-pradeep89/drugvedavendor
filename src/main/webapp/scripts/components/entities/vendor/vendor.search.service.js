'use strict';

angular.module('drugvedavendorApp')
    .factory('VendorSearch', function ($resource) {
        return $resource('api/_search/vendors/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
