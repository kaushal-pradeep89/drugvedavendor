'use strict';

angular.module('drugvedavendorApp')
    .factory('Transaction', function ($resource, DateUtils) {
        return $resource('api/transactions/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createdDateTime = DateUtils.convertDateTimeFromServer(data.createdDateTime);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
