'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductOptionSearch', function ($resource) {
        return $resource('api/_search/productOptions/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
