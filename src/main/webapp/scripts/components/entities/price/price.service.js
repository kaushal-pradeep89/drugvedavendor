'use strict';

angular.module('drugvedavendorApp')
    .factory('Price', function ($resource, DateUtils) {
        return $resource('api/prices/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.createDate = DateUtils.convertDateTimeFromServer(data.createDate);
                    data.updateDate = DateUtils.convertDateTimeFromServer(data.updateDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
