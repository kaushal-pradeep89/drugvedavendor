'use strict';

angular.module('drugvedavendorApp')
    .factory('Cart', function ($resource, DateUtils) {
        return $resource('api/carts/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.addedTime = DateUtils.convertDateTimeFromServer(data.addedTime);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
