'use strict';

angular.module('drugvedavendorApp')
    .factory('CartSearch', function ($resource) {
        return $resource('api/_search/carts/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
