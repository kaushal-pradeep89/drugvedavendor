'use strict';

angular.module('drugvedavendorApp')
    .factory('GPSLocationSearch', function ($resource) {
        return $resource('api/_search/gPSLocations/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
