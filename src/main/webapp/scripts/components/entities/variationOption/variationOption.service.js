'use strict';

angular.module('drugvedavendorApp')
    .factory('VariationOption', function ($resource, DateUtils) {
        return $resource('api/variationOptions/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
