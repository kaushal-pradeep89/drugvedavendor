'use strict';

angular.module('drugvedavendorApp')
    .factory('VariationOptionSearch', function ($resource) {
        return $resource('api/_search/variationOptions/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
