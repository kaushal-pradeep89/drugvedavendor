'use strict';

angular.module('drugvedavendorApp')
    .factory('ProductImageSearch', function ($resource) {
        return $resource('api/_search/productImages/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
