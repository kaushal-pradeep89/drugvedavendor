'use strict';

angular.module('drugvedavendorApp')
    .factory('BrandSearch', function ($resource) {
        return $resource('api/_search/brands/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
