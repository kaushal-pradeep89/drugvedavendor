'use strict';

angular.module('drugvedavendorApp')
    .factory('Brand', function ($resource, DateUtils) {
        return $resource('api/brands/:id', {}, {
            'query': { method: 'GET', isArray: true},
            'get': {
                method: 'GET',
                transformResponse: function (data) {
                    data = angular.fromJson(data);
                    data.cratedDate = DateUtils.convertDateTimeFromServer(data.cratedDate);
                    data.updatedDate = DateUtils.convertDateTimeFromServer(data.updatedDate);
                    return data;
                }
            },
            'update': { method:'PUT' }
        });
    });
