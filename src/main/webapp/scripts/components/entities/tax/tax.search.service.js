'use strict';

angular.module('drugvedavendorApp')
    .factory('TaxSearch', function ($resource) {
        return $resource('api/_search/taxs/:query', {}, {
            'query': { method: 'GET', isArray: true}
        });
    });
