'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductDetailController', function ($scope, $rootScope, $stateParams, entity, Product, Brand, Composition, SKU, ProductType, ProductCategory, ProductImage, ProductOption) {
        $scope.product = entity;
        $scope.load = function (id) {
            Product.get({id: id}, function(result) {
                $scope.product = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:productUpdate', function(event, result) {
            $scope.product = result;
        });
    });
