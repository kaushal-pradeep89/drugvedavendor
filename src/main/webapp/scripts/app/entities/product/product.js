'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('product', {
                parent: 'entity',
                url: '/products',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Products'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/product/products.html',
                        controller: 'ProductController'
                    }
                },
                resolve: {
                }
            })
            .state('product.detail', {
                parent: 'entity',
                url: '/product/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Product'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/product/product-detail.html',
                        controller: 'ProductDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Product', function($stateParams, Product) {
                        return Product.get({id : $stateParams.id});
                    }]
                }
            })
            /*.state('product.new', {
                parent: 'product',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/product/product-dialog.html',
                        controller: 'ProductDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {productName: null, createDate: null, updateDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('product', null, { reload: true });
                    }, function() {
                        $state.go('product');
                    })
                }]
            })*/
            .state('product.new', {
                parent: 'product',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/product/product-dialog-frg.html',
                         controller: 'ProductWizardController',
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Product', function($stateParams, Product) {
                        return {};
                    }]
                }
            })
            .state('product.new.image', {
                parent: 'product.new',
                url : '/product.new.image/{id}',
                data: {
                    roles: ['ROLE_USER'],
                },
               
	        	 templateUrl: 'scripts/app/entities/product/productImage-dialog.html',
	        	 controller: 'ProductImgWizardController', 	 
	        	 resolve: {
	        		 entity: ['$stateParams','Images', function($stateParams, Images) {
	        			 console.log("Product Image");
	        			 var ent=Images.getProductImages($stateParams.id);
	        			 console.log('in state');
	        			 console.log(ent);
	                     return ent;
	                 }]
	             }
               
                
            })
            .state('product.edit', {
                parent: 'product',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/product/product-dialog-frg.html',
                         controller: 'ProductWizardController',
                    }
                },
                resolve: {
                	entity: ['$stateParams','Product', function($stateParams,Product) {
                        return Product.get({id : $stateParams.id});
                    }]
                }
            });
    });
