'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductImgWizardController', function ($scope, $rootScope, $stateParams, entity, Upload, UPLOAD_URL,IMAGE_BASE) {
    	 $scope.productimgs = entity;
    	 $scope.imagebase=IMAGE_BASE;
    	 $scope.product=getProduct(entity);
    	 
         var onSaveFinished = function (result) {
             $scope.product=result;
         };
         
         
         $scope.startUpload = function() {
             if ($scope.file && !$scope.file.$error) {
               $scope.upload($scope.file);
             }
             else
             	console.log($scope.file);
          };

          
          $scope.onSelect = function ($item, $model, $label) {
         	    $scope.$model = $model;        	   
         	};

 		//upload on file select or drop
 		$scope.upload = function (file) {
 			Upload.upload({
 		        url: UPLOAD_URL+"/"+$stateParams.id,
 		        file: file
 		    }).progress(function (evt) {
 		        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
 		        console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
 		    }).success(function (data, status, headers, config) {
 		        console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
 		        if(data.status == "ok"){
 		        	console.log("image saved");
 		        	 $scope.businessImage.folderNumber = parseInt(data.folderNumber);
 		        	 $scope.businessImage.path = data.imagePath;
 		        	 $scope.file = BUSINESS_IMAGES_URL+$scope.businessImage.path;
 		        }
 		    }).error(function (data, status, headers, config) {
 		        console.log('error status: ' + status);
 		    });
 		};
 		
 		function getProduct(productImgs){
 			if(productImgs == undefined ||productImgs[0].product == undefined)
 				return;
 			var product=productImgs[0].product;
 			return product;
 		}

    });
