'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductWizardController', function ($scope, $rootScope, $stateParams, entity,Product, Brand, Composition, SKU, ProductType, ProductCategory, ProductImage, ProductOption, Upload, UPLOAD_URL) {
    	 $scope.product = entity;
         $scope.brands = Brand.query();
         $scope.compositions = Composition.query();
         $scope.skus = SKU.query();
         $scope.producttypes = ProductType.query();
         $scope.productcategorys = ProductCategory.query();
         $scope.productoptions = ProductOption.query();
        
         $scope.load = function(id) {
             Product.get({id : id}, function(result) {
                 $scope.product = result;
             });
         };

         var onSaveFinished = function (result) {
             $scope.product=result;
         };
         
         

         $scope.save = function () {
         	console.log("in save");
             if ($scope.product.id != null) {
                 Product.update($scope.product, onSaveFinished);
             } else {
                 Product.save($scope.product, onSaveFinished);
             }
         };
    });
