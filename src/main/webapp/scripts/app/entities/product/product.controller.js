'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductController', function ($scope, Product, ProductSearch,$timeout) {
        $scope.products = [];

        $scope.delete = function (id) {
            Product.get({id: id}, function(result) {
                $scope.product = result;
                $('#deleteProductConfirmation').modal('show');
            });
        };
        
       // Delay the execution for search
        var query = '';
        var filterTextTimeout;
        $scope.$watch('searchQuery', function (val) {
            if (filterTextTimeout) $timeout.cancel(filterTextTimeout);

            query = val;
            filterTextTimeout = $timeout(function() {
            	if(query!=undefined && query.trim().length >= 2){
            		var data=ProductSearch.getProducts(query.trim()).then(function(data){
            			  $scope.products = data;
            		});              
            	}
            }, 1000); // delay 1 sec
        })

        $scope.confirmDelete = function (id) {
            Product.delete({id: id},
                function () {
                    $('#deleteProductConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ProductSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.products = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
        	$scope.products = [];
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.product = {productName: null, createDate: null, updateDate: null, id: null};
        };
    });
