'use strict';

angular.module('drugvedavendorApp').controller('ProductDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Product', 'Brand', 'Composition', 'SKU', 'ProductType', 'ProductCategory', 'ProductImage', 'ProductOption',
        function($scope, $stateParams, $modalInstance, entity, Product, Brand, Composition, SKU, ProductType, ProductCategory, ProductImage, ProductOption) {

        $scope.product = entity;
        $scope.brands = Brand.query();
        $scope.compositions = Composition.query();
        $scope.skus = SKU.query();
        $scope.producttypes = ProductType.query();
        $scope.productcategorys = ProductCategory.query();
        $scope.productimages = ProductImage.query();
        $scope.productoptions = ProductOption.query();
        $scope.load = function(id) {
            Product.get({id : id}, function(result) {
                $scope.product = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:productUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
        	console.log("in save");
            if ($scope.product.id != null) {
                Product.update($scope.product, onSaveFinished);
            } else {
                Product.save($scope.product, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
