'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductVariationController', function ($scope, ProductVariation, ProductVariationSearch) {
        $scope.productVariations = [];
        $scope.loadAll = function() {
            ProductVariation.query(function(result) {
               $scope.productVariations = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ProductVariation.get({id: id}, function(result) {
                $scope.productVariation = result;
                $('#deleteProductVariationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ProductVariation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteProductVariationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ProductVariationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.productVariations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.productVariation = {measureType: null, id: null};
        };
    });
