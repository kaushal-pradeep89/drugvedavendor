'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductVariationDetailController', function ($scope, $rootScope, $stateParams, entity, ProductVariation, VariationOption, ProductCatalogue) {
        $scope.productVariation = entity;
        $scope.load = function (id) {
            ProductVariation.get({id: id}, function(result) {
                $scope.productVariation = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:productVariationUpdate', function(event, result) {
            $scope.productVariation = result;
        });
    });
