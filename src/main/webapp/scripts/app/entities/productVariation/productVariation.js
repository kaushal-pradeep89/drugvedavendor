'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('productVariation', {
                parent: 'entity',
                url: '/productVariations',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductVariations'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productVariation/productVariations.html',
                        controller: 'ProductVariationController'
                    }
                },
                resolve: {
                }
            })
            .state('productVariation.detail', {
                parent: 'entity',
                url: '/productVariation/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductVariation'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productVariation/productVariation-detail.html',
                        controller: 'ProductVariationDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ProductVariation', function($stateParams, ProductVariation) {
                        return ProductVariation.get({id : $stateParams.id});
                    }]
                }
            })
            .state('productVariation.new', {
                parent: 'productVariation',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productVariation/productVariation-dialog.html',
                        controller: 'ProductVariationDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {measureType: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('productVariation', null, { reload: true });
                    }, function() {
                        $state.go('productVariation');
                    })
                }]
            })
            .state('productVariation.edit', {
                parent: 'productVariation',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productVariation/productVariation-dialog.html',
                        controller: 'ProductVariationDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ProductVariation', function(ProductVariation) {
                                return ProductVariation.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('productVariation', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
