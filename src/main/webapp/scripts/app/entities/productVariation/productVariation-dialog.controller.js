'use strict';

angular.module('drugvedavendorApp').controller('ProductVariationDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ProductVariation', 'VariationOption', 'ProductCatalogue',
        function($scope, $stateParams, $modalInstance, entity, ProductVariation, VariationOption, ProductCatalogue) {

        $scope.productVariation = entity;
        $scope.variationoptions = VariationOption.query();
        $scope.productcatalogues = ProductCatalogue.query();
        $scope.load = function(id) {
            ProductVariation.get({id : id}, function(result) {
                $scope.productVariation = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:productVariationUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.productVariation.id != null) {
                ProductVariation.update($scope.productVariation, onSaveFinished);
            } else {
                ProductVariation.save($scope.productVariation, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
