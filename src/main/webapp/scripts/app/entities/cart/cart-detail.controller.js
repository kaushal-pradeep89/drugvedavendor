'use strict';

angular.module('drugvedavendorApp')
    .controller('CartDetailController', function ($scope, $rootScope, $stateParams, entity, Cart) {
        $scope.cart = entity;
        $scope.load = function (id) {
            Cart.get({id: id}, function(result) {
                $scope.cart = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:cartUpdate', function(event, result) {
            $scope.cart = result;
        });
    });
