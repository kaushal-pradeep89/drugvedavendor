'use strict';

angular.module('drugvedavendorApp')
    .controller('CartController', function ($scope, Cart, CartSearch) {
        $scope.carts = [];
        $scope.loadAll = function() {
            Cart.query(function(result) {
               $scope.carts = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Cart.get({id: id}, function(result) {
                $scope.cart = result;
                $('#deleteCartConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Cart.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCartConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CartSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.carts = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.cart = {quantity: null, productCatalougeId: null, userId: null, addedTime: null, token: null, id: null};
        };
    });
