'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('cart', {
                parent: 'entity',
                url: '/carts',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Carts'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cart/carts.html',
                        controller: 'CartController'
                    }
                },
                resolve: {
                }
            })
            .state('cart.detail', {
                parent: 'entity',
                url: '/cart/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Cart'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/cart/cart-detail.html',
                        controller: 'CartDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Cart', function($stateParams, Cart) {
                        return Cart.get({id : $stateParams.id});
                    }]
                }
            })
            .state('cart.new', {
                parent: 'cart',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cart/cart-dialog.html',
                        controller: 'CartDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {quantity: null, productCatalougeId: null, userId: null, addedTime: null, token: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('cart', null, { reload: true });
                    }, function() {
                        $state.go('cart');
                    })
                }]
            })
            .state('cart.edit', {
                parent: 'cart',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/cart/cart-dialog.html',
                        controller: 'CartDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Cart', function(Cart) {
                                return Cart.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('cart', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
