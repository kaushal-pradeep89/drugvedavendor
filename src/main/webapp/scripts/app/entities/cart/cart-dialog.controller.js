'use strict';

angular.module('drugvedavendorApp').controller('CartDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Cart',
        function($scope, $stateParams, $modalInstance, entity, Cart) {

        $scope.cart = entity;
        $scope.load = function(id) {
            Cart.get({id : id}, function(result) {
                $scope.cart = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:cartUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.cart.id != null) {
                Cart.update($scope.cart, onSaveFinished);
            } else {
                Cart.save($scope.cart, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
