'use strict';

angular.module('drugvedavendorApp').controller('TaxDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Tax', 'ProductCatalogue',
        function($scope, $stateParams, $modalInstance, entity, Tax, ProductCatalogue) {

        $scope.tax = entity;
        $scope.productcatalogues = ProductCatalogue.query();
        $scope.load = function(id) {
            Tax.get({id : id}, function(result) {
                $scope.tax = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:taxUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.tax.id != null) {
                Tax.update($scope.tax, onSaveFinished);
            } else {
                Tax.save($scope.tax, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
