'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('tax', {
                parent: 'entity',
                url: '/taxs',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Taxs'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tax/taxs.html',
                        controller: 'TaxController'
                    }
                },
                resolve: {
                }
            })
            .state('tax.detail', {
                parent: 'entity',
                url: '/tax/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Tax'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/tax/tax-detail.html',
                        controller: 'TaxDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Tax', function($stateParams, Tax) {
                        return Tax.get({id : $stateParams.id});
                    }]
                }
            })
            .state('tax.new', {
                parent: 'tax',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/tax/tax-dialog.html',
                        controller: 'TaxDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {name: null, tax: null, colExt1: null, colExt2: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('tax', null, { reload: true });
                    }, function() {
                        $state.go('tax');
                    })
                }]
            })
            .state('tax.edit', {
                parent: 'tax',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/tax/tax-dialog.html',
                        controller: 'TaxDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Tax', function(Tax) {
                                return Tax.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('tax', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
