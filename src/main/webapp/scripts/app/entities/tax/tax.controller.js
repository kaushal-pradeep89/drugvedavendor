'use strict';

angular.module('drugvedavendorApp')
    .controller('TaxController', function ($scope, Tax, TaxSearch) {
        $scope.taxs = [];
        $scope.loadAll = function() {
            Tax.query(function(result) {
               $scope.taxs = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Tax.get({id: id}, function(result) {
                $scope.tax = result;
                $('#deleteTaxConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Tax.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTaxConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TaxSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.taxs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.tax = {name: null, tax: null, colExt1: null, colExt2: null, id: null};
        };
    });
