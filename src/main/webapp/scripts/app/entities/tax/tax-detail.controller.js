'use strict';

angular.module('drugvedavendorApp')
    .controller('TaxDetailController', function ($scope, $rootScope, $stateParams, entity, Tax, ProductCatalogue) {
        $scope.tax = entity;
        $scope.load = function (id) {
            Tax.get({id: id}, function(result) {
                $scope.tax = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:taxUpdate', function(event, result) {
            $scope.tax = result;
        });
    });
