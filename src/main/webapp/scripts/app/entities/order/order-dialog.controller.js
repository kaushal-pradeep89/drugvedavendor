'use strict';

angular.module('drugvedavendorApp').controller('OrderDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Order', 'Vendor', 'Transaction', 'OrderDetails', 'OrderStatus', 'OrderStatusDetails',
        function($scope, $stateParams, $modalInstance, entity, Order, Vendor, Transaction, OrderDetails, OrderStatus, OrderStatusDetails) {

        $scope.order = entity;
        $scope.vendors = Vendor.query();
        $scope.transactions = Transaction.query({filter: 'order-is-null'});
        $scope.orderdetailss = OrderDetails.query();
        $scope.orderstatuss = OrderStatus.query();
        $scope.orderstatusdetailss = OrderStatusDetails.query();
        $scope.load = function(id) {
            Order.get({id : id}, function(result) {
                $scope.order = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:orderUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.order.id != null) {
                Order.update($scope.order, onSaveFinished);
            } else {
                Order.save($scope.order, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
