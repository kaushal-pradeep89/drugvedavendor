'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderDetailController', function ($scope, $rootScope, $stateParams, entity, Order, Vendor, Transaction, OrderDetails, OrderStatus, OrderStatusDetails) {
        $scope.order = entity;
        $scope.load = function (id) {
            Order.get({id: id}, function(result) {
                $scope.order = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:orderUpdate', function(event, result) {
            $scope.order = result;
        });
    });
