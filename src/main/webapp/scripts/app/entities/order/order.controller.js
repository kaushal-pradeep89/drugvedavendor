'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderController', function ($scope, Order, OrderSearch) {
        $scope.orders = [];
        $scope.loadAll = function() {
            Order.query(function(result) {
               $scope.orders = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Order.get({id: id}, function(result) {
                $scope.order = result;
                $('#deleteOrderConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Order.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteOrderConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            OrderSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.orders = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.order = {created: null, totalAmount: null, discount: null, discountName: null, productUnits: null, taxName: null, taxRate: null, id: null};
        };
    });
