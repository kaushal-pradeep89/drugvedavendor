'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('order', {
                parent: 'entity',
                url: '/orders',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Orders'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/order/orders.html',
                        controller: 'OrderController'
                    }
                },
                resolve: {
                }
            })
            .state('order.detail', {
                parent: 'entity',
                url: '/order/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Order'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/order/order-detail.html',
                        controller: 'OrderDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Order', function($stateParams, Order) {
                        return Order.get({id : $stateParams.id});
                    }]
                }
            })
            .state('order.new', {
                parent: 'order',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/order/order-dialog.html',
                        controller: 'OrderDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {created: null, totalAmount: null, discount: null, discountName: null, productUnits: null, taxName: null, taxRate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('order', null, { reload: true });
                    }, function() {
                        $state.go('order');
                    })
                }]
            })
            .state('order.edit', {
                parent: 'order',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/order/order-dialog.html',
                        controller: 'OrderDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Order', function(Order) {
                                return Order.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('order', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
            .state('orderStatus.status', {
                parent: 'entity',
                url: '/orderStatus/{orderId}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'OrderStatuss'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderStatus/orderStatuss.html',
                        controller: 'OrderStatusController'
                    }
                },
                resolve: {
                	entity:['$stateParams', 'OrderStatusService', function($stateParams, OrderStatusService) {
                        return OrderStatusService.getOrderStatusByOrderId($stateParams.orderId);
                    }]
                }
            });
    });
