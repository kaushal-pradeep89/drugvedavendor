'use strict';

angular.module('drugvedavendorApp').controller('VendorDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Vendor', 'SKU', 'Address',
        function($scope, $stateParams, $modalInstance, entity, Vendor, SKU, Address) {

        $scope.vendor = entity;
        $scope.skus = SKU.query();
        $scope.addresss = Address.query({filter: 'vendor-is-null'});
        $scope.load = function(id) {
            Vendor.get({id : id}, function(result) {
                $scope.vendor = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:vendorUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.vendor.id != null) {
                Vendor.update($scope.vendor, onSaveFinished);
            } else {
                Vendor.save($scope.vendor, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
