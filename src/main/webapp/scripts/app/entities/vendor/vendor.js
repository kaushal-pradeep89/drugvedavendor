'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('vendor', {
                parent: 'entity',
                url: '/vendors',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Vendors'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vendor/vendors.html',
                        controller: 'VendorController'
                    }
                },
                resolve: {
                }
            })
            .state('vendor.detail', {
                parent: 'entity',
                url: '/vendor/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Vendor'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/vendor/vendor-detail.html',
                        controller: 'VendorDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Vendor', function($stateParams, Vendor) {
                        return Vendor.get({id : $stateParams.id});
                    }]
                }
            })
            .state('vendor.new', {
                parent: 'vendor',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/vendor/vendor-dialog.html',
                        controller: 'VendorDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {vendorName: null, phoneNo: null, mobibleNo: null, cratedDate: null, updatedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('vendor', null, { reload: true });
                    }, function() {
                        $state.go('vendor');
                    })
                }]
            })
            .state('vendor.edit', {
                parent: 'vendor',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/vendor/vendor-dialog.html',
                        controller: 'VendorDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Vendor', function(Vendor) {
                                return Vendor.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('vendor', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
