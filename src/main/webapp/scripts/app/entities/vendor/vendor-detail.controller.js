'use strict';

angular.module('drugvedavendorApp')
    .controller('VendorDetailController', function ($scope, $rootScope, $stateParams, entity, Vendor, SKU, Address) {
        $scope.vendor = entity;
        $scope.load = function (id) {
            Vendor.get({id: id}, function(result) {
                $scope.vendor = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:vendorUpdate', function(event, result) {
            $scope.vendor = result;
        });
    });
