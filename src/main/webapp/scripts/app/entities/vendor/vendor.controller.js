'use strict';

angular.module('drugvedavendorApp')
    .controller('VendorController', function ($scope, Vendor, VendorSearch) {
        $scope.vendors = [];
        $scope.loadAll = function() {
            Vendor.query(function(result) {
               $scope.vendors = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Vendor.get({id: id}, function(result) {
                $scope.vendor = result;
                $('#deleteVendorConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Vendor.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteVendorConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            VendorSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.vendors = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.vendor = {vendorName: null, phoneNo: null, mobibleNo: null, cratedDate: null, updatedDate: null, id: null};
        };
    });
