'use strict';

angular.module('drugvedavendorApp').controller('OrderStatusDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'OrderStatus', 'Order', 'OrderStatusDetails',
        function($scope, $stateParams, $modalInstance, entity, OrderStatus, Order, OrderStatusDetails) {

        $scope.orderStatus = entity;
        $scope.load = function(id) {
            OrderStatus.get({id : id}, function(result) {
                $scope.orderStatus = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:orderStatusUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.orderStatus.id != null) {
                OrderStatus.update($scope.orderStatus, onSaveFinished);
            } else {
                OrderStatus.save($scope.orderStatus, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
