'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('orderStatus', {
                parent: 'entity',
                url: '/orderStatuss/',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'OrderStatuss'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderStatus/orderStatuss.html',
                        controller: 'OrderStatusController'
                    }
                },
                resolve: {
                }
            })
            .state('orderStatus.detail', {
                parent: 'entity',
                url: '/orderStatus/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'OrderStatus'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderStatus/orderStatus-detail.html',
                        controller: 'OrderStatusDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'OrderStatusService', function($stateParams, OrderStatusService) {
                        return OrderStatusService.getOrderStatusByOrderId($stateParams.id);
                    }]
                }
            })
            .state('orderStatus.new', {
                parent: 'orderStatus',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/orderStatus/orderStatus-dialog.html',
                        controller: 'OrderStatusDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {eta: null, units: null, status: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('orderStatus', null, { reload: true });
                    }, function() {
                        $state.go('orderStatus');
                    })
                }]
            })

            .state('orderStatus.edit', {
                parent: 'orderStatus',
                url: '{orderId}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/orderStatus/orderStatus-dialog.html',
                        controller: 'OrderStatusDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['OrderStatusService', function(OrderStatusService) {
                                return OrderStatusService.getOrderStatusByOrderId($stateParams.orderId);
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('orderStatus.status', {orderId:result.order.id}, { reload: true });
                    }, function() {
                        $state.go('orderStatus.status',{orderId:$stateParams.orderId},{ reload: true });
                    })
                }]
            });
    });
