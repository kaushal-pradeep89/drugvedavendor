'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderStatusDetailController', function ($scope, $rootScope, $stateParams, entity, OrderStatus, Order, OrderStatusDetails) {
        $scope.orderStatus = entity;
        
        $rootScope.$on('drugvedavendorApp:orderStatusUpdate', function(event, result) {
            $scope.orderStatus = result;
        });
    });
