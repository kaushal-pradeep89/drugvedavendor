'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderDetailsController', function ($scope, OrderDetails, OrderDetailsSearch) {
        $scope.orderDetailss = [];
        $scope.loadAll = function() {
            OrderDetails.query(function(result) {
               $scope.orderDetailss = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            OrderDetails.get({id: id}, function(result) {
                $scope.orderDetails = result;
                $('#deleteOrderDetailsConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            OrderDetails.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteOrderDetailsConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            OrderDetailsSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.orderDetailss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.orderDetails = {productId: null, productName: null, units: null, retailPrice: null, wholeSalePrice: null, purchasedPrice: null, id: null};
        };
    });
