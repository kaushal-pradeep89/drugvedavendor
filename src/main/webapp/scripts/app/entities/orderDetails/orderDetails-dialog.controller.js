'use strict';

angular.module('drugvedavendorApp').controller('OrderDetailsDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'OrderDetails', 'Order', 'OrderStatusDetails',
        function($scope, $stateParams, $modalInstance, entity, OrderDetails, Order, OrderStatusDetails) {

        $scope.orderDetails = entity;
        $scope.orders = Order.query();
        $scope.orderstatusdetailss = OrderStatusDetails.query();
        $scope.load = function(id) {
            OrderDetails.get({id : id}, function(result) {
                $scope.orderDetails = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:orderDetailsUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.orderDetails.id != null) {
                OrderDetails.update($scope.orderDetails, onSaveFinished);
            } else {
                OrderDetails.save($scope.orderDetails, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
