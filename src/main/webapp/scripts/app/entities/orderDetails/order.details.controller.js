'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderDetailController', function ($scope, $rootScope, $stateParams, entity, OrderDetails, Order, OrderStatusDetails) {
        $scope.orderDetailss = entity;
        $scope.orderId=entity[0].order.id;
        $scope.load = function (id) {
            OrderDetails.get({id: id}, function(result) {
                $scope.orderDetails = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:orderDetailsUpdate', function(event, result) {
            $scope.orderDetails = result;
        });
    });