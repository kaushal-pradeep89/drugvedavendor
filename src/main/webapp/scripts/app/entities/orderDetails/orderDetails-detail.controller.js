'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderDetailsDetailController', function ($scope, $rootScope, $stateParams, entity, OrderDetails, Order, OrderStatusDetails) {
        $scope.orderDetails = entity;
        $scope.load = function (id) {
            OrderDetails.get({id: id}, function(result) {
                $scope.orderDetails = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:orderDetailsUpdate', function(event, result) {
            $scope.orderDetails = result;
        });
    });
