'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('orderDetails', {
                parent: 'entity',
                url: '/orderDetailss',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'OrderDetailss'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderDetails/orderDetailss.html',
                        controller: 'OrderDetailsController'
                    }
                },
                resolve: {
                }
            })
            .state('orderDetails.detail', {
                parent: 'entity',
                url: '/orderDetails/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'OrderDetails'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderDetails/orderDetails-detail.html',
                        controller: 'OrderDetailsDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'OrderDetails', function($stateParams, OrderDetails) {
                        return OrderDetails.get({id : $stateParams.id});
                    }]
                }
            })
            .state('orderDetails.new', {
                parent: 'orderDetails',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/orderDetails/orderDetails-dialog.html',
                        controller: 'OrderDetailsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {productId: null, productName: null, units: null, retailPrice: null, wholeSalePrice: null, purchasedPrice: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('orderDetails', null, { reload: true });
                    }, function() {
                        $state.go('orderDetails');
                    })
                }]
            })
            .state('orderDetails.edit', {
                parent: 'orderDetails',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/orderDetails/orderDetails-dialog.html',
                        controller: 'OrderDetailsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['OrderDetails', function(OrderDetails) {
                                return OrderDetails.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('orderDetails', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            })
        .state('orderDetails.detail.orderId', {
            parent: 'entity',
            url: '/orderDetails/{id}/orderId',
            data: {
                roles: ['ROLE_USER'],
                pageTitle: 'OrderDetails'
            },
            views: {
                'content@': {
                    templateUrl: 'scripts/app/entities/orderDetails/orderDetailss.html',
                    controller: 'OrderDetailController'
                }
            },
            resolve: {
                entity: ['$stateParams', 'OrderDetailsService', function($stateParams, OrderDetailsService) {
                    return OrderDetailsService.getOrderDetailsByOrderId($stateParams.id);
                }]
            }
        })
    });
