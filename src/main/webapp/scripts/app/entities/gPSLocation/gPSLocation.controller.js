'use strict';

angular.module('drugvedavendorApp')
    .controller('GPSLocationController', function ($scope, GPSLocation, GPSLocationSearch) {
        $scope.gPSLocations = [];
        $scope.loadAll = function() {
            GPSLocation.query(function(result) {
               $scope.gPSLocations = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            GPSLocation.get({id: id}, function(result) {
                $scope.gPSLocation = result;
                $('#deleteGPSLocationConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            GPSLocation.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteGPSLocationConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            GPSLocationSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.gPSLocations = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.gPSLocation = {location: null, id: null};
        };
    });
