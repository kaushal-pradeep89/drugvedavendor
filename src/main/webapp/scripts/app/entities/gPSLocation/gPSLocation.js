'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('gPSLocation', {
                parent: 'entity',
                url: '/gPSLocations',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'GPSLocations'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/gPSLocation/gPSLocations.html',
                        controller: 'GPSLocationController'
                    }
                },
                resolve: {
                }
            })
            .state('gPSLocation.detail', {
                parent: 'entity',
                url: '/gPSLocation/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'GPSLocation'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/gPSLocation/gPSLocation-detail.html',
                        controller: 'GPSLocationDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'GPSLocation', function($stateParams, GPSLocation) {
                        return GPSLocation.get({id : $stateParams.id});
                    }]
                }
            })
            .state('gPSLocation.new', {
                parent: 'gPSLocation',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/gPSLocation/gPSLocation-dialog.html',
                        controller: 'GPSLocationDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {location: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('gPSLocation', null, { reload: true });
                    }, function() {
                        $state.go('gPSLocation');
                    })
                }]
            })
            .state('gPSLocation.edit', {
                parent: 'gPSLocation',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/gPSLocation/gPSLocation-dialog.html',
                        controller: 'GPSLocationDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['GPSLocation', function(GPSLocation) {
                                return GPSLocation.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('gPSLocation', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
