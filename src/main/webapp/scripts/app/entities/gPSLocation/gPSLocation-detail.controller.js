'use strict';

angular.module('drugvedavendorApp')
    .controller('GPSLocationDetailController', function ($scope, $rootScope, $stateParams, entity, GPSLocation) {
        $scope.gPSLocation = entity;
        $scope.load = function (id) {
            GPSLocation.get({id: id}, function(result) {
                $scope.gPSLocation = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:gPSLocationUpdate', function(event, result) {
            $scope.gPSLocation = result;
        });
    });
