'use strict';

angular.module('drugvedavendorApp').controller('GPSLocationDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'GPSLocation',
        function($scope, $stateParams, $modalInstance, entity, GPSLocation) {

        $scope.gPSLocation = entity;
        $scope.load = function(id) {
            GPSLocation.get({id : id}, function(result) {
                $scope.gPSLocation = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:gPSLocationUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.gPSLocation.id != null) {
                GPSLocation.update($scope.gPSLocation, onSaveFinished);
            } else {
                GPSLocation.save($scope.gPSLocation, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
