'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductImageController', function ($scope, ProductImage, ProductImageSearch) {
        $scope.productImages = [];
        $scope.loadAll = function() {
            ProductImage.query(function(result) {
               $scope.productImages = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ProductImage.get({id: id}, function(result) {
                $scope.productImage = result;
                $('#deleteProductImageConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ProductImage.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteProductImageConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ProductImageSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.productImages = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.productImage = {imageName: null, imageUrl: null, active: null, cratedDate: null, updatedDate: null, id: null};
        };
    });
