'use strict';

angular.module('drugvedavendorApp').controller('ProductImageDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ProductImage', 'Product', 'Upload','UPLOAD_URL',
        function($scope, $stateParams, $modalInstance, entity, ProductImage, Product,Upload,UPLOAD_URL) {

        $scope.productImage = entity;
        $scope.products = Product.query();
        $scope.load = function(id) {
            ProductImage.get({id : id}, function(result) {
                $scope.productImage = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:productImageUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.productImage.id != null) {
                ProductImage.update($scope.productImage, onSaveFinished);
            } else {
                ProductImage.save($scope.productImage, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
        
        $scope.startUpload = function() {
            if ($scope.file && !$scope.file.$error) {
              $scope.upload($scope.file);
            }
            else
            	console.log($scope.file);
         };

         
         $scope.onSelect = function ($item, $model, $label) {
        	    $scope.$model = $model;        	   
        	};

		//upload on file select or drop
		$scope.upload = function (file) {
			Upload.upload({
		        url: UPLOAD_URL+"/"+$scope.$model.id,
		        file: file
		    }).progress(function (evt) {
		        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
		        console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
		    }).success(function (data, status, headers, config) {
		        console.log('file ' + config.file.name + 'uploaded. Response: ' + data);
		        if(data.status == "ok"){
		        	console.log("image saved");
		        	 $scope.businessImage.folderNumber = parseInt(data.folderNumber);
		        	 $scope.businessImage.path = data.imagePath;
		        	 $scope.file = BUSINESS_IMAGES_URL+$scope.businessImage.path;
		        }
		    }).error(function (data, status, headers, config) {
		        console.log('error status: ' + status);
		    });
		};
}]);
