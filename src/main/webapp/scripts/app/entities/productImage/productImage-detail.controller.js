'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductImageDetailController', function ($scope, $rootScope, $stateParams, entity, ProductImage, Product) {
        $scope.productImage = entity;
        $scope.load = function (id) {
            ProductImage.get({id: id}, function(result) {
                $scope.productImage = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:productImageUpdate', function(event, result) {
            $scope.productImage = result;
        });
    });
