'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('productImage', {
                parent: 'entity',
                url: '/productImages',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductImages'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productImage/productImages.html',
                        controller: 'ProductImageController'
                    }
                },
                resolve: {
                }
            })
            .state('productImage.detail', {
                parent: 'entity',
                url: '/productImage/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductImage'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productImage/productImage-detail.html',
                        controller: 'ProductImageDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ProductImage', function($stateParams, ProductImage) {
                        return ProductImage.get({id : $stateParams.id});
                    }]
                }
            })
            .state('productImage.new', {
                parent: 'productImage',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productImage/productImage-dialog.html',
                        controller: 'ProductImageDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {imageName: null, imageUrl: null, active: null, cratedDate: null, updatedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('productImage', null, { reload: true });
                    }, function() {
                        $state.go('productImage');
                    })
                }]
            })
            .state('productImage.edit', {
                parent: 'productImage',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productImage/productImage-dialog.html',
                        controller: 'ProductImageDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ProductImage', function(ProductImage) {
                                return ProductImage.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('productImage', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
