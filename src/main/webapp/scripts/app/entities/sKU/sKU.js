'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('sKU', {
                parent: 'entity',
                url: '/sKUs',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'SKUs'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/sKU/sKUs.html',
                        controller: 'SKUController'
                    }
                },
                resolve: {
                }
            })
            .state('sKU.detail', {
                parent: 'entity',
                url: '/sKU/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'SKU'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/sKU/sKU-detail.html',
                        controller: 'SKUDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'SKU', function($stateParams, SKU) {
                        return SKU.get({id : $stateParams.id});
                    }]
                }
            })
            .state('sKU.new', {
                parent: 'sKU',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/sKU/sKU-dialog.html',
                        controller: 'SKUDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {createDate: null, updateDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('sKU', null, { reload: true });
                    }, function() {
                        $state.go('sKU');
                    })
                }]
            })
            .state('sKU.edit', {
                parent: 'sKU',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/sKU/sKU-dialog.html',
                        controller: 'SKUDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['SKU', function(SKU) {
                                return SKU.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('sKU', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
