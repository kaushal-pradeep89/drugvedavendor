'use strict';

angular.module('drugvedavendorApp')
    .controller('SKUDetailController', function ($scope, $rootScope, $stateParams, entity, SKU, Vendor, Price, Product) {
        $scope.sKU = entity;
        $scope.load = function (id) {
            SKU.get({id: id}, function(result) {
                $scope.sKU = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:sKUUpdate', function(event, result) {
            $scope.sKU = result;
        });
    });
