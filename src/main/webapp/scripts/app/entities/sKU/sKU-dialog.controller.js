'use strict';

angular.module('drugvedavendorApp').controller('SKUDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'SKU', 'Vendor', 'Price', 'Product',
        function($scope, $stateParams, $modalInstance, entity, SKU, Vendor, Price, Product) {

        $scope.sKU = entity;
        $scope.vendors = Vendor.query();
        $scope.prices = Price.query();
        $scope.products = Product.query();
        $scope.load = function(id) {
            SKU.get({id : id}, function(result) {
                $scope.sKU = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:sKUUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.sKU.id != null) {
                SKU.update($scope.sKU, onSaveFinished);
            } else {
                SKU.save($scope.sKU, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
