'use strict';

angular.module('drugvedavendorApp')
    .controller('SKUController', function ($scope, SKU, SKUSearch) {
        $scope.sKUs = [];
        $scope.loadAll = function() {
            SKU.query(function(result) {
               $scope.sKUs = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            SKU.get({id: id}, function(result) {
                $scope.sKU = result;
                $('#deleteSKUConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            SKU.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteSKUConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            SKUSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.sKUs = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.sKU = {createDate: null, updateDate: null, id: null};
        };
    });
