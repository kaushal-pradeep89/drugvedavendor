'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderStatusDetailsController', function ($scope, OrderStatusDetails, OrderStatusDetailsSearch) {
        $scope.orderStatusDetailss = [];
        $scope.loadAll = function() {
            OrderStatusDetails.query(function(result) {
               $scope.orderStatusDetailss = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            OrderStatusDetails.get({id: id}, function(result) {
                $scope.orderStatusDetails = result;
                $('#deleteOrderStatusDetailsConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            OrderStatusDetails.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteOrderStatusDetailsConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            OrderStatusDetailsSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.orderStatusDetailss = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.orderStatusDetails = {createDateTime: null, status: null, id: null};
        };
    });
