'use strict';

angular.module('drugvedavendorApp').controller('OrderStatusDetailsDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'OrderStatusDetails', 'Order', 'OrderStatus', 'OrderDetails',
        function($scope, $stateParams, $modalInstance, entity, OrderStatusDetails, Order, OrderStatus, OrderDetails) {

        $scope.orderStatusDetails = entity;
        $scope.orders = Order.query();
        $scope.orderstatuss = OrderStatus.query();
        $scope.orderdetailss = OrderDetails.query({filter: 'orderstatusdetails-is-null'});
        $scope.load = function(id) {
            OrderStatusDetails.get({id : id}, function(result) {
                $scope.orderStatusDetails = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:orderStatusDetailsUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.orderStatusDetails.id != null) {
                OrderStatusDetails.update($scope.orderStatusDetails, onSaveFinished);
            } else {
                OrderStatusDetails.save($scope.orderStatusDetails, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
