'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('orderStatusDetails', {
                parent: 'entity',
                url: '/orderStatusDetailss',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'OrderStatusDetailss'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderStatusDetails/orderStatusDetailss.html',
                        controller: 'OrderStatusDetailsController'
                    }
                },
                resolve: {
                }
            })
            .state('orderStatusDetails.detail', {
                parent: 'entity',
                url: '/orderStatusDetails/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Order Status Details'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderStatusDetails/orderStatusDetails-detail.html',
                        controller: 'OrderStatusDetailsDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'OrderStatusDetails', function($stateParams, OrderStatusDetails) {
                        return OrderStatusDetails.get({id : $stateParams.id});
                    }]
                }
            })
            .state('orderStatusDetails.getDetailsWithParams', {
                parent: 'entity',
                url: '/orderStatusDetails/{orderId}/{statusId}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Order Status Details'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/orderStatusDetails/orderStatusDetailss.html',
                        controller: 'OrderStatusDetailsDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'OrderStatusDetailsService', function($stateParams, OrderStatusDetailsService) {
                        return OrderStatusDetailsService.getOrderStatusDetails($stateParams.orderId, $stateParams.statusId);
                    }]
                }
            })
            .state('orderStatusDetails.new', {
                parent: 'orderStatusDetails',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/orderStatusDetails/orderStatusDetails-dialog.html',
                        controller: 'OrderStatusDetailsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {createDateTime: null, status: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('orderStatusDetails', null, { reload: true });
                    }, function() {
                        $state.go('orderStatusDetails');
                    })
                }]
            })
            .state('orderStatusDetails.edit', {
                parent: 'orderStatusDetails',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/orderStatusDetails/orderStatusDetails-dialog.html',
                        controller: 'OrderStatusDetailsDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['OrderStatusDetails', function(OrderStatusDetails) {
                                return OrderStatusDetails.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('orderStatusDetails', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
