'use strict';

angular.module('drugvedavendorApp')
    .controller('OrderStatusDetailsDetailController', function ($scope, $rootScope, $stateParams, entity, OrderStatusDetails, Order, OrderStatus, OrderDetails) {
        $scope.orderStatusDetailss = entity;
        $scope.orderId = $stateParams.orderId;
        $scope.load = function (id) {
            OrderStatusDetails.get({id: id}, function(result) {
                $scope.orderStatusDetails = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:orderStatusDetailsUpdate', function(event, result) {
            $scope.orderStatusDetails = result;
        });
    });
