'use strict';

angular.module('drugvedavendorApp').controller('ProductOptionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ProductOption', 'Product',
        function($scope, $stateParams, $modalInstance, entity, ProductOption, Product) {

        $scope.productOption = entity;
        $scope.products = Product.query();
        $scope.load = function(id) {
            ProductOption.get({id : id}, function(result) {
                $scope.productOption = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:productOptionUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.productOption.id != null) {
                ProductOption.update($scope.productOption, onSaveFinished);
            } else {
                ProductOption.save($scope.productOption, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
