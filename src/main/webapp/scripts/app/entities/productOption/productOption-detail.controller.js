'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductOptionDetailController', function ($scope, $rootScope, $stateParams, entity, ProductOption, Product) {
        $scope.productOption = entity;
        $scope.load = function (id) {
            ProductOption.get({id: id}, function(result) {
                $scope.productOption = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:productOptionUpdate', function(event, result) {
            $scope.productOption = result;
        });
    });
