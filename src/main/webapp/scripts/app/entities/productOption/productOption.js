'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('productOption', {
                parent: 'entity',
                url: '/productOptions',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductOptions'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productOption/productOptions.html',
                        controller: 'ProductOptionController'
                    }
                },
                resolve: {
                }
            })
            .state('productOption.detail', {
                parent: 'entity',
                url: '/productOption/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductOption'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productOption/productOption-detail.html',
                        controller: 'ProductOptionDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ProductOption', function($stateParams, ProductOption) {
                        return ProductOption.get({id : $stateParams.id});
                    }]
                }
            })
            .state('productOption.new', {
                parent: 'productOption',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productOption/productOption-dialog.html',
                        controller: 'ProductOptionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {measureType: null, quantity: null, active: null, cratedDate: null, updatedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('productOption', null, { reload: true });
                    }, function() {
                        $state.go('productOption');
                    })
                }]
            })
            .state('productOption.edit', {
                parent: 'productOption',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productOption/productOption-dialog.html',
                        controller: 'ProductOptionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ProductOption', function(ProductOption) {
                                return ProductOption.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('productOption', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
