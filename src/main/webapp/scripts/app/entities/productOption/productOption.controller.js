'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductOptionController', function ($scope, ProductOption, ProductOptionSearch) {
        $scope.productOptions = [];
        $scope.loadAll = function() {
            ProductOption.query(function(result) {
               $scope.productOptions = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ProductOption.get({id: id}, function(result) {
                $scope.productOption = result;
                $('#deleteProductOptionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ProductOption.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteProductOptionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ProductOptionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.productOptions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.productOption = {measureType: null, quantity: null, active: null, cratedDate: null, updatedDate: null, id: null};
        };
    });
