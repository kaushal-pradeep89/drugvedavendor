'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductCategoryDetailController', function ($scope, $rootScope, $stateParams, entity, ProductCategory, Product) {
        $scope.productCategory = entity;
        $scope.load = function (id) {
            ProductCategory.get({id: id}, function(result) {
                $scope.productCategory = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:productCategoryUpdate', function(event, result) {
            $scope.productCategory = result;
        });
    });
