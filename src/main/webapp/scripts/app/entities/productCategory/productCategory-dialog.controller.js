'use strict';

angular.module('drugvedavendorApp').controller('ProductCategoryDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ProductCategory', 'Product',
        function($scope, $stateParams, $modalInstance, entity, ProductCategory, Product) {

        $scope.productCategory = entity;
        $scope.products = Product.query();
        $scope.load = function(id) {
            ProductCategory.get({id : id}, function(result) {
                $scope.productCategory = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:productCategoryUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
        	console.log("in save");
            if ($scope.productCategory.id != null) {
                ProductCategory.update($scope.productCategory, onSaveFinished);
            } else {
                ProductCategory.save($scope.productCategory, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
