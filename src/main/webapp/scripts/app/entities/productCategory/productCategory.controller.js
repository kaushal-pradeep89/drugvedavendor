'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductCategoryController', function ($scope, ProductCategory, ProductCategorySearch) {
        $scope.productCategorys = [];
        $scope.loadAll = function() {
            ProductCategory.query(function(result) {
               $scope.productCategorys = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ProductCategory.get({id: id}, function(result) {
                $scope.productCategory = result;
                $('#deleteProductCategoryConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ProductCategory.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteProductCategoryConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ProductCategorySearch.query({query: $scope.searchQuery}, function(result) {
                $scope.productCategorys = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.productCategory = {categoryName: null, categoryDescription: null, active: null, cratedDate: null, updatedDate: null, id: null};
        };
    });
