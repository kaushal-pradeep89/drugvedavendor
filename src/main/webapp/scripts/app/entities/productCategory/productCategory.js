'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('productCategory', {
                parent: 'entity',
                url: '/productCategorys',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductCategorys'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productCategory/productCategorys.html',
                        controller: 'ProductCategoryController'
                    }
                },
                resolve: {
                }
            })
            .state('productCategory.detail', {
                parent: 'entity',
                url: '/productCategory/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductCategory'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productCategory/productCategory-detail.html',
                        controller: 'ProductCategoryDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ProductCategory', function($stateParams, ProductCategory) {
                        return ProductCategory.get({id : $stateParams.id});
                    }]
                }
            })
            .state('productCategory.new', {
                parent: 'productCategory',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productCategory/productCategory-dialog.html',
                        controller: 'ProductCategoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {categoryName: null, categoryDescription: null, active: null, cratedDate: null, updatedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('productCategory', null, { reload: true });
                    }, function() {
                        $state.go('productCategory');
                    })
                }]
            })
            .state('productCategory.edit', {
                parent: 'productCategory',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productCategory/productCategory-dialog.html',
                        controller: 'ProductCategoryDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ProductCategory', function(ProductCategory) {
                                return ProductCategory.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('productCategory', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
