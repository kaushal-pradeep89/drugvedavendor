'use strict';

angular.module('drugvedavendorApp')
    .controller('CompositionController', function ($scope, Composition, CompositionSearch) {
        $scope.compositions = [];
        $scope.loadAll = function() {
            Composition.query(function(result) {
               $scope.compositions = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Composition.get({id: id}, function(result) {
                $scope.composition = result;
                $('#deleteCompositionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Composition.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteCompositionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            CompositionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.compositions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.composition = {composition: null, description: null, id: null};
        };
    });
