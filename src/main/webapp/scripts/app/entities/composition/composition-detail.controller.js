'use strict';

angular.module('drugvedavendorApp')
    .controller('CompositionDetailController', function ($scope, $rootScope, $stateParams, entity, Composition, Product) {
        $scope.composition = entity;
        $scope.load = function (id) {
            Composition.get({id: id}, function(result) {
                $scope.composition = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:compositionUpdate', function(event, result) {
            $scope.composition = result;
        });
    });
