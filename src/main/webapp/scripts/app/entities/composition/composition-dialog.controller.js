'use strict';

angular.module('drugvedavendorApp').controller('CompositionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Composition', 'Product',
        function($scope, $stateParams, $modalInstance, entity, Composition, Product) {

        $scope.composition = entity;
        $scope.products = Product.query();
        $scope.load = function(id) {
            Composition.get({id : id}, function(result) {
                $scope.composition = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:compositionUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.composition.id != null) {
                Composition.update($scope.composition, onSaveFinished);
            } else {
                Composition.save($scope.composition, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
