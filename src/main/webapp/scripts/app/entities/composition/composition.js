'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('composition', {
                parent: 'entity',
                url: '/compositions',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Compositions'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/composition/compositions.html',
                        controller: 'CompositionController'
                    }
                },
                resolve: {
                }
            })
            .state('composition.detail', {
                parent: 'entity',
                url: '/composition/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Composition'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/composition/composition-detail.html',
                        controller: 'CompositionDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Composition', function($stateParams, Composition) {
                        return Composition.get({id : $stateParams.id});
                    }]
                }
            })
            .state('composition.new', {
                parent: 'composition',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/composition/composition-dialog.html',
                        controller: 'CompositionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {composition: null, description: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('composition', null, { reload: true });
                    }, function() {
                        $state.go('composition');
                    })
                }]
            })
            .state('composition.edit', {
                parent: 'composition',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/composition/composition-dialog.html',
                        controller: 'CompositionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Composition', function(Composition) {
                                return Composition.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('composition', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
