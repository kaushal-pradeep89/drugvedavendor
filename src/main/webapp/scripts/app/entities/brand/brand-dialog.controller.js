'use strict';

angular.module('drugvedavendorApp').controller('BrandDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Brand', 'Product',
        function($scope, $stateParams, $modalInstance, entity, Brand, Product) {

        $scope.brand = entity;
        $scope.products = Product.query();
        $scope.load = function(id) {
            Brand.get({id : id}, function(result) {
                $scope.brand = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:brandUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.brand.id != null) {
                Brand.update($scope.brand, onSaveFinished);
            } else {
                Brand.save($scope.brand, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
