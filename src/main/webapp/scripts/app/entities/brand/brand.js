'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('brand', {
                parent: 'entity',
                url: '/brands',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Brands'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/brand/brands.html',
                        controller: 'BrandController'
                    }
                },
                resolve: {
                }
            })
            .state('brand.detail', {
                parent: 'entity',
                url: '/brand/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Brand'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/brand/brand-detail.html',
                        controller: 'BrandDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Brand', function($stateParams, Brand) {
                        return Brand.get({id : $stateParams.id});
                    }]
                }
            })
            .state('brand.new', {
                parent: 'brand',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/brand/brand-dialog.html',
                        controller: 'BrandDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {brandName: null, brandDescription: null, cratedDate: null, updatedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('brand', null, { reload: true });
                    }, function() {
                        $state.go('brand');
                    })
                }]
            })
            .state('brand.edit', {
                parent: 'brand',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/brand/brand-dialog.html',
                        controller: 'BrandDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Brand', function(Brand) {
                                return Brand.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('brand', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
