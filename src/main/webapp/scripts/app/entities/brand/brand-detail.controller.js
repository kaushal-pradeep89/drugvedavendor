'use strict';

angular.module('drugvedavendorApp')
    .controller('BrandDetailController', function ($scope, $rootScope, $stateParams, entity, Brand, Product) {
        $scope.brand = entity;
        $scope.load = function (id) {
            Brand.get({id: id}, function(result) {
                $scope.brand = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:brandUpdate', function(event, result) {
            $scope.brand = result;
        });
    });
