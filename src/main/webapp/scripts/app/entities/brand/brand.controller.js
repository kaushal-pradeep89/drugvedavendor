'use strict';

angular.module('drugvedavendorApp')
    .controller('BrandController', function ($scope, Brand, BrandSearch) {
        $scope.brands = [];
        $scope.loadAll = function() {
            Brand.query(function(result) {
               $scope.brands = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Brand.get({id: id}, function(result) {
                $scope.brand = result;
                $('#deleteBrandConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Brand.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteBrandConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            BrandSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.brands = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.brand = {brandName: null, brandDescription: null, cratedDate: null, updatedDate: null, id: null};
        };
    });
