'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductCatalogueDetailController', function ($scope, $rootScope, $stateParams, entity, ProductCatalogue, Tax, ProductVariation, Vendor, Product, Price, CatalogueProductVariation,ProductSearch,$timeout) {
        $scope.productCatalogue = entity;
        $scope.taxs = Tax.query();
        $scope.productVariation=productvariation($scope.productCatalogue);
        //$scope.variationOptions = [{"quanity":10}];
        $scope.variationOptions = variationOptions($scope.productVariation);
        $scope.vendors = Vendor.query();
        $scope.products = function(value){
        	   if(value!=undefined && value.trim().length >= 2){
        			 return ProductSearch.getProducts(value).then(function(data){
    	        		return data;
        			  }	
    	        );
        	}
        };

        $scope.prices = Price.query({filter: 'productcatalogue-is-null'});
        $scope.load = function (id) {
            ProductCatalogue.get({id: id}, function(result) {
                $scope.productCatalogue = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:productCatalogueUpdate', function(event, result) {
            $scope.productCatalogue = result;
        });
        
        var onSaveFinished = function (result) {
            $scope.productCatalogue=result;
        };

        $scope.save = function () {
            if ($scope.productCatalogue.id != null) {
                ProductCatalogue.update($scope.productCatalogue, onSaveFinished);
            } else {
                ProductCatalogue.save($scope.productCatalogue, onSaveFinished);
            }
        };
        $scope.saveVariation = function(){
        	console.log($scope.productVariation);
        	$scope.productVariation.variationoptions = $scope.variationOptions;
        	var prodVar = {};
        	prodVar = $scope.productVariation;
              
        	prodVar.variationoptions=$scope.variationOptions;
        	console.log(JSON.stringify(prodVar));
        	console.log("variation "+$scope.productCatalogue.id);
        	CatalogueProductVariation.save($scope.productCatalogue.id,$scope.productVariation);
        	console.log($scope.productVariation);
        	
        };
        $scope.addTo = function(array, template) {
        	array.push(template);
        };
        $scope.removeFrom = function(array, index) {
        	var opt = array[index];
        	opt.active = false;
        	array[index] = opt;
          };
        function productvariation(catalogue){
        	if(catalogue.id != undefined){
        		return catalogue.productVariation;
        	}
        	
        	return {};
        };
        
        function variationOptions(productVariation){
        	if(productVariation != null){
	        	if(productVariation.id != undefined){
	        		return productVariation.variationoptions;
	        	}
        	}	
	        	return [{"quanity":10,"active":true}];
        };
      
    });
