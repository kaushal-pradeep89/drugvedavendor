'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('productCatalogue', {
                parent: 'entity',
                url: '/productCatalogues',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductCatalogues'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productCatalogue/productCatalogues.html',
                        controller: 'ProductCatalogueController'
                    }
                },
                resolve: {
                }
            })
            .state('productCatalogue.detail', {
                parent: 'entity',
                url: '/productCatalogue/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductCatalogue'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productCatalogue/productCatalogue-detail.html',
                        controller: 'ProductCatalogueDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ProductCatalogue', function($stateParams, ProductCatalogue) {
                        return ProductCatalogue.get({id : $stateParams.id});
                    }]
                }
            })
            .state('productCatalogue.new', {
                parent: 'productCatalogue',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/productCatalogue/productCatalogue-dialog-custom-frg.html',
                         controller: 'ProductCatalogueDetailController',
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ProductCatalogue', function($stateParams, ProductCatalogue) {
                        return {};
                    }]
                }
            })
            .state('productCatalogue.new.productVariation', {
                parent: 'productCatalogue.new',
                url : '/productCatalogue.new.productVariation/{id}',
                data: {
                    roles: ['ROLE_USER'],
                },
               
	        	 templateUrl: 'scripts/app/entities/productCatalogue/productCatalogue-productVariation.html',
	        	 controller: 'ProductCatalogueDetailController', 	 
	        	 resolve: {
	        		 entity: ['$stateParams','ProductVariation', function($stateParams,ProductVariation) {
	                     return ProductVariation.get({id : $stateParams.id});
	                 }]
	             }
	   
                
            })
            .state('productCatalogue.new.tax', {
                parent: 'productCatalogue.new',
                url : '/productCatalogue.new.tax/{id}',
                data: {
                    roles: ['ROLE_USER'],
                },
               
            	 templateUrl: 'scripts/app/entities/productCatalogue/productCatalogue-tax.html',
            	 controller: 'ProductCatalogueTaxController', 	 
            	 resolve: {
                     entity: ['$stateParams','Tax', 'ProductCatalogue','ShareService', function($stateParams,Tax, ProductCatalogue, ShareService) {      
                    	  
                    	  return ProductCatalogue.get({id:$stateParams.id});
                     }]
                 }
       
                
            })
            .state('productCatalogue.edit', {
                parent: 'productCatalogue',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },              
                views: {
                    'content@': {
                    	 templateUrl: 'scripts/app/entities/productCatalogue/productCatalogue-dialog-custom-frg.html',
                         controller: 'ProductCatalogueDetailController',
                    }
                },
                resolve: {
                	entity: ['$stateParams','ProductCatalogue', function($stateParams,ProductCatalogue) {
                        return ProductCatalogue.get({id : $stateParams.id});
                    }]
                }
            });
    });
