'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductCatalogueTaxController', function ($scope, $rootScope, $stateParams, entity, ProductCatalogue, Tax, ShareService) {
        $scope.tax = entity.tax;
        $scope.productCatalogue = entity;
        //$scope.productCatalogue = ShareService.getProperty();
        $scope.taxs = [];
        $scope.loadAll = function() {
            Tax.query(function(result) {
               $scope.taxs = result;
            });
        };
        
        $scope.loadAll();
        
        $scope.onSelect = function ($item, $model, $label) {
        	console.log("tax selected");
    	    $scope.tax = $model;
    	    $scope.productCatalogue.tax = $scope.tax;
    	};
        
    	 var onSaveFinished = function (result) {
         	console.log(result);
             $scope.productCatalogue=result;
         };

         $scope.saveTax = function () {
             if ($scope.productCatalogue.id != null) {
                 ProductCatalogue.update($scope.productCatalogue, onSaveFinished);
             } else {
                 ProductCatalogue.save($scope.productCatalogue, onSaveFinished);
             }
         };     
        
    }
);