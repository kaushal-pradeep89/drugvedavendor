'use strict';

angular.module('drugvedavendorApp').controller('ProductCatalogueDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ProductCatalogue', 'Tax', 'ProductVariation', 'Vendor', 'Product', 'Price',
        function($scope, $stateParams, $modalInstance, entity, ProductCatalogue, Tax, ProductVariation, Vendor, Product, Price) {

        $scope.productCatalogue = entity;
        $scope.taxs = Tax.query();
        $scope.productvariations = ProductVariation.query();
        $scope.vendors = Vendor.query();
        $scope.products = Product.query();
        $scope.prices = Price.query({filter: 'productcatalogue-is-null'});
        $scope.load = function(id) {
            ProductCatalogue.get({id : id}, function(result) {
                $scope.productCatalogue = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:productCatalogueUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.productCatalogue.id != null) {
                ProductCatalogue.update($scope.productCatalogue, onSaveFinished);
            } else {
                ProductCatalogue.save($scope.productCatalogue, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
