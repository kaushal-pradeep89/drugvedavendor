'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductCatalogueController', function ($scope, ProductCatalogue, ProductCatalogueSearch) {
        $scope.productCatalogues = [];
        $scope.loadAll = function() {
            ProductCatalogue.query(function(result) {
               $scope.productCatalogues = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ProductCatalogue.get({id: id}, function(result) {
                $scope.productCatalogue = result;
                $('#deleteProductCatalogueConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ProductCatalogue.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteProductCatalogueConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ProductCatalogueSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.productCatalogues = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.productCatalogue = {minOrder: null, applyTax: null, id: null};
        };
    });
