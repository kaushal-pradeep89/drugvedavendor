'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('variationOption', {
                parent: 'entity',
                url: '/variationOptions',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'VariationOptions'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/variationOption/variationOptions.html',
                        controller: 'VariationOptionController'
                    }
                },
                resolve: {
                }
            })
            .state('variationOption.detail', {
                parent: 'entity',
                url: '/variationOption/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'VariationOption'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/variationOption/variationOption-detail.html',
                        controller: 'VariationOptionDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'VariationOption', function($stateParams, VariationOption) {
                        return VariationOption.get({id : $stateParams.id});
                    }]
                }
            })
            .state('variationOption.new', {
                parent: 'variationOption',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/variationOption/variationOption-dialog.html',
                        controller: 'VariationOptionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {quanity: null, active: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('variationOption', null, { reload: true });
                    }, function() {
                        $state.go('variationOption');
                    })
                }]
            })
            .state('variationOption.edit', {
                parent: 'variationOption',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/variationOption/variationOption-dialog.html',
                        controller: 'VariationOptionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['VariationOption', function(VariationOption) {
                                return VariationOption.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('variationOption', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
