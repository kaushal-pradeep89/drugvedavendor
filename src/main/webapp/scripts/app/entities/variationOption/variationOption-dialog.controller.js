'use strict';

angular.module('drugvedavendorApp').controller('VariationOptionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'VariationOption', 'ProductVariation',
        function($scope, $stateParams, $modalInstance, entity, VariationOption, ProductVariation) {

        $scope.variationOption = entity;
        $scope.productvariations = ProductVariation.query();
        $scope.load = function(id) {
            VariationOption.get({id : id}, function(result) {
                $scope.variationOption = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:variationOptionUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.variationOption.id != null) {
                VariationOption.update($scope.variationOption, onSaveFinished);
            } else {
                VariationOption.save($scope.variationOption, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
