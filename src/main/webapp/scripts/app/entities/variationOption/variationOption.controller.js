'use strict';

angular.module('drugvedavendorApp')
    .controller('VariationOptionController', function ($scope, VariationOption, VariationOptionSearch) {
        $scope.variationOptions = [];
        $scope.loadAll = function() {
            VariationOption.query(function(result) {
               $scope.variationOptions = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            VariationOption.get({id: id}, function(result) {
                $scope.variationOption = result;
                $('#deleteVariationOptionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            VariationOption.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteVariationOptionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            VariationOptionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.variationOptions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.variationOption = {quanity: null, active: null, id: null};
        };
    });
