'use strict';

angular.module('drugvedavendorApp')
    .controller('VariationOptionDetailController', function ($scope, $rootScope, $stateParams, entity, VariationOption, ProductVariation) {
        $scope.variationOption = entity;
        $scope.load = function (id) {
            VariationOption.get({id: id}, function(result) {
                $scope.variationOption = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:variationOptionUpdate', function(event, result) {
            $scope.variationOption = result;
        });
    });
