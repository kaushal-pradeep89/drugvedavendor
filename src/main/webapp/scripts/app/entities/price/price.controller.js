'use strict';

angular.module('drugvedavendorApp')
    .controller('PriceController', function ($scope, Price, PriceSearch) {
        $scope.prices = [];
        $scope.loadAll = function() {
            Price.query(function(result) {
               $scope.prices = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Price.get({id: id}, function(result) {
                $scope.price = result;
                $('#deletePriceConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Price.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deletePriceConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            PriceSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.prices = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.price = {price: null, bulkPrice: null, offerPrice: null, discount: null, createDate: null, updateDate: null, id: null};
        };
    });
