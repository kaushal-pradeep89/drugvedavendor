'use strict';

angular.module('drugvedavendorApp')
    .controller('PriceDetailController', function ($scope, $rootScope, $stateParams, entity, Price, SKU) {
        $scope.price = entity;
        $scope.load = function (id) {
            Price.get({id: id}, function(result) {
                $scope.price = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:priceUpdate', function(event, result) {
            $scope.price = result;
        });
    });
