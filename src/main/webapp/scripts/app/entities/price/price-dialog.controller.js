'use strict';

angular.module('drugvedavendorApp').controller('PriceDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Price', 'SKU',
        function($scope, $stateParams, $modalInstance, entity, Price, SKU) {

        $scope.price = entity;
        $scope.skus = SKU.query();
        $scope.load = function(id) {
            Price.get({id : id}, function(result) {
                $scope.price = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:priceUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.price.id != null) {
                Price.update($scope.price, onSaveFinished);
            } else {
                Price.save($scope.price, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
