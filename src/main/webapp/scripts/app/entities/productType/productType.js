'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('productType', {
                parent: 'entity',
                url: '/productTypes',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductTypes'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productType/productTypes.html',
                        controller: 'ProductTypeController'
                    }
                },
                resolve: {
                }
            })
            .state('productType.detail', {
                parent: 'entity',
                url: '/productType/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'ProductType'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/productType/productType-detail.html',
                        controller: 'ProductTypeDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'ProductType', function($stateParams, ProductType) {
                        return ProductType.get({id : $stateParams.id});
                    }]
                }
            })
            .state('productType.new', {
                parent: 'productType',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productType/productType-dialog.html',
                        controller: 'ProductTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {productType: null, cratedDate: null, updatedDate: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('productType', null, { reload: true });
                    }, function() {
                        $state.go('productType');
                    })
                }]
            })
            .state('productType.edit', {
                parent: 'productType',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/productType/productType-dialog.html',
                        controller: 'ProductTypeDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['ProductType', function(ProductType) {
                                return ProductType.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('productType', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
