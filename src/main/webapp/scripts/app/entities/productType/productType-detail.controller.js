'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductTypeDetailController', function ($scope, $rootScope, $stateParams, entity, ProductType, Product) {
        $scope.productType = entity;
        $scope.load = function (id) {
            ProductType.get({id: id}, function(result) {
                $scope.productType = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:productTypeUpdate', function(event, result) {
            $scope.productType = result;
        });
    });
