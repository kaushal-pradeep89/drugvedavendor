'use strict';

angular.module('drugvedavendorApp').controller('ProductTypeDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'ProductType', 'Product',
        function($scope, $stateParams, $modalInstance, entity, ProductType, Product) {

        $scope.productType = entity;
        $scope.products = Product.query();
        $scope.load = function(id) {
            ProductType.get({id : id}, function(result) {
                $scope.productType = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:productTypeUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.productType.id != null) {
                ProductType.update($scope.productType, onSaveFinished);
            } else {
                ProductType.save($scope.productType, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
