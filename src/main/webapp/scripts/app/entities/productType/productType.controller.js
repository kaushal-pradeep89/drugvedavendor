'use strict';

angular.module('drugvedavendorApp')
    .controller('ProductTypeController', function ($scope, ProductType, ProductTypeSearch) {
        $scope.productTypes = [];
        $scope.loadAll = function() {
            ProductType.query(function(result) {
               $scope.productTypes = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            ProductType.get({id: id}, function(result) {
                $scope.productType = result;
                $('#deleteProductTypeConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            ProductType.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteProductTypeConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            ProductTypeSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.productTypes = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.productType = {productType: null, cratedDate: null, updatedDate: null, id: null};
        };
    });
