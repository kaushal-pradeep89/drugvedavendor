'use strict';

angular.module('drugvedavendorApp')
    .controller('AddressDetailController', function ($scope, $rootScope, $stateParams, entity, Address, Vendor) {
        $scope.address = entity;
        $scope.load = function (id) {
            Address.get({id: id}, function(result) {
                $scope.address = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:addressUpdate', function(event, result) {
            $scope.address = result;
        });
    });
