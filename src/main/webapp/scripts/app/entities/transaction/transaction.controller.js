'use strict';

angular.module('drugvedavendorApp')
    .controller('TransactionController', function ($scope, Transaction, TransactionSearch) {
        $scope.transactions = [];
        $scope.loadAll = function() {
            Transaction.query(function(result) {
               $scope.transactions = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            Transaction.get({id: id}, function(result) {
                $scope.transaction = result;
                $('#deleteTransactionConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            Transaction.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteTransactionConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            TransactionSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.transactions = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.transaction = {createdDateTime: null, transactionType: null, consumerId: null, amount: null, id: null};
        };
    });
