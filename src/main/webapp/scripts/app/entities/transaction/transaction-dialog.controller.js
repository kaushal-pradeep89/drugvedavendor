'use strict';

angular.module('drugvedavendorApp').controller('TransactionDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'Transaction', 'Order',
        function($scope, $stateParams, $modalInstance, entity, Transaction, Order) {

        $scope.transaction = entity;
        $scope.orders = Order.query();
        $scope.load = function(id) {
            Transaction.get({id : id}, function(result) {
                $scope.transaction = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:transactionUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.transaction.id != null) {
                Transaction.update($scope.transaction, onSaveFinished);
            } else {
                Transaction.save($scope.transaction, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
