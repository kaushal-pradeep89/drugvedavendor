'use strict';

angular.module('drugvedavendorApp')
    .controller('TransactionDetailController', function ($scope, $rootScope, $stateParams, entity, Transaction, Order) {
        $scope.transaction = entity;
        $scope.load = function (id) {
            Transaction.get({id: id}, function(result) {
                $scope.transaction = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:transactionUpdate', function(event, result) {
            $scope.transaction = result;
        });
    });
