'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('transaction', {
                parent: 'entity',
                url: '/transactions',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Transactions'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/transaction/transactions.html',
                        controller: 'TransactionController'
                    }
                },
                resolve: {
                }
            })
            .state('transaction.detail', {
                parent: 'entity',
                url: '/transaction/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'Transaction'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/transaction/transaction-detail.html',
                        controller: 'TransactionDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'Transaction', function($stateParams, Transaction) {
                        return Transaction.get({id : $stateParams.id});
                    }]
                }
            })
            .state('transaction.new', {
                parent: 'transaction',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/transaction/transaction-dialog.html',
                        controller: 'TransactionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {createdDateTime: null, transactionType: null, consumerId: null, amount: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('transaction', null, { reload: true });
                    }, function() {
                        $state.go('transaction');
                    })
                }]
            })
            .state('transaction.edit', {
                parent: 'transaction',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/transaction/transaction-dialog.html',
                        controller: 'TransactionDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['Transaction', function(Transaction) {
                                return Transaction.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('transaction', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
