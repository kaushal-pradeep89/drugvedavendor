'use strict';

angular.module('drugvedavendorApp')
    .config(function ($stateProvider) {
        $stateProvider
            .state('state', {
                parent: 'entity',
                url: '/states',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'States'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/state/states.html',
                        controller: 'StateController'
                    }
                },
                resolve: {
                }
            })
            .state('state.detail', {
                parent: 'entity',
                url: '/state/{id}',
                data: {
                    roles: ['ROLE_USER'],
                    pageTitle: 'State'
                },
                views: {
                    'content@': {
                        templateUrl: 'scripts/app/entities/state/state-detail.html',
                        controller: 'StateDetailController'
                    }
                },
                resolve: {
                    entity: ['$stateParams', 'State', function($stateParams, State) {
                        return State.get({id : $stateParams.id});
                    }]
                }
            })
            .state('state.new', {
                parent: 'state',
                url: '/new',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/state/state-dialog.html',
                        controller: 'StateDialogController',
                        size: 'lg',
                        resolve: {
                            entity: function () {
                                return {stateName: null, id: null};
                            }
                        }
                    }).result.then(function(result) {
                        $state.go('state', null, { reload: true });
                    }, function() {
                        $state.go('state');
                    })
                }]
            })
            .state('state.edit', {
                parent: 'state',
                url: '/{id}/edit',
                data: {
                    roles: ['ROLE_USER'],
                },
                onEnter: ['$stateParams', '$state', '$modal', function($stateParams, $state, $modal) {
                    $modal.open({
                        templateUrl: 'scripts/app/entities/state/state-dialog.html',
                        controller: 'StateDialogController',
                        size: 'lg',
                        resolve: {
                            entity: ['State', function(State) {
                                return State.get({id : $stateParams.id});
                            }]
                        }
                    }).result.then(function(result) {
                        $state.go('state', null, { reload: true });
                    }, function() {
                        $state.go('^');
                    })
                }]
            });
    });
