'use strict';

angular.module('drugvedavendorApp')
    .controller('StateController', function ($scope, State, StateSearch) {
        $scope.states = [];
        $scope.loadAll = function() {
            State.query(function(result) {
               $scope.states = result;
            });
        };
        $scope.loadAll();

        $scope.delete = function (id) {
            State.get({id: id}, function(result) {
                $scope.state = result;
                $('#deleteStateConfirmation').modal('show');
            });
        };

        $scope.confirmDelete = function (id) {
            State.delete({id: id},
                function () {
                    $scope.loadAll();
                    $('#deleteStateConfirmation').modal('hide');
                    $scope.clear();
                });
        };

        $scope.search = function () {
            StateSearch.query({query: $scope.searchQuery}, function(result) {
                $scope.states = result;
            }, function(response) {
                if(response.status === 404) {
                    $scope.loadAll();
                }
            });
        };

        $scope.refresh = function () {
            $scope.loadAll();
            $scope.clear();
        };

        $scope.clear = function () {
            $scope.state = {stateName: null, id: null};
        };
    });
