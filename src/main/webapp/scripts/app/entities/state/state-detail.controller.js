'use strict';

angular.module('drugvedavendorApp')
    .controller('StateDetailController', function ($scope, $rootScope, $stateParams, entity, State) {
        $scope.state = entity;
        $scope.load = function (id) {
            State.get({id: id}, function(result) {
                $scope.state = result;
            });
        };
        $rootScope.$on('drugvedavendorApp:stateUpdate', function(event, result) {
            $scope.state = result;
        });
    });
