'use strict';

angular.module('drugvedavendorApp').controller('StateDialogController',
    ['$scope', '$stateParams', '$modalInstance', 'entity', 'State',
        function($scope, $stateParams, $modalInstance, entity, State) {

        $scope.state = entity;
        $scope.load = function(id) {
            State.get({id : id}, function(result) {
                $scope.state = result;
            });
        };

        var onSaveFinished = function (result) {
            $scope.$emit('drugvedavendorApp:stateUpdate', result);
            $modalInstance.close(result);
        };

        $scope.save = function () {
            if ($scope.state.id != null) {
                State.update($scope.state, onSaveFinished);
            } else {
                State.save($scope.state, onSaveFinished);
            }
        };

        $scope.clear = function() {
            $modalInstance.dismiss('cancel');
        };
}]);
