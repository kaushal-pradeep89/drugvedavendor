'use strict';

angular.module('drugvedavendorApp')
    .controller('LogoutController', function (Auth) {
        Auth.logout();
    });
